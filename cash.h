#ifndef CASH_H
#define CASH_H
#include "ui_cash.h"
#include "common.h"

class CException;//класс исключений базы данных
//class CScanerException;//класс исключений сканера ШК
//class CPrinterException;//класс исключений принтера - ФР
class CDatabase;
class CGoodsModel;// класс моедли представления
class CScaner;

class QWidgetCash: public QWidget, public Ui::CashForm
{
  Q_OBJECT
    private:
  enum eAction {eSearchArticul=11,//поиск по артикулу
		eSearchBC=88,//поиск по ШК
		eQuantity=66//изменение количества
  };//перечисление операций
  CGood current_pos;//текущая позиция
  CGood previous_pos;//предыдущая позиция
  QMap<int, CGood> goods;
  void getArgument() throw();
  QString argument;//строка со значением поля ввода
  CDatabase* db;
  CGoodsModel* modelGoods;
  enum eLayout {eDigit=0, eKeyAction=1};//раскладка кнопок цифры, кнопки действий
  eLayout button_layout;
  eCashSatate state;//состояние чека(закрыт, набор позиций, подитог)
  QVector<CScaner*> scaners;
  void CatchHandler(CException e);
  //void CatchPortHandler();
  void DisplayCurrentPos();//отобразить текущую позицию в метках
  void ReloadCheck(bool force) throw (CException);//обновляет чек в окне чека
  void SetupTableView();//настроить представление чека, связать с моделью
  void ActionShortKey(eAction a);//вызов обработчиков по двузначным сокращениям для операций
  void SetupAction();//установка обработчиков
  void SetupButtons();//установка оформления кнопок
  void SetupButton(QPushButton* pb, const KeySettings& ks) throw();//устновка одной кнопки
  void SetupScaner();//настройка сканеров
  void InvalidArgument() throw();//выводит сообщение о недопустимом аргументе
  void PostEvent(int key) throw();//отправляет события нажатия цифровых клавиш, клавиш перемещения и редоктирования полю ввода lineEdit
  void DisplayTotal() throw();//отобразить сумму по чеку
  void UpdateState() throw();//обновляет состояние виджетов в соотьветствии с сотоянием чека
  void ClearWidget() throw();//очищает все виджеты
 public:
  QWidgetCash(const WidgetParam& wp, QWidget *parent = NULL);
  virtual ~QWidgetCash();
  public slots:
  void SearchArticul();//поиск по артикулу|поиск по ШК
  void SearchBC();
  void SearchScanerBC(const QString& bc);
  void SearchCost();//поиск по цене
  void NumAction();//переключение раскладки кнопок цифры-действие
  void Function();//вызов слотов по двузначному коду
  void SetQuantity();//установка количества|открытие денежного ящика
  void Storno();//1|сторнирование позиции, с пустым аргументом ошибка
  void Discount();//9|скидка
  void CashIn();//0|внесение
  void CashOut();//3|изъятие
  void Backspace();//5|забой
  void OpenCashBox();//открытие ДЯ
  void Clearing();//7|подбезнал
  void Subtotal();//подитог
  void Pay();//расчет
  void pressUp();//8|вверх
  void pressRight();//6|вправо
  void pressLeft();//4|влево
  void pressDown();//2|вниз
  void pressDecimal();//десятичная точка|поиск по цене
};

#define VALID_ARGUMENT getArgument();		\
  if(argument.isEmpty())			\
    {						\
      InvalidArgument();			\
      return;					\
    }
#endif CASH_H
