#include "codbcload.h"
#include "exceptions.h"
#include "common.h"
#include <QSqlQuery>

//класс обмена через ODBC

CODBCLoad::CODBCLoad(CDatabase* db): CLoad(db), exchange_query(NULL)
{
    GetIniSettings(set);
    exchange_db=QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"alias "<<set.alias;
    exchange_db.setDatabaseName(set.alias);
    //exchange_db->setDatabaseName(qsExDbName);
    qDebug()<<"CODBCLoad::CODBCLoad(CDatabase* db)";
    if(!exchange_db.open()) throw CDBException(DB_NOTCONNECTED);
    exchange_query=new QSqlQuery(exchange_db);
}

CODBCLoad::~CODBCLoad()
{

}



void CODBCLoad::GetIniSettings(LoadSettings& set) throw (CDBException)
{
set.class_loader=LoadSettings::ODBC;
qDebug()<<"CODBCLoad::GetIniSettings(LoadSettings& set)";
cash_db->GetLoaderSettings(set);
CLoad::GetIniSettings(set);
}

//секция выгрузки

void CODBCLoad::UploadRecord(const QString& values) throw (CDBException)
{
    //qDebug()<<query+values;
    if(!exchange_query->exec(query+values)) throw CDBException(DB_EXCHANGE);
}

void CODBCLoad::UploadCashsale() throw (CDBException)
{
    query=set.cashsale;//запрос на вставку в таблицу контрольной ленты
    //qDebug()<<"CODBCLoad::UploadCashsale()";
    cash_db->CashsaleUpload(exset);
}



void CODBCLoad::UploadCashpay() throw (CDBException)
{
    query=set.cashpay;
    cash_db->CashpayUpload(exset);
}

void CODBCLoad::UploadCashdisc() throw (CDBException)
{
query=set.cashdisc;
cash_db->CashdiscUpload(exset);
}

void CODBCLoad::UploadCashGoods() throw (CDBException)
{
query=set.cashgood;
cash_db->CashgoodUpload(exset);
}

void CODBCLoad::UploadCurmoney() throw (CDBException)
{
query=set.curmoney;
cash_db->CurmoneyUpload(exset);
}

void CODBCLoad::UploadCurrests() throw (CDBException)
{
query=set.currests;
cash_db->CurrestsUpload(exset);
}




//секция загрузки
int CODBCLoad::LoadPlu(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadDepart(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadScales(int& res) throw (CDBException)
{

}

//virtual int LoadCost(int& res) throw (CLoadException, CDBException);//загрузка справочника цен
int CODBCLoad::LoadBar(int& res) throw (CDBException)//загрузка справочника ШК
{

}

int CODBCLoad::LoadClassif(int& res) throw (CDBException) //загрузка классификатора
{

}

int CODBCLoad::LoadPricekin(int& res) throw (CDBException)
{

}

//virtual int LoadMesuriment(int& res) throw (CLoadException, CDBException);
int CODBCLoad::LoadCosts(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadSizes(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadPersonal(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadTaxes(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadTax(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadDisccli(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadCliclass(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadCredcard(int& res) throw (CDBException)
{

}

int CODBCLoad::LoadDclislst(int& res) throw (CDBException)
{

}

void CODBCLoad::VerifyLoad() throw (CDBException)
{

}



