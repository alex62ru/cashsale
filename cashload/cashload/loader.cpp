#include "loader.h"
#include "exceptions.h"
//#include <QObject>
//#include <QMessageBox>
//#include <QtEndian>
#include <stdint.h>
//#include "coracle.h"
#include <QSettings>
#include <QTextCodec>



//CLoad::CLoad(COracle *oracle, QObject *parent): QThread(parent), ora(oracle)
CLoad::CLoad(CDatabase *db): cash_db(db)
{
//GetIniSettings(set);
}

CLoad::~CLoad()
{

}

void CLoad::GetIniSettings(LoadSettings& set) throw (CDBException)
{

//QTextCodec::setCodecForLocale(QTextCodec::codecForName(set.locale));
}

QString CLoad::fromStdString(const char *str, int size)
{
    return QTextDecoder(QTextCodec::codecForName("CP866")).toUnicode(str, size==-1?strlen(str):size);
}


int CLoad::Load() throw (CDBException)//дополнить
{
try
{
if(!mutex.tryLock(100)) return -1;
int result(0);
//дополнить
//cash_db->ResetConstraint(ld);
VerifyLoad();
LoadClassif(result); qDebug()<<"LoadClassif = "<<result;
LoadDepart(result); qDebug()<<"LoadDepart = "<<result;
LoadScales(result); qDebug()<<"LoadScales = "<<result;
LoadPricekin(result); qDebug()<<"LoadPricekin = "<<result;
LoadSizes(result); qDebug()<<"LoadSizes = "<<result;
LoadPersonal(result); qDebug()<<"LoadPersonal = "<<result;
LoadDisccli(result); qDebug()<<"LoadDisccli = "<<result; //времнно отключил - долго грузяться
LoadCliclass(result); qDebug()<<"LoadCliclass = "<<result;

LoadDclislst(result); qDebug()<<"LoadDclislst = "<<result;
LoadCredcard(result); qDebug()<<"LoadCredcard = "<<result;
LoadPlu(result); qDebug()<<"LoadPlu = "<<result;
//result+=LoadCosts();
LoadBar(result); qDebug()<<"LoadBar = "<<result;
//LoadTaxes(result); qDebug()<<"LoadTaxes= "<<result;
//LoadTax(result); qDebug()<<"LoadTax = "<<result;

mutex.unlock();
return result;
} catch (CDBException e)
    {
    mutex.unlock();
    //qDebug()<<e.getTitle()<<e.getErrorMessage();
    throw;
    }
  catch (...)
    {
    mutex.unlock();
    throw;
    }
}

int CLoad::Upload(ExchangeSettings es) throw (CDBException)
{
    int result(0);
    exset=es;
    UploadCurrests();
    UploadCashsale();
    UploadCashdisc();
    UploadCashpay();
    UploadCashGoods();\
    UploadCurmoney();
}


//******************************************************


//********************************************
//реализация класса загрузки paradox
//********************************************

const unsigned short CDBLoad::sizes[CDBLoad::es_always_last]={0, 0x0400, 0x0800, 0x0C00, 0x1000};

CDBLoad::CDBLoad(CDatabase *db):CLoad(db), file_head(NULL)
{
  /*
   *enum eSupermag {smPlu=0x01, smBar=0x02, smClassif=0x03, smDiscCard=0x04, smScale=0x05, smSizes=0x06, smPersonal=0x07, smCredCard=0x08, smCredPref=0x09, smClients=0x0A, smDisccli=0x0B,
                    smDiscSum=0x0D, smDcliLst=0x0E, always_last};
   */
map_t.insert(tClassif, smClassif);
map_t.insert(tPlu, smPlu);
map_t.insert(tBar, smBar);
map_t.insert(tDiscCard, smDiscCard);
map_t.insert(tScale, smScale);
map_t.insert(tSizes, smSizes);
map_t.insert(tPersonal, smPersonal);
map_t.insert(tCredCard, smCredCard);
map_t.insert(tCredPref, smCredPref);
map_t.insert(tClients, smClients);
map_t.insert(tDisccli, smDisccli);
map_t.insert(tDiscSum, smDiscSum);
map_t.insert(tDclisLst, smDcliLst);
map_t.insert(tDepart, smDepart);
map_t.insert(tCliclass, smCliclass);
map_t.insert(tTax, smTax);
map_t.insert(tTaxes, smTaxes);
cash_db->GetLoaderSettings(set);//чтение настроек загрузки
//qDebug(set.load_dir.append(set.flg_ins).toLocal8Bit().data());
file_head=new HeadDB;
memset(file_head, 0, sizeof(HeadDB));
b_head=new block_head;
memset(b_head, 0, sizeof(block_head));

}

CDBLoad::~CDBLoad()
{
if (NULL!=file_head) delete file_head;
if(NULL!=b_head) delete b_head;
}

void CDBLoad::ParseHeader()
{
file_data=file.map(0, sizeof(HeadDB));
if(!file_data) throw CDBException(MAP_FAIL);
memcpy(file_head, file_data, sizeof(HeadDB));
if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
//QMessageBox::information(0, tr("void CDBLoad::ParseHeader()"), QString(tr("число %1")).arg(sizeof(HeadDB)));
field.resize(file_head->num_fields);//изменение размера вектора описателя полей
//qDebug(QString("Количество полей: %1").arg(field.size()).toLocal8Bit().data());
file_data=file.map(sizeof(HeadDB), sizeof(field_desc)*file_head->num_fields, QFile::NoOptions);
if(!file_data) throw CDBException(MAP_FAIL);
memcpy(field.data(), file_data, sizeof(field_desc)*file_head->num_fields);
if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
}

void CDBLoad::VerifyLoad() throw (CDBException)
{
//if(!mutex.tryLock()) return;
//mutex.lock();
dir.setPath(set.load_dir);//установка каталого обмена
//QMessageBox::information(0, tr("Инфа"), set.load_dir+set.flg_change);
table.clear();
if(!dir.exists(set.flg_ins) || dir.exists(set.flg_lck))
    {
    //mutex.unlock();
    return;//если файла нет - возврат
    }
if(file.isOpen()) file.close();
file.setFileName(set.load_dir+set.flg_ins);//для загрузки DB cash0XX.db
//qDebug((set.load_dir+set.flg_ins).toLocal8Bit().data());
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->record_length];//массив байтов записи
unsigned int block_ofset;
/*unsigned short block_num*/b_head->next_block=file_head->first_block;
//QMessageBox::information(0, tr("Инфа"), QString(tr("число %1")).arg(file_head->num_use_block));
for(int i=0;i<file_head->num_use_block;i++)//цикл по используемым блокам
    {
    block_ofset=file_head->head_length+sizes[file_head->data_block_size]*(b_head->next_block-1);//смещение блока внутри файла
    file_data=file.map(block_ofset, sizeof(block_head));//проецируем заголовок блока
    if(!file_data) throw CDBException(MAP_FAIL);
    memcpy(b_head, file_data, sizeof(block_head));
    //block_num=b_head.next_block;
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    //field.first();
    //QMessageBox::information(0, tr("Инфа"), QString(tr("число %1")).arg());
    for(int j=0;j<b_head->ofset_last/file_head->record_length+1;j++)//обход по данным внутри блока
        {
        //QMessageBox::information(0, tr("Инфа"), QString(tr("число %1")).arg(block_ofset+sizeof(b_head)+j*file_head->record_length));
        file_data=file.map(block_ofset+sizeof(block_head)+j*file_head->record_length, file_head->record_length);//map записи
        if(!file_data) throw CDBException(MAP_FAIL);
        memcpy(data, file_data, file_head->record_length);
        if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
        cash_t rec;
        rec.table_name=fromStdString(data, field[0].field_size);
        //QMessageBox::information(0, tr("Инфа"), rec.table_name);
        unsigned short stub, stub1(0x0000);
        memcpy(&stub, (data+=field[0].field_size), field[1].field_size);
        rec.data_type=qFromBigEndian<qint16>(stub)^0x8000;//получается именно так преобразовывать, тип справочника
        //QMessageBox::information(0, tr("Инфа"), QString(tr("число %1")).arg(rec.data_type));
        memcpy(&stub, (data+=field[1].field_size), field[2].field_size);
        rec.oper=qFromBigEndian<qint16>(stub)^0x8000;//замена или обновление
        table[rec.data_type]=rec;//добавление в map названий таблиц - закладывается соответствие типа справочника и соответствующего файла
        qDebug(rec.table_name.toLocal8Bit().data());
        //table.insert(rec.data_type, rec);
        data=dt;//возврат в зад
        }
    }
if(dt) delete[] dt;
//QMessageBox::information(0, tr("окончание процедуры"), table[4].table_name);
file.close();
//file.rename(set.load_dir+set.flg_ins, set.load_dir+"~"+set.flg_ins);
//mutex.unlock();
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    //mutex.unlock();
    throw;
    }
}

int CDBLoad::LoadPlu(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadPlu()";
//cash_db->SetTrasactionMode(CDatabase::REPEATABLE_READ);
  QVector<Costs> v_cos;
  Costs cos;
begin_load(tPlu)
Plu rec;
unsigned short stub;
rec.articul=fromStdString(data, field[0].field_size);
//QMessageBox::information(0, tr("Инфа"), rec.description);
rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
rec.mesuriment=fromStdString(data+=field[1].field_size, field[2].field_size);
qint64 stub64;
memcpy(&stub64, data+=field[2].field_size, field[3].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.mespresision, &stub64, sizeof(qint64));
rec.add1=fromStdString(data+=field[3].field_size, field[4].field_size);
rec.add2=fromStdString(data+=field[4].field_size, field[5].field_size);
rec.add3=fromStdString(data+=field[5].field_size, field[6].field_size);
memcpy(&stub64, data+=field[6].field_size, field[7].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.addnum1, &stub64, sizeof(qint64));
memcpy(&stub64, data+=field[7].field_size, field[8].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.addnum2, &stub64, sizeof(qint64));
memcpy(&stub64, data+=field[8].field_size, field[9].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.addnum3, &stub64, sizeof(qint64));
rec.scale=fromStdString(data+=field[9].field_size, field[10].field_size);
memcpy(&stub, (data+=field[10].field_size), field[11].field_size);
rec.groop1=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, (data+=field[11].field_size), field[12].field_size);
rec.groop2=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, (data+=field[12].field_size), field[13].field_size);
rec.groop3=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, (data+=field[13].field_size), field[14].field_size);
rec.groop4=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, (data+=field[14].field_size), field[15].field_size);
rec.groop5=qFromBigEndian<qint16>(stub)^0x8000;
//загрузка цен
cos.articul=rec.articul;
cos.pricelist=1;//преобразовать в константу
memcpy(&stub64, data+=field[15].field_size, field[16].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&cos.price, &stub64, sizeof(qint64));
//cash_db->CostsLoad(cos, CDatabase::eLoadInsert);
v_cos<<cos;
//************
memcpy(&stub, data+=(field[16].field_size+field[17].field_size), field[18].field_size);
rec.department=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=(field[18].field_size+field[19].field_size), field[20].field_size);
rec.deleted=!(qFromBigEndian<qint16>(stub)^0x8000);
//qDebug()<<rec.description;
//end_load(cash_db->PluLoad)
/**/
cash_db->PluLoad(rec, /*db_flags*/CDatabase::eLoadInsert);
                          }
                        }
                        if(dt) delete[] dt;
                        file.close();
                        cash_db->Commit();
//                        return file_head->num_records/*record_count*/;
                            } catch(...)
                            {
                            if(dt) delete[] dt;
                            file.close();
                            res-=result;
                            cash_db->Rollback();
                            throw;
                            }
try
{
  foreach(cos, v_cos)
  {
//  qDebug()<<cos.articul.toUtf8().constData()<<" "<<cos.price;
  cash_db->CostsLoad(cos, CDatabase::eLoadInsert);
  }
  cash_db->Commit();
  return file_head->num_records;
} catch(...)
  {
  cash_db->Rollback();
  throw;
  }
}


int CDBLoad::LoadClassif(int& res) throw (CDBException)
{
qDebug()<<"CDBLoad::LoadClassif()";
//cash_db->StartTransaction();
begin_load(tClassif)
Classif rec;
unsigned short stub(0);
memcpy(&stub, data, field[0].field_size);
rec.groop1=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[0].field_size, field[1].field_size);
rec.groop2=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[1].field_size, field[2].field_size);
rec.groop3=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[2].field_size, field[3].field_size);
rec.groop4=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[3].field_size, field[4].field_size);
rec.groop5=qFromBigEndian<qint16>(stub)^0x8000;
rec.description=fromStdString(data+=field[4].field_size, field[5].field_size);
//qDebug(rec.description.toLocal8Bit().data());
end_load(cash_db->ClassifLoad)
//qDebug()<<"#define end_load(_func)";
}

int CDBLoad::LoadDepart(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadDepart()";
 begin_load(tDepart)
 Depart rec;
 unsigned short stub(0);
 memcpy(&stub, data, field[0].field_size);
 rec.department=qFromBigEndian<qint16>(stub)^0x8000;
 rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
 end_load(cash_db->DepartLoad);
}

int CDBLoad::LoadScales(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadScales()";
begin_load(tScale)
Scales rec;
rec.scale=fromStdString(data, field[0].field_size);
rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
end_load(cash_db->ScalesLoad);
}

int CDBLoad::LoadPricekin(int& res) throw (CDBException)
{
return 0;
}

//virtual int LoadMesuriment() throw (CLoadException);
int CDBLoad::LoadCosts(int& res) throw (CDBException)
{
    return 0;
}

int CDBLoad::LoadBar(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadBar()";
begin_load(tBar);
Barcode rec;
rec.bar=fromStdString(data, field[0].field_size);
rec.articul=fromStdString(data+=field[0].field_size, field[1].field_size);
rec.cardsize=fromStdString(data+=field[1].field_size, field[2].field_size);
qint64 stub64;
memcpy(&stub64, data+=field[2].field_size, field[3].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.quantity, &stub64, sizeof(qint64));
end_load(cash_db->BarLoad);
}

int CDBLoad::LoadSizes(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadSizes()";
begin_load(tSizes)
Sizes rec;
rec.scale=fromStdString(data, field[0].field_size);
rec.cardsize=fromStdString(data+=field[0].field_size, field[1].field_size);
end_load(cash_db->SizesLoad);
}

int CDBLoad::LoadPersonal(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadPersonal()";
begin_load(tPersonal);
Personal rec;
unsigned short stub(0);
memcpy(&stub, data, field[0].field_size);
rec.ident=qFromBigEndian<qint16>(stub)^0x8000;
rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
rec.passw=fromStdString(data+=field[1].field_size, field[2].field_size);
memcpy(&stub, data+=field[2].field_size, field[3].field_size);
rec.accesslevel=qFromBigEndian<qint16>(stub)^0x8000;
end_load(cash_db->PersonalLoad);
}

int CDBLoad::LoadTaxes(int &res) throw (CDBException)
{

}

int CDBLoad::LoadTax(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadTax";
begin_load(tTax)
Tax rec;
rec.articul=fromStdString(data, field[0].field_size);
unsigned short stub(0);
memcpy(&stub, data+=field[0].field_size, field[1].field_size);
rec.taxindex=qFromBigEndian<qint16>(stub)^0x8000;
qint64 stub64;
memcpy(&stub64, data+=field[1].field_size, field[2].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.taxvalue, &stub64, sizeof(qint64));
rec.taxvalue/=100;
memcpy(&stub64, data+=field[2].field_size, field[3].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.taxsum, &stub64, sizeof(qint64));
end_load(cash_db->TaxLoad);
}

int CDBLoad::LoadCliclass(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadCliclass()";
begin_load(tCliclass)
Cliclass rec;
unsigned short stub(0);
memcpy(&stub, data, field[0].field_size);
rec.groop1=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[0].field_size, field[1].field_size);
rec.groop2=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[1].field_size, field[2].field_size);
rec.groop3=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[2].field_size, field[3].field_size);
rec.groop4=qFromBigEndian<qint16>(stub)^0x8000;
memcpy(&stub, data+=field[3].field_size, field[4].field_size);
rec.groop5=qFromBigEndian<qint16>(stub)^0x8000;
rec.barcode=fromStdString(data+=field[4].field_size, field[5].field_size);
qint64 stub64;
memcpy(&stub64, data+=field[5].field_size, field[6].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.discount, &stub64, sizeof(qint64));
rec.discount=1-rec.discount/100;
rec.pricelist=1;//значение по умолчанию
end_load(cash_db->CliclassLoad)
}

int CDBLoad::LoadDisccli(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadDisccli()";
begin_load(tDisccli)
Disccli rec;
rec.barcode=fromStdString(data, field[0].field_size);
rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
qint64 stub64;
memcpy(&stub64, data+=field[1].field_size, field[2].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.discount, &stub64, sizeof(qint64));
rec.discount=1-rec.discount/100;
rec.pricelist=1;
unsigned short stub(0);
memcpy(&stub, data+=field[2].field_size, field[3].field_size);
rec.clientindex=qFromBigEndian<qint16>(stub)^0x8000;
end_load(cash_db->DisccliLoad)
}

int CDBLoad::LoadCredcard(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadCredcard()";
begin_load(tCredCard)
Credcard rec;
unsigned short stub(0);
memcpy(&stub, data, field[0].field_size);
rec.CredCardIndex=qFromBigEndian<qint16>(stub)^0x8000;
rec.description=fromStdString(data+=field[0].field_size, field[1].field_size);
qint64 stub64;
memcpy(&stub64, data+=(field[1].field_size+field[2].field_size+field[3].field_size), field[4].field_size);
stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
memcpy(&rec.limitsum, &stub64, sizeof(qint64));
memcpy(&stub, data+=field[4].field_size, field[5].field_size);
rec.canreturn=qFromBigEndian<qint16>(stub)^0x8000;
rec.soft=fromStdString(data+=field[5].field_size, field[6].field_size);
rec.ident=fromStdString(data+=field[6].field_size, field[7].field_size);
//memcpy(&stub64, data+=field[1].field_size, field[2].field_size);
//stub64=qFromBigEndian<qint64>(stub64)^0x8000000000000000;
//memcpy(&rec.limitsum, &stub64, sizeof(qint64));
//memcpy(&stub, data+=field[2].field_size, field[3].field_size);
//rec.canreturn=qFromBigEndian<qint16>(stub)^0x8000;
//rec.soft=fromStdString(data+=field[3].field_size, field[4].field_size);
//rec.ident=fromStdString(data+=field[4].field_size, field[5].field_size);
end_load(cash_db->CredcardLoad)
}

int CDBLoad::LoadDclislst(int& res) throw (CDBException)
{
  qDebug()<<"CDBLoad::LoadDclislst()";
begin_load(tDclisLst)
Dclilst rec;
rec.codeend=fromStdString(data, field[0].field_size);
rec.codestart=fromStdString(data+=field[0].field_size, field[1].field_size);
end_load(cash_db->DclilstLoad)
}

/*int CDBLoad::Load(int& res) throw (CDBException)
{
int result(0);
result=LoadClassif();
QMessageBox::information(0, tr("Инфа"), tr("Загрузка выполенена"));
return result;
}*/
void CDBLoad::UploadRecord(const QString& values) throw (CDBException)
{

}

void CDBLoad::UploadCashsale() throw (CDBException)//выгрузка контрольной ленты
{

}

void CDBLoad::UploadCashdisc() throw (CDBException)
{

}

void CDBLoad::UploadCashGoods() throw (CDBException)
{

}

void CDBLoad::UploadCashpay() throw (CDBException)
{

}

void CDBLoad::UploadCurmoney() throw (CDBException)
{

}

void CDBLoad::UploadCurrests() throw (CDBException)
{

}


