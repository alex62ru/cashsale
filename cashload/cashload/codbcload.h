#ifndef CODBCLOAD_H
#define CODBCLOAD_H

#include "loader.h"

class CODBCLoad: public CLoad
{
    QSqlDatabase exchange_db;//база обмена
    QSqlQuery* exchange_query;//запрос обмена
    QString qsExDb;//движек базы обмена
    QString qsExDbName;//имя соединения
    QString query;//запрос для выполнения
    LoadSettings ls;
protected:
    //выгрузка продаж
    virtual void UploadCashsale() throw (CDBException);//выгрузка контрольной ленты
    virtual void UploadCashpay() throw (CDBException);//выгрузка проведенных платежей
    virtual void UploadCashdisc() throw (CDBException);//выгрузка проведенных сикдок
    virtual void UploadCashGoods() throw (CDBException);//выгрузка товарного отчета
    virtual void UploadCurmoney() throw (CDBException);//выгрузка таблицы работы с денежными суммами
    virtual void UploadCurrests() throw (CDBException);//выгрузка закрытых сме
    virtual void UploadRecord(const QString& values) throw (CDBException);//загрузка одной строки VALUES(....)
public:
    CODBCLoad(CDatabase* db);
    virtual ~CODBCLoad();
    //загрузка справочников
    virtual int LoadPlu(int& res) throw (CDBException);
    virtual int LoadDepart(int& res) throw (CDBException);
    virtual int LoadScales(int& res) throw (CDBException);
    //virtual int LoadCost(int& res) throw (CLoadException, CDBException);//загрузка справочника цен
    virtual int LoadBar(int& res) throw (CDBException);//загрузка справочника ШК
    virtual int LoadClassif(int& res) throw (CDBException);//загрузка классификатора
    virtual int LoadPricekin(int& res) throw (CDBException);
    //virtual int LoadMesuriment(int& res) throw (CLoadException, CDBException);
    virtual int LoadCosts(int& res) throw (CDBException);
    virtual int LoadSizes(int& res) throw (CDBException);
    virtual int LoadPersonal(int& res) throw (CDBException);
    virtual int LoadTaxes(int& res) throw (CDBException);
    virtual int LoadTax(int& res) throw (CDBException);
    virtual int LoadDisccli(int& res) throw (CDBException);
    virtual int LoadCliclass(int& res) throw (CDBException);
    virtual int LoadCredcard(int& res) throw (CDBException);
    virtual int LoadDclislst(int& res) throw (CDBException);
    virtual void VerifyLoad() throw (CDBException);
    virtual void GetIniSettings(LoadSettings& set) throw (CDBException);//чтение настроек загрузки

};

#endif // CODBCLOAD_H
