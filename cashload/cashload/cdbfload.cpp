#include "cdbfload.h"
#include "exceptions.h"

//реализация dbf загрузки
CDBFLoad::CDBFLoad(CDatabase *db): CLoad(db)
{
//cash_db->GetLoaderSettings(set);//чтение настроек загрузки
file_head=new HeadDBF;
}

CDBFLoad::~CDBFLoad()
{
if (NULL!=file_head) delete file_head;
}


int CDBFLoad::LoadPlu(int& res) throw (CDBException)
{
//QMessageBox::information(0, tr("Инфа"), tr("LoadPlu()"));
dir.setPath(set.load_dir);
if(!dir.exists(table[tPlu].table_name)) return -1;
if (file.isOpen()) file.close();
//file.setFileName(set.plu_path);--ПЕРЕДЕЛАТЬ
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0; i<file_head->CountRec;i++)
    {
    //QMessageBox::information(0, tr("Инфа"), tr("LoadPlu()-1"));
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;//устанавливаем указатель на начало буфера
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    plu_rec.articul=(fromStdString(++data, field_list[0]->FieldSize)).simplified();
    plu_rec.description=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize)).simplified();
    plu_rec.mesuriment=(fromStdString((data+=field_list[1]->FieldSize), field_list[2]->FieldSize)).simplified();
    plu_rec.mespresision=(fromStdString((data+=field_list[2]->FieldSize), field_list[3]->FieldSize).remove(' ')).toFloat();
    plu_rec.add1=(fromStdString((data+=field_list[3]->FieldSize), field_list[4]->FieldSize)).simplified();
    plu_rec.add2=(fromStdString((data+=field_list[4]->FieldSize), field_list[5]->FieldSize)).simplified();
    plu_rec.add3=(fromStdString((data+=field_list[5]->FieldSize), field_list[6]->FieldSize)).simplified();
    plu_rec.addnum1=(fromStdString((data+=field_list[6]->FieldSize), field_list[7]->FieldSize).remove(' ')).toDouble();
    plu_rec.addnum2=(fromStdString((data+=field_list[7]->FieldSize), field_list[8]->FieldSize).remove(' ')).toDouble();
    plu_rec.addnum3=(fromStdString((data+=field_list[8]->FieldSize), field_list[9]->FieldSize).remove(' ')).toDouble();
    plu_rec.scale=(fromStdString((data+=field_list[9]->FieldSize), field_list[10]->FieldSize)).simplified();//т.к. в таблице есть внешний ключ
    plu_rec.groop1=(fromStdString((data+=field_list[10]->FieldSize), field_list[11]->FieldSize).remove(' ')).toFloat();
    plu_rec.groop2=(fromStdString((data+=field_list[11]->FieldSize), field_list[12]->FieldSize).remove(' ')).toFloat();
    plu_rec.groop3=(fromStdString((data+=field_list[12]->FieldSize), field_list[13]->FieldSize).remove(' ')).toFloat();
    plu_rec.groop4=(fromStdString((data+=field_list[13]->FieldSize), field_list[14]->FieldSize).remove(' ')).toFloat();
    plu_rec.groop5=(fromStdString((data+=field_list[14]->FieldSize), field_list[15]->FieldSize).remove(' ')).toFloat();
    plu_rec.price=(fromStdString((data+=field_list[15]->FieldSize), field_list[16]->FieldSize).remove(' ')).toDouble();
    plu_rec.department=(fromStdString((data+=(field_list[16]->FieldSize+field_list[17]->FieldSize)), field_list[18]->FieldSize).remove(' ')).toFloat();
    plu_rec.deleted=(fromStdString((data+=(field_list[18]->FieldSize+field_list[19]->FieldSize)), field_list[20]->FieldSize).remove(' ')).toFloat()?false:true;
    //QMessageBox::information(0, tr("Инфа"), plu_rec.articul+' '+plu_rec.description+' '+plu_rec.add1+' '+QString().setNum(plu_rec.price));
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->PluLoad(plu_rec, static_cast<COracle::eRecord>(rec));
    //cash_db->Commit();
    }
} catch (...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadClassif(int& res) throw (CDBException)
{
if(!dir.exists(table[tClassif].table_name)) return -1;
if (file.isOpen()) file.close();
//file.setFileName(set.clas_path);
if (!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0; i<file_head->CountRec; i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if (!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if (*data==DELETED) continue;
    classif_rec.groop1=(fromStdString(++data, field_list[0]->FieldSize).remove(' ')).toUShort();
    classif_rec.groop2=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize).remove(' ')).toFloat();
    classif_rec.groop3=(fromStdString((data+=field_list[1]->FieldSize), field_list[2]->FieldSize).remove(' ')).toFloat();
    classif_rec.groop4=(fromStdString((data+=field_list[2]->FieldSize), field_list[3]->FieldSize).remove(' ')).toFloat();
    classif_rec.groop5=(fromStdString((data+=field_list[3]->FieldSize), field_list[4]->FieldSize).remove(' ')).toFloat();
    classif_rec.description=(fromStdString((data+=field_list[4]->FieldSize), field_list[5]->FieldSize)).simplified();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->ClassifLoad(classif_rec, static_cast<COracle::eRecord>(rec));
    //cash_db->Commit();
    }
} catch (...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if (dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadDepart(int& res) throw (CDBException)
{
if(!dir.exists(table[tPlu].table_name)) return -1;//исправить
if(file.isOpen()) file.close();
//file.setFileName(set.depart_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0; i<file_head->CountRec; i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    depart_rec.department=(fromStdString(++data, field_list[0]->FieldSize).remove(' ')).toFloat();
    //QMessageBox::information(0, tr("Инфа"), fromStdString(data, field_list[0]->FieldSize).remove(' '));
    depart_rec.description=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize)).simplified();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->DepartLoad(depart_rec, static_cast<COracle::eRecord>(rec));
    //cash_db->Commit();
    //QMessageBox::information(0, tr("Инфа"), depart_rec.description);
    }
} catch (...)
    {
    if(dt) delete[] dt;
    file.close();;
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadScales(int& res) throw (CDBException)
{
if(!dir.exists(table[tScale].table_name)) return -1;
if(file.isOpen()) file.close();
//file.setFileName(set.scales_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0;i<file_head->CountRec; i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(UNMAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    Scales scales_rec;
    scales_rec.scale=(fromStdString(++data, field_list[0]->FieldSize)).simplified();
    scales_rec.description=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize)).simplified();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->ScalesLoad(scales_rec, static_cast<COracle::eRecord>(rec));
    }
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadPricekin(int& res) throw (CDBException)
{
if(!dir.exists(table[tPlu].table_name)) return -1;//исправить
if(file.isOpen()) file.close();
//file.setFileName(set.pricekin_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0;i<file_head->CountRec;i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    Pricekin pricekin_rec;
    pricekin_rec.priecelist=(fromStdString(++data, field_list[0]->FieldSize).remove(' ')).toFloat();
    pricekin_rec.description=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize)).simplified();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->PricekinLoad(pricekin_rec, static_cast<COracle::eRecord>(rec));
    }
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}


int CDBFLoad::LoadCosts(int& res) throw (CDBException)
{
if(!dir.exists(table[tPlu].table_name)) return -1;//исправить
if(file.isOpen()) file.close();
//file.setFileName(set.costs_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0;i<file_head->CountRec;i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    Costs cost_rec;
    cost_rec.articul=fromStdString(++data, field_list[0]->FieldSize).simplified();
    cost_rec.price=fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize).remove(' ').toDouble();
    cost_rec.pricelist=fromStdString((data+=field_list[1]->FieldSize), field_list[2]->FieldSize).remove(' ').toFloat();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->CostsLoad(cost_rec, static_cast<COracle::eRecord>(rec));
    //cash_db->Commit();
    }
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadBar(int& res) throw (CDBException)
{
if(!dir.exists(table[tBar].table_name)) return -1;
if(file.isOpen()) file.close();
//file.setFileName(set.barcode_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0;i<file_head->CountRec;i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    Barcode bar_rec;
    bar_rec.bar=fromStdString(++data, field_list[0]->FieldSize).simplified();
    bar_rec.articul=(fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize)).simplified().remove(QRegExp("^0+"));
    //bar_rec.scale=fromStdString((data+=field_list[1]->FieldSize), field_list[2]->FieldSize).simplified();
    bar_rec.cardsize=fromStdString((data+=field_list[1]->FieldSize), field_list[2]->FieldSize).simplified();
    bar_rec.quantity=fromStdString((data+=field_list[2]->FieldSize), field_list[3]->FieldSize).remove(' ').toDouble();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->BarLoad(bar_rec, static_cast<COracle::eRecord>(rec));
    }
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

int CDBFLoad::LoadSizes(int& res) throw (CDBException)
{
//QMessageBox::information(0, tr("Инфа"), "CDBFLoad::LoadSizes(");
if(!dir.exists(table[tSizes].table_name)) return -1;
if(file.isOpen()) file.close();
//file.setFileName(set.sizes_path);
if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);
char *data(NULL), *dt(NULL);
try
{
ParseHeader();
dt=data=new char[file_head->SizeRec];
for(int i=0;i<file_head->CountRec;i++)
    {
    file_data=file.map(file_head->SizeHeader+i*file_head->SizeRec, file_head->SizeRec);
    if(!file_data) throw CDBException(MAP_FAIL);
    data=dt;
    memcpy(data, file_data, file_head->SizeRec);
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    if(*data==DELETED) continue;
    Sizes size_rec;
    size_rec.scale=fromStdString(++data, field_list[0]->FieldSize).simplified();
    size_rec.cardsize=fromStdString((data+=field_list[0]->FieldSize), field_list[1]->FieldSize).simplified();
    //unsigned char rec(COracle::eUnknown);
    //if(i==0) rec|=COracle::eFirst;
    //if(i==(file_head->CountRec-1)) rec|=COracle::eLast;
    //cash_db->SizesLoad(size_rec, static_cast<COracle::eRecord>(rec));
    }
} catch(...)
    {
    if(dt) delete[] dt;
    file.close();
    cash_db->Rollback();
    throw;
    }
if(dt) delete[] dt;
file.close();
cash_db->Commit();
return file_head->CountRec;
}

void CDBFLoad::VerifyLoad() throw (CDBException)//дополнить
{
lMask=0;
table.clear();//очистка словаря обмена
dir.setPath(set.load_dir);//установка каталога обмена
unsigned short flag(-1);
if(dir.exists(set.flg_upd)) flag=1;
if(dir.exists(set.flg_ins)) flag=0;
/*if(dir.exists(set.plu_path)) table.insert(tPlu, cash_t(set.plu_path, tPlu, flag));
if(dir.exists(set.barcode_path)) table.insert(tBar, cash_t(set.barcode_path, tBar, flag));
if(dir.exists(set.clas_path)) table.insert(tClassif, cash_t(set.clas_path, tClassif, flag));
//if(dir.exists(set.price_path)) table.insert(tPlu, cash_t(set.plu_path, tPlu, flag));
//if(dir.exists(set.depart_path)) table.insert(tPlu, cash_t(set.plu_path, tPlu, flag));
if(dir.exists(set.scales_path)) table.insert(tScale, cash_t(set.scales_path, tScale, flag));
//if(dir.exists(set.pricekin_path)) table.insert(tPlu, cash_t(set.plu_path, tPlu, flag));
//if(dir.exists(set.mesuriment_path)) lMask|=CLoad::MESURIMENT;
//if(dir.exists(set.costs_path)) lMask|=CLoad::COSTS;
//if(dir.exists(set.barcode_path)) lMask|=BAR;
if(dir.exists(set.sizes_path)) table.insert(tSizes, cash_t(set.sizes_path, tSizes, flag));*/
//if((flag!=-1) && (!mutex.tryLock()))  emit DataReady();
}

void CDBFLoad::ParseHeader()
{
//QMessageBox::information(0, tr("Инфа"), tr("ParseHeader()"));
int count=field_list.count();
for (int l=0; l<count; l++)
    {
    delete field_list.last();//возвратить последний и удалить по указателю
    field_list.removeLast();//удалить последний
    }
//QMessageBox::information(0, tr("Инфа"), tr("ParseHeader()-1"));
file_data=file.map(0, sizeof(HeadDBF), QFile::NoOptions);//спроецировать заголовок
if (!file_data) throw CDBException(MAP_FAIL);
//QMessageBox::information(0, tr("Инфа"), tr("ParseHeader()-2"));
memcpy(file_head, file_data, sizeof(HeadDBF));
if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);//освободить проекцию
int field_count = (file_head->SizeHeader-1-sizeof(HeadDBF))/sizeof(FieldDesc);//подсчет количества полей
for (int i=0; i<field_count; i++)
    {
    file_data=file.map(sizeof(HeadDBF)+i*sizeof(FieldDesc), sizeof(FieldDesc));//спроецировать описатель поля
    if(!file_data) throw CDBException(MAP_FAIL);
    field_head=new FieldDesc;//выделить память под заголовок поля
    memcpy(field_head, file_data, sizeof(FieldDesc));// перенести данные о поле
    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);
    field_list.append(field_head);
    }
//QMessageBox::information(0, tr("Колво строк"), QString().arg(file_head->SizeHeader));
}

int  CDBFLoad::LoadPersonal(int& res) throw (CDBException)
{
    return 0;
}

int CDBFLoad::LoadTaxes(int &res) throw (CDBException)
{
  return 0;
}

int CDBFLoad::LoadTax(int& res) throw (CDBException)
{
    return 0;
}

int CDBFLoad::LoadCliclass(int& res) throw (CDBException)
{
    return 0;
}

int CDBFLoad::LoadDisccli(int& res) throw (CDBException)
{
    return 0;
}

int CDBFLoad::LoadCredcard(int& res) throw (CDBException)
{
    return 0;
}

int CDBFLoad::LoadDclislst(int& res) throw (CDBException)
{
    return 0;
}

//void CDBFLoad::UploadRecord(const QString& values) throw (CDBException)
//{

//}
