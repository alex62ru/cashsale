#ifndef CDBFLOAD_H
#define CDBFLOAD_H

#include "loader.h"

class CDBFLoad: public CLoad
{
private:
    //QDir dir;//каталог загрузки
    #pragma pack(push)  /* push current alignment to stack */
    #pragma pack(1) /* set alignment to 1 byte boundary */
    struct HeadDBF //заголовок файла
    {
     char Signature;
     char LastModDate[3];
     long CountRec;
     short SizeHeader;
     short SizeRec;
     short Reserved0;
     char Transaction;
     char Encrypt;
     char Reserved1[12];
     char MDX;
     char CodePage;
     short Reserved2;
     //char LangDrv[32];
     //long Reserved3;
    };
    struct FieldDesc //дескриптор поля
    {
     char Name[11];
     char FieldType;
     long Reserved0;
     char FieldSize;
     char AddFieldSize;
     char Reserved1[13];
     char FlagMDX;
    };
    #pragma pack(pop)
enum {IN_USE=0x20, DELETED=0x2A};
//QFile file; //фаил с данными
//uchar* file_data;
HeadDBF* file_head;
FieldDesc* field_head;
QList<FieldDesc*> field_list;
unsigned long lMask;//маска загрузки
void ParseHeader();//разобрать заголовок
public:
    CDBFLoad(CDatabase *db);
    virtual ~CDBFLoad();
protected:
    virtual int LoadPlu(int& res) throw (CDBException);
    virtual int LoadClassif(int& res) throw (CDBException);
    virtual int LoadDepart(int& res) throw (CDBException);
    virtual int LoadScales(int& res) throw (CDBException);
    virtual int LoadPricekin(int& res) throw (CDBException);
    //virtual int LoadMesuriment(int& res) throw (CLoadException, CDBException);
    virtual int LoadCosts(int& res) throw (CDBException);
    virtual int LoadBar(int& res) throw (CDBException);
    virtual int LoadSizes(int& res) throw (CDBException);
    virtual int LoadPersonal(int& res) throw (CDBException);
    virtual int LoadTaxes(int& res) throw (CDBException);
    virtual int LoadTax(int& res) throw (CDBException);
    virtual int LoadDisccli(int& res) throw (CDBException);
    virtual int LoadCliclass(int& res) throw (CDBException);
    virtual int LoadCredcard(int& res) throw (CDBException);
    virtual int LoadDclislst(int& res) throw (CDBException);
    virtual void VerifyLoad() throw (CDBException);
        //секция выгрузки

};

#endif // CDBFLOAD_H
