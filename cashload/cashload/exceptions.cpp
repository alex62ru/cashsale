#include "exceptions.h"
#include "database.h"

CException::CException(eException ee): e(ee)
{

}

CException::CException()
{

}
 
CException::~CException()
{

}

QString CException::CharToString(const char* str)//пришлось реализовать, т.к. иначе невозможно создавать экземпляры
{

}

const QString& CException::getErrorMessage() const throw()
{
  return errorMsg;
}

const QString& CException::getTitle() const throw()
{
  return title;
}

//************************************
CDBException::CDBException()
{

}


CDBException::CDBException(eException ee): CException(ee)
{
    title=QObject::tr("Error file operation");
    switch (e)
    {
    case FILE_DONT_OPEN: errorMsg=QObject::tr("File dont opened");
        break;
    case MAP_FAIL: errorMsg=QObject::tr("Error file mapping");
        break;
    case UNMAP_FAIL: errorMsg=QObject::tr("Error file close map");
        break;
    case DB_NOTCONNECTED: errorMsg=QObject::tr("Database not connected");
        title=QObject::tr("Error database");
        break;
      default: errorMsg=QObject::tr("Unknown error");
    }

}

CDBException::CDBException(CDatabase* db): CException()
{

}

CDBException::~CDBException()
{

}

const QString& CDBException::getErrorMessage() const throw()
{
  return CException::getErrorMessage();
}

const QString& CDBException::getTitle() const throw()
{
  return CException::getTitle();
}



//******************//
CPGDBException::CPGDBException():CDBException()
{
  errorMsgList<<QObject::tr("Error database object");
  title=QObject::tr("Error database Postgree");
}

CPGDBException::CPGDBException(eException ee): CDBException(ee)
{

}

CPGDBException::CPGDBException(CDatabase* db): CDBException(db), cdb(static_cast<CPGDatabase*>(db))
{
  title=QObject::tr("Error database Postgree");
  /* if (!cdb)
     {
     errorMsgList<<QObject::tr("Error message: ")<<QObject::tr("Error database connection");
     return;
     }
     if (PQstatus(cdb->getConnection())!=CONNECTION_OK)
     {
     errorMsgList<<QObject::tr("Error message: ")<<QObject::tr("Errror database connection");
     return;
     }*/
  //errorMsg=QObject::tr("Result status: ")+PQresStatus(PQresultStatus(cdb->getResult()));
  const char *error=PQresultErrorMessage(cdb->getResult());
  errorMsg+="\r\n"+QObject::tr("Error RESULT message: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_SEVERITY);
  errorMsg+="\r\n"+QObject::tr("Error severity: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_SQLSTATE);
  errorMsg+="\r\n"+QObject::tr("SQLSTATE code: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_MESSAGE_PRIMARY);
  errorMsg+="\r\n"+QObject::tr("Primary human-readable error message: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_MESSAGE_DETAIL);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_MESSAGE_DETAIL: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_MESSAGE_HINT);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_MESSAGE_HINT: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_STATEMENT_POSITION);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_STATEMENT_POSITION: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_INTERNAL_POSITION);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_INTERNAL_POSITION: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_INTERNAL_QUERY);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_INTERNAL_QUERY: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_CONTEXT);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_CONTEXT: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_SOURCE_FILE);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_SOURCE_FILE: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_SOURCE_LINE);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_SOURCE_LINE: ")+CharToString(error);
  error=PQresultErrorField(cdb->getResult(), PG_DIAG_SOURCE_FUNCTION);
  errorMsg+="\r\n"+QObject::tr("PG_DIAG_SOURCE_FUNCTION: ")+CharToString(error);
  /*errorMsgList<<QObject::tr("Error message: ")<<PQerrorMessage(cdb->getConnection());
    errorMsgList<<QObject::tr("Result status: ")<<PQresStatus(PQresultStatus(cdb->getResult()));*/
}

CPGDBException::~CPGDBException()
{

}

QString CPGDBException::CharToString(const char* str)
{
  if(!str) return QString("");
  Q_ASSERT(str);//елси указатель NULL
  return QTextDecoder(QTextCodec::codecForName("UTF-8")).toUnicode(str, strlen(str));
}

const QString& CPGDBException::getErrorMessage() const throw()
{
  return CDBException::getErrorMessage();
}

const QString& CPGDBException::getTitle() const throw()
{
  return CDBException::getTitle();
}

//*****************************

CPortException::CPortException(): CException()
{
  title=QObject::tr("COM port exception");
  //errorMsg=QTextCodec::codecForName("UTF-8")->fromUnicode(strerror(errno));
  const char *error=strerror(errno);//приведен правильный вариант декодировки символов, не на всех ОС работает
  errorMsg=CharToString(error);
  //errorMsg=error;
  //errorMsg=QString::fromLatin1(strerror(errno));
}

CPortException::CPortException(eException ee): CException(ee)
{
  title=QObject::tr("COM port exception");
}

CPortException::~CPortException()
{

}

QString CPortException::CharToString(const char* str)
{
  if(!str) return QString("");
  Q_ASSERT(str);//елси указатель NULL
  return QTextDecoder(QTextCodec::codecForName("UTF-8")).toUnicode(str, strlen(str));
}

const QString& CPortException::getErrorMessage() const throw()
{
  return CException::getErrorMessage();
}

const QString& CPortException::getTitle() const throw()
{
  return CException::getTitle();
}
