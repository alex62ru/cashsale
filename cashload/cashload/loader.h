#ifndef LOADER_H
#define LOADER_H

//#include "coracle.h"
#include "database.h"
#include <QObject>
#include <QDir>
#include <QFile>
#include <QVector>
#include <QMutex>
#include <QThread>
#ifdef OS_WINDOWS
#include <QSqlDatabase>
#endif
//class QObject;
class QThread;
//class QDir;
//class QFile;
class CDBException;//исключения загрузки
//class ELoadException;
class CDatabase;
class CPGDatabase;
class QSqlQuery;



class CLoad//предок загрузки
{
protected:
    //COracle *ora;//указатель на движек
    CDatabase *cash_db;//указатель на движек
    Plu plu_rec;
    Classif classif_rec;
    Depart depart_rec;
    CDatabase::eBitMask/*int*/ db_flags;//первая или последняя запись
    LoadSettings set;//настройки обмена
    QFile file; //фаил с данными
    QDir dir;//каталог загрузки
    uchar* file_data;
    enum eTable {tClassif=0x00000001, tScale=0x00000002, tBar=0x00000004, tDiscCard=0x00000008, tSizes=0x00000010, tPlu=0x00000020, tPersonal=0x00000040, tCredCard=0x00000080,
                 tCredPref=0x00000100, tClients=0x00000200, tDisccli=0x00000400, tDiscSum=0x00000800, tDclisLst=0x00001000, tCliclass=0x00002000, tTax=0x00004000, tDepart=0x00008000, tTaxes=0x00010000,
                 tCurrests=0x00020000, tCashsale=0x00040000, tCashpay=0x00080000, tCashdisc=0x00200000, tCurmoney=0x00400000,
                 eChange=0x80000000/*старший разряд 1-замена, 0-обновление*/};//присутствует в ответе, 1 - таблиза изменялась
    struct cash_t
    {
    QString table_name;
    unsigned short data_type;
    unsigned short oper;
    cash_t(QString tn, unsigned short dt, unsigned short o): table_name(tn), data_type(dt), oper(o)
        {
        }
    cash_t(): table_name(), data_type(0), oper(0)
        {
        }
    };
    QMap<unsigned short, cash_t> table;//список таблиц
    virtual int LoadPlu(int& res) throw (CDBException) = 0;
    virtual int LoadDepart(int& res) throw (CDBException) = 0;
    virtual int LoadScales(int& res) throw (CDBException) = 0;
    //virtual int LoadCost(int& res) throw (CLoadException, CDBException);//загрузка справочника цен
    virtual int LoadBar(int& res) throw (CDBException) = 0;//загрузка справочника ШК
    virtual int LoadClassif(int& res) throw (CDBException) = 0;//загрузка классификатора
    virtual int LoadPricekin(int& res) throw (CDBException) = 0;
    //virtual int LoadMesuriment(int& res) throw (CLoadException, CDBException) = 0;
    virtual int LoadCosts(int& res) throw (CDBException) = 0;
    virtual int LoadSizes(int& res) throw (CDBException) = 0;
    virtual int LoadPersonal(int& res) throw (CDBException) = 0;
    virtual int LoadTaxes(int& res) throw (CDBException) = 0;
    virtual int LoadTax(int& res) throw (CDBException) = 0;
    virtual int LoadDisccli(int& res) throw (CDBException) = 0;
    virtual int LoadCliclass(int& res) throw (CDBException) = 0;
    virtual int LoadCredcard(int& res) throw (CDBException) = 0;
    virtual int LoadDclislst(int& res) throw (CDBException) = 0;
    virtual void VerifyLoad() throw (CDBException) = 0;
    virtual void GetIniSettings(LoadSettings& set) throw (CDBException);
    QString fromStdString(const char * str, int size = -1);
    //секция выгрузки
    ExchangeSettings exset;
    virtual void UploadCashsale() throw (CDBException) = 0;//выгрузка контрольной ленты
    virtual void UploadCashpay() throw (CDBException) = 0;//выгрузка проведенных платежей
    virtual void UploadCashdisc() throw (CDBException) = 0;//выгрузка проведенных сикдок
    virtual void UploadCashGoods() throw (CDBException) = 0;//выгрузка товарного отчета
    virtual void UploadCurmoney() throw (CDBException) = 0;//выгрузка таблицы работы с денежными суммами
    virtual void UploadCurrests() throw (CDBException) = 0;//выгрузка закрытых смен
    virtual void UploadRecord(const QString& values) throw (CDBException) = 0;//загрузка одной строки VALUES(....)
public:
    friend class CDatabase;
    friend class CPGDatabase;
    CLoad(CDatabase* db);
    QMutex mutex;
    virtual ~CLoad();
    virtual int Load() throw (CDBException);
    virtual int Upload(ExchangeSettings es = ExchangeSettings()) throw (CDBException);
};


//***********************************************************
//загрузка paradox
//***********************************************************

class CDBLoad: public CLoad
{
private:
              /*Paradox uses little endian format (the natural 80x86 format) for
              control structures like file headers and block headers. It uses
              modified big endian format for numeric data, i.e., type N, $, D,
              and S fields in data records.*/
    //QDir dir;//каталог загрузки
    #pragma pack(push)  /* push current alignment to stack */
    #pragma pack(1) /* set alignment to 1 byte boundary */
    struct HeadDB
    {
    unsigned short record_length;
    unsigned short head_length;
    unsigned char file_type;
    unsigned char data_block_size;
    unsigned long num_records;
    unsigned short num_use_block;//Number of blocks in use
    unsigned short total_blocks;//Total blocks in file
    unsigned short first_block;
    unsigned short last_use_block;
    unsigned char reserved1[0x21-0x12];
    unsigned char num_fields;
    unsigned char reserved2;
    unsigned char num_key_fields;
    unsigned char reserved3[0x4D-0x24];
    unsigned short first_free_block;//совбодный блок
    unsigned char reseved3[0x78-0x4F];
    };
    HeadDB* file_head;//заголовок
    struct field_desc
    {
    unsigned char field_type;
    unsigned char field_size;
    };
    struct block_head
    {
    unsigned short next_block; //Next block number (Zero if last block)
    unsigned short prev_block; //Previous block number (Zero if first block)
    signed short ofset_last; //Offset of last record in block.
    //signed long ofset_last; //Offset of last record in block.
    //data block
    };
    enum eSupermag {smPlu=0x01, smBar=0x02, smClassif=0x03, smDiscCard=0x04, smScale=0x05, smSizes=0x06, smPersonal=0x07, smCredCard=0x08, smCredPref=0x09, smClients=0x0A, smDisccli=0x0B,
                    smDiscSum=0x0D, smDcliLst=0x0E, smDepart=0x0F/*уточнить!!!!*/, smPricekin=0x10/*уточнить*/, smCliclass=0x16, smTaxes=0x17, smTax=0x19, always_last};
    block_head* b_head;
    QMap<eTable,eSupermag> map_t;//хранит соответствие ключа - таблицы и внутреннего представлния имени таблицы из файла cash***.db
    QVector<field_desc> field;//The field description array contains two bytes for each field. The first byte contains the field type code.
    QStringList field_name;//Field names are in field number sequence. Each field name is a null-terminated string (00h marks the end of the string).
    /*struct cash_db
    {
    QString table_name;
    unsigned short data_type;
    unsigned short oper;
    };*/
    //QMap<unsigned short, cash_db> table;//список таблиц
    #pragma pack(pop)   /* restore original alignment from stack */
    enum eSize{b1K=0x01, b2K=0x02, b3K=0x03, b4K=0x04, es_always_last=0x05};//
    static const unsigned short sizes[es_always_last];//={0, 0x0400, 0x0800, 0x0C00,0x1000};
    void ParseHeader();//разобрать заголовок
public:
    CDBLoad(CDatabase *db);
    virtual ~CDBLoad();
protected:
    virtual int LoadPlu(int& res) throw (CDBException);
    virtual int LoadClassif(int& res) throw (CDBException);
    virtual int LoadDepart(int& res) throw (CDBException);
    virtual int LoadScales(int& res) throw (CDBException);
    virtual int LoadPricekin(int& res) throw (CDBException);
    //virtual int LoadMesuriment(int& res) throw (CLoadException, CDBException);
    virtual int LoadCosts(int& res) throw (CDBException);
    virtual int LoadBar(int& res) throw (CDBException);
    virtual int LoadSizes(int& res) throw (CDBException);
    virtual int LoadPersonal(int& res) throw (CDBException);
    virtual int LoadTaxes(int& res) throw (CDBException);
    virtual int LoadTax(int& res) throw (CDBException);
    virtual int LoadDisccli(int& res) throw (CDBException);
    virtual int LoadCliclass(int& res) throw (CDBException);
    virtual int LoadCredcard(int& res) throw (CDBException);
    virtual int LoadDclislst(int& res) throw (CDBException);
    virtual void VerifyLoad() throw (CDBException);
        //секция выгрузки
    virtual void UploadRecord(const QString& values) throw (CDBException);
    virtual void UploadCashsale() throw (CDBException);//выгрузка контрольной ленты
    virtual void UploadCashpay() throw (CDBException) = 0;//выгрузка проведенных платежей
    virtual void UploadCashdisc() throw (CDBException) = 0;//выгрузка проведенных сикдок
    virtual void UploadCashGoods() throw (CDBException) = 0;//выгрузка товарного отчета
    virtual void UploadCurmoney() throw (CDBException) = 0;//выгрузка таблицы работы с денежными суммами
    virtual void UploadCurrests() throw (CDBException) = 0;//выгрузка закрытых смен
};



//макросы загрузки DB
#define begin_load(_eTable) dir.setPath(set.load_dir);\
                            int result(_eTable);\
                            qDebug((set.load_dir+table[map_t[_eTable]].table_name+QObject::tr(".db")).toLocal8Bit().data());\
                            db_flags=CDatabase::eNull;\
                            /*необходимо проверить формирование признака замены или обновления*/\
                            db_flags=table[map_t[_eTable]].oper?CDatabase::eLoadInsert:CDatabase::eLoadUpdate;\
                            if(!file.isOpen()) file.close();\
                            file.setFileName(set.load_dir+table[map_t[_eTable]].table_name+QObject::tr(".db"));\
                            if(!file.open(QIODevice::ReadOnly)) throw CDBException(FILE_DONT_OPEN);\
                            char *data(NULL), *dt(NULL);\
                            try\
                            {\
                            cash_db->StartTransaction();\
                            ParseHeader();\
                            dt=data=new char[file_head->record_length];\
                            unsigned int block_ofset;\
                            b_head->next_block=file_head->first_block;\
                            int record_count(0);\
                            for(int i=0;i<file_head->num_use_block;i++)\
                                {\
                                block_ofset=file_head->head_length+sizes[file_head->data_block_size]*(b_head->next_block-1);\
                                file_data=file.map(block_ofset, sizeof(block_head));\
                                if(!file_data) throw CDBException(MAP_FAIL);\
                                memcpy(b_head, file_data, sizeof(block_head));\
                                if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);\
                                int iter(1);\
                                iter=b_head->ofset_last/file_head->record_length+1;\
                                if(b_head->ofset_last<0) iter=0;\
                                if(b_head->ofset_last==0) iter=1;\
                                for(int j=0;j<iter/*+1*/;j++)\
                                    {\
                                    data=dt;\
                                    file_data=file.map(block_ofset+sizeof(block_head)+j*file_head->record_length, file_head->record_length);\
                                    if(!file_data) throw CDBException(MAP_FAIL);\
                                    memcpy(data, file_data, file_head->record_length);\
                                    if(!file.unmap(file_data)) throw CDBException(UNMAP_FAIL);

#define end_load(_func) _func(rec, /*db_flags*/CDatabase::eLoadInsert);\
                          }\
                        }\
                        if(dt) delete[] dt;\
                        file.close();\
                        cash_db->Commit();\
                        return file_head->num_records/*record_count*/;\
                            } catch(...)\
                            {\
                            if(dt) delete[] dt;\
                            file.close();\
                            res-=result;\
                            cash_db->Rollback();\
                            throw;\
                            }




//278   unsigned char count(COracle::eUnknown);\




#endif // LOADER_H
