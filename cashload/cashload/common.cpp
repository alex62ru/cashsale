#include "common.h"
//#include "pg_type.h"

#define TEXTOID			25
#define BYTEAOID		17
#define VARCHAROID		1043
#define TIMESTAMPTZOID	1184

CGood::CGood(int p, const QString& art, const QString& des, const QString& cs, int d, const QString& pr, const QString& q, const QString& s, const QString& dis, const QString& tl): pos(p), articul(art), description(des), cardsize(cs), price(pr), quantity(q), sum(s), department(d), discount(dis), total(tl)
{

}

/*CGood::CGood(int p, const QString& art, const QString& des, double pr, double q, const QString& mes, double s, int d): pos(p), articul(art), description(des), price(QString("%1 %2").arg(pr, 0, 'F', 2).arg(QObject::tr("usd"))), quantity(QString("%1 %2").arg(q, 0, 'F', 3).arg(mes)), sum(QString("%1 %2").arg(s, 0, 'F', 2).arg(QObject::tr("usd"))), department(d)
{

}*/


CGood::~CGood()
{

}

CGood::CGood(): pos(0), articul(""), description(""), cardsize("NOSIZE"), price(""), quantity(""), mesuriment(""), department(0), discount(""), total("")
{
  //CGood(0, "", "", 0, 0, "", 0, 0);
}

/*CGood::CGood(const CGood& goo): pos(goo.pos), articul(goo.articul), description(goo.description), cardsize(goo.cardsize), price(goo.price), quantity(goo.quantity), mesuriment(goo.mesuriment), sum(goo.sum), department(goo.department)
{

}

CGood& CGood::operator=(const CGood& goo)
{
  if(this==&goo) return *this;
  qDebug()<<"CGood::operator=(const CGood& goo)";
  pos=goo.pos;
  articul=goo.articul;
  description=goo.description;
  cardsize=goo.cardsize;
  price=goo.price;
  quantity=goo.quantity;
  mesuriment=goo.mesuriment;
  sum=goo.sum;
  department=goo.department;
  qDebug()<<description.toUtf8().constData();
  return *this;
  }*/

CInsertParams::CInsertParams()
{

}

CInsertParams::~CInsertParams()
{

}

void CInsertParams::ApendValue(const char *value, short oid)
{
    params.append(Param(QString(value), oid));
}

QString CInsertParams::GetInsertValues()
{
    QString ret("(");
    QString tmp;
    foreach(Param p, params)
    {
    switch (p.oid)
        {
        case TEXTOID:
        case BYTEAOID:
        case VARCHAROID:
            ret.append("'");
            ret.append(p.value);
            ret.append("'");
            break;
        case TIMESTAMPTZOID:
        ret.append("'");
        tmp=(p.value.section(QRegExp("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)[0-9]{2}"), 0));//установка даты
        tmp.replace(QRegExp("[- /.]"), "-");//заменить разделитель даты
        ret.append(tmp);
        ret.append("'");
        ret.append(", ");
        break;
        default:
        ret.append(p.value);
            break;
        }
    ret.append(", ");
    }
    ret.truncate(ret.length()-2);
    ret.append(")");
    return ret;
}

void CInsertParams::Clear()
{
    params.clear();
}
