#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include "common.h"

class CDatabase;
class CPGDatabase;

enum eException{OPERATION_OK, FILE_DONT_OPEN, MAP_FAIL, UNMAP_FAIL, DB_NOTCONNECTED, PORT_DONT_OPEN, DB_EXCHANGE/*общее исключение для базы обмена*/};

class CException //общий класс всех исключений
{
 protected:
  //CDatabase* cdb;
  eException e;
  QString errorMsg;//сообщение об ошибке
  QString title;//заголовок окна сообщения
  virtual QString CharToString(const char* str);//преобразует указатель на строку в объект QString в соответствии с некотрой кодировкой
 public:
  CException(eException ee);
  CException();
  virtual ~CException();
  virtual const QString& getErrorMessage() const throw();
  virtual const QString& getTitle() const throw();
};

class CDBException: public CException //общий класс для всех баз данных внутренних и внешних
{
 public:
  CDBException();//вызывается, когда объекта-соединения еще нет
  CDBException(eException ee);
  CDBException(CDatabase* db);
  virtual ~CDBException();
  virtual const QString& getErrorMessage() const throw();
  virtual const QString& getTitle() const throw();
};

class CPGDBException: public CDBException
{
 private:
  virtual QString CharToString(const char* str);
  CPGDatabase* cdb;
  QStringList errorMsgList;//многострочное описание ошибки
 public:
  CPGDBException(eException ee);
  CPGDBException();
  CPGDBException(CDatabase* db);
  virtual ~CPGDBException();
  virtual const QString& getErrorMessage() const throw();
  virtual const QString& getTitle() const throw();
};

class CPortException: public CException //класс исключений внешних устройств
{
  virtual QString CharToString(const char* str);
 public:
  CPortException();//получает сообщений об ошибке из глобальной переменной errno
  CPortException(eException ee);
  virtual ~CPortException();
  virtual const QString& getErrorMessage() const throw();
  virtual const QString& getTitle() const throw();
};

#endif //EXCEPTIONS_H
