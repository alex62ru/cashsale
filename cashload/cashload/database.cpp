#include "database.h"
#include "exceptions.h"
#include "loader.h"

#define MAX_STR_LENGTH 128

CDatabase::CDatabase(const QString &con, eBitMask bm): con_string(con), bitSettings(bm)
{

}

void CDatabase::SetLoader(CLoad *ld)
{
    loader=ld;
}

CPGDatabase::CPGDatabase(const QString& con, eBitMask bm) throw (CPGDBException): CDatabase(con, bm), connection(NULL), wait_connection(1), locale(NULL)// устанавливаем по умолчанию ожидание в 100 мс
{
  bitSettings=bm;
  //в следующей строке ошибка
  //if (PQstatus(get_con()) != CONNECTION_OK) throw CPGDBException();
  connection=getConnection();//создание соединения
  get_waitcon();
  set_locale();
  qDebug()<<"Protocol version: "<<PQprotocolVersion(connection);
  PreparedStatement();
}

CPGDatabase::~CPGDatabase()
{
  if(locale) delete locale;//нужно проверить
  PQfinish(connection);
  qDebug()<<"CPGDatabase::~CPGDatabase()";
}



PGconn* CPGDatabase::getConnection() throw(CPGDBException)
{
  connection=PQconnectdb(con_string.toUtf8().constData());
  if(PQstatus(connection)!=CONNECTION_OK)
    {
#ifdef OS_WINDOWS
      Sleep(wait_connection*1000);
#endif
#ifdef OS_LINUX
      sleep(wait_connection);
#endif
      PQreset(connection);
    }
  if(PQstatus(connection)!=CONNECTION_OK) throw CPGDBException();//если коннект не установился, исключение
  return connection;
}

PGresult* CPGDatabase::getResult() throw()
{
  return result;
}


void CPGDatabase::prepare(const QString& query)//извлекает из базы данных список запросов, разбирает их, подсчитывает количество аргументов и подготавливает в движке
{
  try
    {
  result=PQexec(connection, query.toUtf8().constData());
  //qDebug(query.toUtf8().constData());
  if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);
  int num_strvalue=PQfnumber(result, QObject::tr("strvalue").toUtf8().constData()); if(num_strvalue==-1) throw CPGDBException(this);
  //qDebug("int num_strvalue");
  int num_name=PQfnumber(result, QObject::tr("var_name").toUtf8().constData()); if(num_name==-1) throw CPGDBException(this);
  PGresult* res;
  char *name, *value;
  //qDebug("for(int i=0;i<PQntuples(result);i++)");
  for(int i=0;i<PQntuples(result);i++)
    {
      name=PQgetvalue(result, i, num_name);
      value=PQgetvalue(result, i, num_strvalue);
      qDebug()<<name;
      qDebug()<<value;
      qDebug()<<QString(value).count(QChar('$'));
      res=PQprepare(connection, name, value, QString(value).count(QChar('$')), NULL);
      ASSERT_COMMAND_OK(res);
    }
  PQclear(result);
    } catch (...)
    {
        PQclear(result);
        throw;
    }
}



void CPGDatabase::PreparedStatement() throw (CPGDBException)
{
  try
    {
      QString query;
      if(bitSettings & eLoad)
	{
	  query=QObject::tr("select var_name, strvalue from cashsettings where ((var_name like '%upd') or (var_name like '%ins')) and (var_name not like 'flg%')");
	  prepare(query);
      query=QObject::tr("select var_name, strvalue from cashsettings where (var_name like 'sd_%')");
      prepare(query);
	}
      if(bitSettings & eFront)
	{
	  query=QObject::tr("select var_name, strvalue from cashsettings where (var_name like 'ss_%')");
	  prepare(query);	
	  //query=QObject::tr("select face_text, shortcut, face_font, icon_path from buttons where object_name=%1");
	  //prepare(query);//подготовка запросов на извлечение информации о кнопках
	}
    }catch (...)
    {
      PQclear(result);
      throw;
    }
}

CGood CPGDatabase::get_pos(const char* pos) throw (CPGDBException)
{
  Q_CHECK_PTR(pos);
  field_count=1;
  paramValues.resize(field_count); paramLength.resize(field_count); paramFormats.resize(field_count);
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=strlen(pos); paramValues[0]=pos;
  try
    {
      //qDebug()<<"CPGDatabase::get_pos(const char* pos)"<<pos;
      result=PQexecPrepared(connection, "ss_pos", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);
      CGood res(getSingleStr(0).toInt(), getSingleStr(1), getSingleStr(2), getSingleStr(3), getSingleStr(4).toInt(), getSingleStr(5), getSingleStr(6), getSingleStr(7), getSingleStr(8), getSingleStr(9));//извлечение текущей строки товара
      PQclear(result);
      qDebug()<<"CPGDatabase::get_pos(const char* pos) END"<<res.description.toUtf8().constData();
      return res;//пока нет размера
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

int CPGDatabase::get_waitcon() throw (CPGDBException)
{
  try
  {
  result=PQexec(getConnection(), "select intvalue from cashsettings where var_name='wait_con'");
  if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);
  PQclear(result);
  } catch (...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::ClassifLoad(const Classif& rec, eBitMask ss) throw (CPGDBException)
{
  field_count=6;
  paramValues.clear(); paramFormats.clear(); paramLength.clear();
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.groop1);
  paramFormats[0]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.groop1); paramValues[0]=(char*)&temp1;
  unsigned short temp2=htons(rec.groop2);
  paramFormats[1]=RFORMAT_BINARY; paramLength[1]=sizeof(rec.groop2); paramValues[1]=(char*)&temp2;
  unsigned short temp3=htons(rec.groop3);
  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.groop3); paramValues[2]=(char*)&temp3;
  unsigned short temp4=htons(rec.groop4);
  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.groop4); paramValues[3]=(char*)&temp4;
  unsigned short temp5=htons(rec.groop5);
  paramFormats[4]=RFORMAT_BINARY; paramLength[4]=sizeof(rec.groop5); paramValues[4]=(char*)&temp5;
  char temp6[MAX_STR_LENGTH]; memset(temp6, 0, MAX_STR_LENGTH);
  memcpy(temp6, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[5]=RFORMAT_TEXT; paramLength[5]=rec.description.length(); paramValues[5]=temp6;
try
  {
    if(ss & eLoadUpdate)
      {
      result=PQexecPrepared(connection, "classif_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      }
    if(ss & eLoadInsert)//если идет замена или заполнение справочника, возможно добавление новой позиции через обновление несуществующей(круто загнуль надеюсь потом сам разберусь
      {
      result=PQexecPrepared(connection, "classif_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
      }
    //qDebug()<<"CPGDatabase::ClassifLoad() "<<"result="<<PQresultStatus(result)<<PQerrorMessage(connection);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::DepartLoad(const Depart& rec, eBitMask ss) throw (CPGDBException)
{
  field_count=2;
  paramValues.clear(); paramFormats.clear(); paramLength.clear();
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.department);
  paramLength[0]=sizeof(rec.department); paramFormats[0]=RFORMAT_BINARY; paramValues[0]=(char*)&temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramLength[1]=rec.description.length(); paramFormats[1]=RFORMAT_TEXT; paramValues[1]=temp2; /*0-string*/
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "depart_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "depart_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    //qDebug()<<"CPGDatabase::ScalesLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
    {
    PQclear(result);
    throw;
    }
}

void CPGDatabase::ScalesLoad(const Scales& rec, eBitMask ss) throw (CPGDBException)
{
field_count=2;
paramValues.clear(); paramFormats.clear(); paramLength.clear();
//paramValues.squeeze(); paramFormats.squeeze(); paramLength.squeeze();
paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
memcpy(temp1, rec.scale.toUtf8().constData(), strlen(rec.scale.toUtf8().constData()));
paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.scale.length(); paramValues[0]=temp1;
char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
//qDebug()<<"description="<<rec.description.toLocal8Bit().constData();
//qDebug()<<"scale="<<rec.scale.toUtf8().constData();
//Sleep(10);
try
{
  if(ss & eLoadUpdate) result=PQexecPrepared(connection, "scales_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
  if(ss & eLoadInsert) result=PQexecPrepared(connection, "scales_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
  //if(ss & eLoadInsert) result=PQexecParams(connection, "insert into scales values($1::varchar, $2::varchar)", field_count, NULL, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
  //qDebug()<<"CPGDatabase::ScalesLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);

  ASSERT_COMMAND_OK(result);
  PQclear(result);
} catch(...)
  {
  PQclear(result);
  throw;
  }
}

void CPGDatabase::PricekinLoad(const Pricekin& rec, eBitMask ss) throw (CPGDBException)
{/*insert into pricekin values($1::smallint, $2::varchar)*/
  field_count=2;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.priecelist);
  paramFormats[0]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.priecelist); paramValues[0]=(char*)&temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
  try
    {
      if(ss & eLoadUpdate) result=PQexecPrepared(connection, "pricekin_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
      if(ss & eLoadInsert) result=PQexecPrepared(connection, "pricekin_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
      //qDebug()<<"CPGDatabase::ScalesLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
      ASSERT_COMMAND_OK(result);
      PQclear(result);
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}


void CPGDatabase::PluLoad(const Plu& rec, eBitMask ss) throw (CPGDBException)
{
  /*
'insert into plucash values($1::varchar, $2::varchar, $3::varchar, $4::double precision, $5::varchar, $6::varchar, $7::varchar, $8::double precision, $9::double precision, $10::double precision,
                                        $11::varchar, $12::smallint,  $13::smallint, $14::smallint, $15::smallint, $16::smallint, $17::smallint, $18::smallint)'
   */
  field_count=18;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
//  paramValues.clear(); paramFormats.clear(); paramLength.clear();
  //qDebug()<<rec.articul.length();
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.articul.toUtf8().constData(), strlen(rec.articul.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.articul.length(); paramValues[0]=temp1;
  /*qDebug()<<rec.articul.toUtf8().constData()<<" "<<rec.description.toUtf8().constData()<<"  "<<rec.mesuriment.toUtf8().constData()<<" "<<rec.mespresision<<"  "<<
            rec.add1.toUtf8().constData()<<"  "<<rec.add2.toUtf8().constData()<<"  "<<rec.add3.toUtf8().constData()<<"  "<<
            rec.addnum1<<"  "<<rec.addnum2<<"  "<<rec.addnum3<<"  "<<
            rec.scale.toUtf8().constData()<<"  "<<rec.groop1<<"  "<<rec.groop2<<"  "<<rec.groop3<<"  "<<rec.groop4<<"  "<<rec.groop5<<"  "<<rec.department<<"  "<<rec.deleted;*/
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  memcpy(temp3, rec.mesuriment.toUtf8().constData(), strlen(rec.mesuriment.toUtf8().constData()));
  paramFormats[2]=RFORMAT_TEXT; paramLength[2]=rec.mesuriment.length(); paramValues[2]=temp3;
//  qint64 temp4=htond(rec.mespresision);
//  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.mespresision); paramValues[3]=(char*)&temp4;
  char temp4[MAX_STR_LENGTH]; memset(temp4, 0, MAX_STR_LENGTH);
  QString cs_4; cs_4.setNum(rec.mespresision, 'f', 3);
  memcpy(temp4, cs_4.toUtf8().constData(), strlen(cs_4.toUtf8().constData()));
  paramFormats[3]=RFORMAT_TEXT; paramLength[3]=cs_4.length(); paramValues[3]=temp4;
  char temp5[MAX_STR_LENGTH]; memset(temp5, 0, MAX_STR_LENGTH);
  memcpy(temp5, rec.add1.toUtf8().constData(), strlen(rec.add1.toUtf8().constData()));
  paramFormats[4]=RFORMAT_TEXT; paramLength[4]=rec.add1.length(); paramValues[4]=temp5;
  char temp6[MAX_STR_LENGTH]; memset(temp6, 0, MAX_STR_LENGTH);
  memcpy(temp6, rec.add2.toUtf8().constData(), strlen(rec.add2.toUtf8().constData()));
  paramFormats[5]=RFORMAT_TEXT; paramLength[5]=rec.add2.length(); paramValues[5]=temp6;
  char temp7[MAX_STR_LENGTH]; memset(temp7, 0, MAX_STR_LENGTH);
  memcpy(temp7, rec.add3.toUtf8().constData(), strlen(rec.add3.toUtf8().constData()));
  paramFormats[6]=RFORMAT_TEXT; paramLength[6]=rec.add3.length(); paramValues[6]=temp7;
//  qint64 temp8=htond(rec.addnum1);
  //qDebug()<<"sizeof(double)="<<sizeof(rec.addnum1);
//  paramFormats[7]=RFORMAT_BINARY; paramLength[7]=sizeof(rec.addnum1); paramValues[7]=(char*)&temp8;
  char temp8[MAX_STR_LENGTH]; memset(temp8, 0, MAX_STR_LENGTH);
  QString cs_8; cs_8.setNum(rec.addnum1, 'f');
  memcpy(temp8, cs_8.toUtf8().constData(), strlen(cs_8.toUtf8().constData()));
  paramFormats[7]=RFORMAT_TEXT; paramLength[7]=cs_8.length(); paramValues[7]=temp8;
//  qint64 temp9=htond(rec.addnum2);
//  paramFormats[8]=RFORMAT_BINARY; paramLength[8]=sizeof(rec.addnum2); paramValues[8]=(char*)&temp9;
  char temp9[MAX_STR_LENGTH]; memset(temp9, 0, MAX_STR_LENGTH);
  QString cs_9; cs_9.setNum(rec.addnum2, 'f');
  memcpy(temp9, cs_9.toUtf8().constData(), strlen(cs_9.toUtf8().constData()));
  paramFormats[8]=RFORMAT_TEXT; paramLength[8]=cs_9.length(); paramValues[8]=temp9;
//  qint64 temp10=htond(rec.addnum3);
//  paramFormats[9]=RFORMAT_BINARY; paramLength[9]=sizeof(rec.addnum3); paramValues[9]=(char*)&temp10;
  char temp10[MAX_STR_LENGTH]; memset(temp10, 0, MAX_STR_LENGTH);
  QString cs_10; cs_10.setNum(rec.addnum3, 'f');
  memcpy(temp10, cs_10.toUtf8().constData(), strlen(cs_10.toUtf8().constData()));
  paramFormats[9]=RFORMAT_TEXT; paramLength[9]=cs_10.length(); paramValues[9]=temp10;
  char temp11[MAX_STR_LENGTH]; memset(temp11, 0, MAX_STR_LENGTH);
  memcpy(temp11, rec.scale.toUtf8().constData(), strlen(rec.scale.toUtf8().constData()));
  paramFormats[10]=RFORMAT_TEXT; paramLength[10]=rec.scale.length(); paramValues[10]=temp11;
  unsigned short temp12=htons(rec.groop1);
  paramFormats[11]=RFORMAT_BINARY; paramLength[11]=sizeof(rec.groop1); paramValues[11]=(char*)&temp12;
  unsigned short temp13=htons(rec.groop2);
  paramFormats[12]=RFORMAT_BINARY; paramLength[12]=sizeof(rec.groop2); paramValues[12]=(char*)&temp13;
  unsigned short temp14=htons(rec.groop3);
  paramFormats[13]=RFORMAT_BINARY; paramLength[13]=sizeof(rec.groop3); paramValues[13]=(char*)&temp14;
  unsigned short temp15=htons(rec.groop4);
  paramFormats[14]=RFORMAT_BINARY; paramLength[14]=sizeof(rec.groop4); paramValues[14]=(char*)&temp15;
  unsigned short temp16=htons(rec.groop5);
  paramFormats[15]=RFORMAT_BINARY; paramLength[15]=sizeof(rec.groop5); paramValues[15]=(char*)&temp16;
  unsigned short temp17=htons(rec.department);
  paramFormats[16]=RFORMAT_BINARY; paramLength[16]=sizeof(rec.department); paramValues[16]=(char*)&temp17;
  unsigned short temp18=rec.deleted?1:0;
  paramFormats[17]=RFORMAT_BINARY; paramLength[17]=sizeof(unsigned short); paramValues[17]=(char*)&temp18;
  try
    {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "plucash_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "plucash_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
      //qDebug()<<"CPGDatabase::PluLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
      ASSERT_COMMAND_OK(result);
      PQclear(result);
    } catch(...)
    {
    PQclear(result);
    throw;
    }
}

void CPGDatabase::CostsLoad(const Costs& rec, eBitMask ss) throw (CPGDBException)
{/*insert into costs values($1::varchar, $2::money, $3::smallint, now()*/
  field_count=3;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.articul.toUtf8().constData(), strlen(rec.articul.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.articul.length(); paramValues[0]=temp1;
//  qint64 temp2=htond(rec.price);
//  qDebug()<<rec.price<<"  "<<temp2;
//  paramFormats[1]=RFORMAT_BINARY; paramLength[1]=sizeof(rec.price); paramValues[1]=(char*)&temp2;
//  paramFormats[1]=RFORMAT_BINARY; paramLength[1]=sizeof(rec.price); paramValues[1]=(char*)&rec.price;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  QString cs_2; cs_2.setNum(rec.price, 'f', 2);
  memcpy(temp2, cs_2.toUtf8().constData(), strlen(cs_2.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=cs_2.length(); paramValues[1]=temp2;
  unsigned short temp3=htons(rec.pricelist);
  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.pricelist); paramValues[2]=(char*)&temp3;
  try
  {
    result=PQexecPrepared(connection, "costs_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::BarLoad(const Barcode& rec, eBitMask ss) throw (CPGDBException)
{
  field_count=4;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  paramFormats[0]=paramFormats[1]=paramFormats[2]=RFORMAT_TEXT;
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.bar.toUtf8().constData(), strlen(rec.bar.toUtf8().constData()));
  paramLength[0]=rec.bar.length(); paramValues[0]=temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.articul.toUtf8().constData(), strlen(rec.articul.toUtf8().constData()));
  paramLength[1]=rec.articul.length(); paramValues[1]=temp2;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  memcpy(temp3, rec.cardsize.toUtf8().constData(), strlen(rec.cardsize.toUtf8().constData()));
  paramLength[2]=rec.cardsize.length(); paramValues[2]=temp3;
  char temp4[MAX_STR_LENGTH]; memset(temp4, 0, MAX_STR_LENGTH);
  QString cs_4; cs_4.setNum(rec.quantity, 'f', 3);
  memcpy(temp4, cs_4.toUtf8().constData(), strlen(cs_4.toUtf8().constData()));
  paramFormats[3]=RFORMAT_TEXT; paramLength[3]=cs_4.length(); paramValues[3]=temp4;
try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "bar_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "bar_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    //qDebug()<<"CPGDatabase::BarLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::SizesLoad(const Sizes& rec, eBitMask ss) throw (CPGDBException)
{
  field_count=2;
  paramValues.clear(); paramFormats.clear(); paramLength.clear();
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  paramFormats[0]=paramFormats[1]=RFORMAT_TEXT;/*0-string*/
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.scale.toUtf8().constData(), strlen(rec.scale.toUtf8().constData()));
//  paramLength[0]=rec.scale.length(); paramValues[0]=rec.scale.toUtf8().constData(); qDebug()<<rec.scale.toUtf8().constData();
  paramLength[0]=rec.scale.length(); paramValues[0]=temp1; //qDebug()<<rec.scale.toUtf8().constData();
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.cardsize.toUtf8().constData(), strlen(rec.cardsize.toUtf8().constData()));
//  paramLength[1]=rec.cardsize.length(); paramValues[1]=rec.cardsize.toUtf8().constData(); qDebug()<<rec.cardsize.toUtf8().constData();
  paramLength[1]=rec.cardsize.length(); paramValues[1]=temp2; //qDebug()<<rec.cardsize.toUtf8().constData();
  try
  {
    result=PQexecPrepared(connection, "sizes_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    //qDebug()<<"CPGDatabase::ScalesLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
    //Sleep(100);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
    {
    PQclear(result);
    throw;
    }
}

void CPGDatabase::PersonalLoad(const Personal& rec, eBitMask ss) throw (CPGDBException)
{/*insert into personal values($1::smallint, $2::varchar, $3::varchar, $4::smallint)*/
  field_count=4;
  paramValues.clear(); paramFormats.clear(); paramLength.clear();
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.ident);
  paramFormats[0]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.ident); paramValues[0]=(char*)&temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  memcpy(temp3, rec.passw.toUtf8().constData(), strlen(rec.passw.toUtf8().constData()));
  paramFormats[2]=RFORMAT_TEXT; paramLength[2]=rec.passw.length(); paramValues[2]=temp3;
  unsigned short temp4=htons(rec.accesslevel);
  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.accesslevel); paramValues[3]=(char*)&temp4;
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "personal_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "personal_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::TaxLoad(const Tax& rec, eBitMask ss) throw (CPGDBException)
{/*insert into tax values($1::varchar, $2::smallint, $3::numeric, $4::numeric)*/
  field_count=4;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.articul.toUtf8().constData(), strlen(rec.articul.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.articul.length(); paramValues[1]=temp1;
  unsigned short temp2=htons(rec.taxindex);
  paramFormats[1]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.taxindex); paramValues[0]=(char*)&temp2;
//  qint64 temp3=htond(rec.taxvalue);
//  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.taxvalue); paramValues[2]=(char*)&temp3;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  QString cs_3; cs_3.setNum(rec.taxvalue, 'f', 2);
  memcpy(temp3, cs_3.toUtf8().constData(), strlen(cs_3.toUtf8().constData()));
  paramFormats[2]=RFORMAT_TEXT; paramLength[2]=cs_3.length(); paramValues[2]=temp3;
//  qint64 temp4=htond(rec.taxsum);
//  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=rec.taxvalue; paramValues[3]=(char*)&temp4;
  char temp4[MAX_STR_LENGTH]; memset(temp4, 0, MAX_STR_LENGTH);
  QString cs_4; cs_4.setNum(rec.taxsum);
  memcpy(temp4, cs_4.toUtf8().constData(), strlen(cs_4.toUtf8().constData()));
  paramFormats[3]=RFORMAT_TEXT; paramLength[3]=cs_4.length(); paramValues[3]=temp4;
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "tax_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "tax_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::DisccliLoad(const Disccli& rec, eBitMask ss) throw (CPGDBException)
{/*insert into disccli values($1::varchar, $2::varchar, $3::numeric, $4::smallint, $5::smallint)*/
  field_count=5;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
//  qDebug()<<rec.barcode.toUtf8().constData();
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.barcode.toUtf8().constData(), strlen(rec.barcode.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.barcode.length(); paramValues[0]=temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
//  qint64 temp3=htond(rec.discount);
//  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.discount); paramValues[2]=(char*)&temp3;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  QString cs_3; cs_3.setNum(rec.discount, 'f', 2);
  memcpy(temp3, cs_3.toUtf8().constData(), strlen(cs_3.toUtf8().constData()));
  paramFormats[2]=RFORMAT_TEXT; paramLength[2]=cs_3.length(); paramValues[2]=temp3;
  unsigned short temp4=htons(rec.pricelist);
  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.pricelist); paramValues[3]=(char*)&temp4;
  unsigned short temp5=htons(rec.clientindex);
  paramFormats[4]=RFORMAT_BINARY; paramLength[4]=sizeof(rec.clientindex); paramValues[4]=(char*)&temp5;
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "disccli_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "disccli_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::CliclassLoad(const Cliclass& rec, eBitMask ss) throw (CPGDBException)
{/*insert into cliclass values($1::smallint, $2::smallint, $3::smallint, $4::smallint, $5::smallint, $6::varchar, $7::numeric, $8::smallint)*/
  field_count=8;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.groop1);
  paramFormats[0]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.groop1); paramValues[0]=(char*)&temp1;
  unsigned short temp2=htons(rec.groop2);
  paramFormats[1]=RFORMAT_BINARY; paramLength[1]=sizeof(rec.groop2); paramValues[1]=(char*)&temp2;
  unsigned short temp3=htons(rec.groop3);
  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.groop3); paramValues[2]=(char*)&temp3;
  unsigned short temp4=htons(rec.groop4);
  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.groop4); paramValues[3]=(char*)&temp4;
  unsigned short temp5=htons(rec.groop5);
  paramFormats[4]=RFORMAT_BINARY; paramLength[4]=sizeof(rec.groop5); paramValues[4]=(char*)&temp5;
  char temp6[MAX_STR_LENGTH]; memset(temp6, 0, MAX_STR_LENGTH);
  memcpy(temp6, rec.barcode.toUtf8().constData(), strlen(rec.barcode.toUtf8().constData()));
  paramFormats[5]=RFORMAT_TEXT; paramLength[5]=rec.barcode.length(); paramValues[5]=temp6;
  //qDebug()<<rec.barcode.toUtf8().constData();
//  qint64 temp7=htond(rec.discount);
//  paramFormats[6]=RFORMAT_BINARY; paramLength[6]=sizeof(rec.discount); paramValues[6]=(char*)&temp7;
  char temp7[MAX_STR_LENGTH]; memset(temp7, 0, MAX_STR_LENGTH);
  QString cs_7; cs_7.setNum(rec.discount, 'f', 2);
  memcpy(temp7, cs_7.toUtf8().constData(), strlen(cs_7.toUtf8().constData()));
  paramFormats[6]=RFORMAT_TEXT; paramLength[6]=cs_7.length(); paramValues[6]=temp7;
  unsigned short temp8=htons(rec.pricelist);
  paramFormats[7]=RFORMAT_BINARY; paramLength[7]=sizeof(rec.pricelist); paramValues[7]=(char*)&temp8;
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "cliclass_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "cliclass_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::CredcardLoad(const Credcard& rec, eBitMask ss) throw (CPGDBException)
{/*insert into credcard values($1::smallint, $2::varchar, $3::numeric, $4::smallint, $5::varchar, $6::varchar)*/
  field_count=6;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  unsigned short temp1=htons(rec.CredCardIndex);
  paramFormats[0]=RFORMAT_BINARY; paramLength[0]=sizeof(rec.CredCardIndex); paramValues[0]=(char*)&temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.description.toUtf8().constData(), strlen(rec.description.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.description.length(); paramValues[1]=temp2;
//  qint64 temp3=htond(rec.limitsum);
//  paramFormats[2]=RFORMAT_BINARY; paramLength[2]=sizeof(rec.limitsum); paramValues[2]=(char*)&temp3;
  char temp3[MAX_STR_LENGTH]; memset(temp3, 0, MAX_STR_LENGTH);
  QString cs_3; cs_3.setNum(rec.limitsum, 'f', 2);
  memcpy(temp3, cs_3.toUtf8().constData(), strlen(cs_3.toUtf8().constData()));
  paramFormats[2]=RFORMAT_TEXT; paramLength[2]=cs_3.length(); paramValues[2]=temp3;
  unsigned short temp4=htons(rec.canreturn);
  paramFormats[3]=RFORMAT_BINARY; paramLength[3]=sizeof(rec.canreturn); paramValues[3]=(char*)&temp4;
  char temp5[MAX_STR_LENGTH]; memset(temp5, 0, MAX_STR_LENGTH);
  memcpy(temp5, rec.soft.toUtf8().constData(), strlen(rec.soft.toUtf8().constData()));
  paramFormats[4]=RFORMAT_TEXT; paramLength[4]=rec.soft.length(); paramValues[4]=temp5;
  char temp6[MAX_STR_LENGTH]; memset(temp6, 0, MAX_STR_LENGTH);
  memcpy(temp6, rec.ident.toUtf8().constData(), strlen(rec.ident.toUtf8().constData()));
  paramFormats[5]=RFORMAT_TEXT; paramLength[5]=rec.ident.length(); paramValues[5]=temp6;
  try
  {
    if(ss & eLoadUpdate) result=PQexecPrepared(connection, "credcard_upd", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    if(ss & eLoadInsert) result=PQexecPrepared(connection, "credcard_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::DclilstLoad(const Dclilst& rec, eBitMask ss) throw (CPGDBException)
{/*insert into dclislst values($1::varchar, $2::varchar)*/
  field_count=2;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, rec.codeend.toUtf8().constData(), strlen(rec.codeend.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=rec.codeend.length(); paramValues[0]=temp1;
  char temp2[MAX_STR_LENGTH]; memset(temp2, 0, MAX_STR_LENGTH);
  memcpy(temp2, rec.codestart.toUtf8().constData(), strlen(rec.codestart.toUtf8().constData()));
  paramFormats[1]=RFORMAT_TEXT; paramLength[1]=rec.codestart.length(); paramValues[1]=temp2;
  try
  {
    //if(ss & eLoadUpdate) result=PQexecPrepared(connection, dclislst_upd.toUtf8().constData(), field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    result=PQexecPrepared(connection, "dclislst_ins", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);
    //qDebug()<<"CPGDatabase::ScalesLoad()="<<PQresultStatus(result)<<PQerrorMessage(connection);
    ASSERT_COMMAND_OK(result);
    PQclear(result);
  } catch(...)
    {
    PQclear(result);
    throw;
    }
}

void CPGDatabase::StartTransaction() throw (CPGDBException)
{
  try
  {
    result=PQexec(connection, "START TRANSACTION;");
    ASSERT_COMMAND_OK(result);
    qDebug()<<"Start transaction";
    PQclear(result);
  } catch (...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::Commit() throw (CPGDBException)
{
  try
  {
    result=PQexec(connection, "COMMIT;");
    ASSERT_COMMAND_OK(result);
    qDebug()<<"Commit";
    PQclear(result);
  } catch (...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::Rollback() throw (CPGDBException)
{
  try
  {
  result=PQexec(connection, "ROLLBACK;");
  ASSERT_COMMAND_OK(result);
  qDebug()<<"ROLLBACK";
  PQclear(result);
  } catch (...)
  {
    PQclear(result);
    throw;
  }
}

QString CPGDatabase::getSingleStr(int col_num, int row_num)
{
  //return QString().fromLocal8Bit(PQgetvalue(result, 0, col_num));
  //qDebug()<<"CPGDatabase::getSingleStr(int col_num)"<<col_num;
  return QString().fromUtf8(PQgetvalue(result, row_num, col_num));
}

void CPGDatabase::GetLoaderSettings(LoadSettings &set) throw (CPGDBException)
{
  try
  {
    result=PQexec(connection, "select strvalue::varchar from cashsettings where var_name='flg_upd'");
    ASSERT_TUPLES_OK(result);
    set.flg_upd=getSingleStr(0);
    PQclear(result);
    result=PQexec(connection, "select strvalue::varchar from cashsettings where var_name='flg_ins'");
    ASSERT_TUPLES_OK(result);
    set.flg_ins=getSingleStr(0);
    PQclear(result);
    result=PQexec(connection, "select strvalue::varchar from cashsettings where var_name='load_dir'");
    ASSERT_TUPLES_OK(result);
    set.load_dir=getSingleStr(0);
    PQclear(result);
    result=PQexec(connection, "select intvalue::smallint from cashsettings where var_name='timer'");
    ASSERT_TUPLES_OK(result);
    set.timer=PQbinaryTuples(result)?ntohs(*PQgetvalue(result, 0, 0)):(QString().fromLocal8Bit(PQgetvalue(result, 0, 0))).toShort();
    PQclear(result);
    result=PQexec(connection, "select strvalue from cashsettings where var_name='flg_lck'");
    ASSERT_TUPLES_OK(result);
    set.flg_lck=getSingleStr(0);
    PQclear(result);
    QString table("uq_locale");
    char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
    memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
    field_count=1;
    paramValues.resize(field_count); paramLength.resize(field_count); paramFormats.resize(field_count);
    paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
    //result=PQexecPrepared(connection, "uq_cashsale", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
    PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
    set.locale=getSingleStr(1);//чтение второго столбца strvalue;
    PQclear(result);
    switch (set.class_loader)
    {
    case LoadSettings::ODBC:
//        field_count=1;
//        paramValues.resize(field_count); paramLength.resize(field_count); paramFormats.resize(field_count);
        table="uq_cashsale";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.cashsale=getSingleStr(1);//чтение второго столбца strvalue;
        PQclear(result);
        table="uq_cashdisc";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.cashdisc=getSingleStr(1);
        PQclear(result);
        table="uq_cashpay";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.cashpay=getSingleStr(1);
        PQclear(result);
        table="uq_currests";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.currests=getSingleStr(1);
        PQclear(result);
        table="uq_cashgood";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.cashgood=getSingleStr(1);
        PQclear(result);
        table="uq_curmoney";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.curmoney=getSingleStr(1);
        PQclear(result);
        table="uq_outdb";
        memset(temp1, 0, MAX_STR_LENGTH);
        memcpy(temp1, table.toUtf8().constData(), strlen(table.toUtf8().constData()));
        paramFormats[0]=RFORMAT_TEXT; paramLength[0]=table.length(); paramValues[0]=temp1;
        PQP("sd_cashsettings", field_count, PGRES_TUPLES_OK);
        set.alias=getSingleStr(1);
        PQclear(result);
        break;
    }

  } catch (...)
  {
    PQclear(result);
    throw;
  }
}

void CPGDatabase::PQP(const char *query, int fcount, ExecStatusType type, int result_format/*RFORMAT_TEXT*/) throw (CPGDBException)
{
result=PQexecPrepared(connection, query, fcount, paramValues.constData(), paramLength.constData(), paramFormats.constData(), result_format);
if(PQresultStatus(result)!=type) throw CPGDBException(this);
}

void CPGDatabase::SetTrasactionMode(eTransactionMode tm) throw (CPGDBException)
{/*{SERIALIZABLE, REPEATABLE_READ, READ_COMMITTED, READ_UNCOMMITTED}*/
  QString level("SET TRANSACTION ISOLATION LEVEL ");
  switch (tm)
    {
    case SERIALIZABLE: level+="SERIALIZABLE;";
          break;
    case REPEATABLE_READ: level+="REPEATABLE READ;";
          break;
    case READ_COMMITTED: level+="READ COMMITTED;";
          break;
    case READ_UNCOMMITTED: level+="READ UNCOMMITTED;";
          break;
    }
  try
  {
  result=PQexec(connection, level.toUtf8().constData());
  ASSERT_COMMAND_OK(result);
  qDebug()<<"SetTrasactionMode";
  PQclear(result);
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

const int CPGDatabase::OpenCheck(int linked) throw (CPGDBException)
{
  
}

CGood CPGDatabase::AddGoodBC(const QString& bc) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramLength.resize(field_count); paramFormats.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, bc.toUtf8().constData(), strlen(bc.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=bc.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_addbc", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);//возвращаем результат в текстовом виде
      if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);
      char pos[MAX_STR_LENGTH]; memset(pos, 0, MAX_STR_LENGTH);//номер позиции в строковом виде
      memcpy(pos, PQgetvalue(result, 0, 0), PQgetlength(result, 0, 0));//копирование данных во временную стековую переменную
      PQclear(result);
      return get_pos(pos);
    } catch (...)
    {
      //addGoodBCPrepared=0;//сбросить признак подготовленности запроса
      PQclear(result);
      throw;
    }
}

CGood CPGDatabase::AddGoodArt(const QString& art) throw (CPGDBException)
{
 field_count=1;
  paramValues.resize(field_count); paramLength.resize(field_count); paramFormats.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, art.toUtf8().constData(), strlen(art.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=art.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_addart", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);//возвращаем результат в текстовом виде
      if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);
      char pos[MAX_STR_LENGTH]; memset(pos, 0, MAX_STR_LENGTH);//номер позиции в строковом виде
      memcpy(pos, PQgetvalue(result, 0, 0), PQgetlength(result, 0, 0));//копирование данных во временную стековую переменную
      PQclear(result);
      return get_pos(pos);
    } catch (...)
    {
      //addGoodBCPrepared=0;//сбросить признак подготовленности запроса
      PQclear(result);
      throw;
    }
}


void CPGDatabase::GetButtonSettings(KeySettings& ks) throw (CPGDBException)
{
  field_count=1;//количество связываемых переменных-аргументов запроса
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count); //изменение размера векторов-аргументов
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);//подготавливаем переменную для передачи строкового параметра
  memcpy(temp1, ks.object_name.toUtf8().constData(), strlen(ks.object_name.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=ks.object_name.length(); paramValues[0]=temp1;//заполняем вектора описателей параметров(тип, длина, значение)
  //QMessageBox::critical(NULL, "Cpgdatabase", ks.object_name);
  try
    {
      result=PQexecPrepared(connection, "ss_buttons", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_BINARY);//выполняем заранее подготовленный запрос
      if(PQresultStatus(result)!=PGRES_TUPLES_OK) throw CPGDBException(this);//проверяем возврат, если он отличен от "выполнение успешное, команда вернула данные"
      ks.face_text=getSingleStr(0);
      ks.shortcut=getSingleStr(1);
      ks.face_font=getSingleStr(2);
      ks.description=getSingleStr(3);
      ks.icon_path=getSingleStr(4);
      //QMessageBox::critical(NULL, "Cpgdatabase", ks.icon_path);
      PQclear(result);
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

void CPGDatabase::GetPortSettings(CP& cp) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, cp.name.toUtf8().constData(), strlen(cp.name.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=cp.name.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_port", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      cp.path=getSingleStr(0);//получения номера com порта, пути до него
      bool ok;
      int timeout(QString(getSingleStr(1)).toInt(&ok));//пеобразование из текста в целое число таймаута ожидания
      cp.timeout.tv_sec = timeout / 1000000;//получение задержки в секундах
      cp.timeout.tv_usec = timeout % 1000000;//плученеи задержки в микросекундах 
      PQclear(result);
      qDebug("Port path: %s", cp.path.toUtf8().constData());
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

int CPGDatabase::ReloadCheck(QMap<int, CGood>& goods, bool force) throw(CPGDBException)
{
  field_count=0;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  try
    {
      result=PQexecPrepared(connection, "ss_reloadcheck", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      //CGood goo;//компилятор не позволял объявлять переменную внутри условия
      if(force || getSingleStr(0).toInt())
	{
	  PQclear(result);
	  result=PQexecPrepared(connection, "ss_check", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
	  ASSERT_TUPLES_OK(result);
	  goods.clear();
	  int ntuples(PQntuples(result));
	  qDebug()<<"int ntuples(PQntuples(result));"<<ntuples<<" "<<PQnfields(result);
	  for(int i=0;i<ntuples;++i)
	    {
	      CGood goo(getSingleStr(0, i).toInt(), getSingleStr(1, i), getSingleStr(2, i), getSingleStr(3, i), getSingleStr(4, i).toInt(), getSingleStr(5, i), getSingleStr(6, i), getSingleStr(7, i), getSingleStr(8, i), getSingleStr(9, i));//goo=get_pos(QString().setNum(i, 10).toUtf8().constData());
	      //goo=get_pos("30");
	      goods[goo.pos]=goo;
	      //qDebug()<<"goo.pos"<<goo.pos;
	      //goods[i+1]=goo;
	    }
	  PQclear(result);
	  return ntuples;
	}
      PQclear(result);
      return 0;
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

CGood CPGDatabase::Quantity(const QString& qnt) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, qnt.toUtf8().constData(), strlen(qnt.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=qnt.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_quantity", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      memset(temp1, 0, MAX_STR_LENGTH); memcpy(temp1, PQgetvalue(result, 0, 0), PQgetlength(result, 0, 0));//temp указывает на номер позиции
      PQclear(result);
      return get_pos(temp1);   
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}


CGood CPGDatabase::Storno(const QString& id) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, id.toUtf8().constData(), strlen(id.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=id.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_storno", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      memset(temp1, 0, MAX_STR_LENGTH); memcpy(temp1, PQgetvalue(result, 0, 0), PQgetlength(result, 0, 0));
      PQclear(result);
      return get_pos(temp1);
      //PQclear(result);
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}


QString CPGDatabase::GetTotal() throw (CPGDBException)
{
  field_count=0;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
try
  {
    result=PQexecPrepared(connection, "ss_total", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
    ASSERT_TUPLES_OK(result);
    QString temp(getSingleStr(0));
    PQclear(result);
    return temp;
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}

CGood CPGDatabase::Subtotal() throw (CPGDBException)
{
  field_count=0;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  try
    {
      result=PQexecPrepared(connection, "ss_subtotal", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      //ASSERT_COMMAND_OK(result);
      ASSERT_TUPLES_OK(result);
      char pos[MAX_STR_LENGTH]; memset(pos, 0, MAX_STR_LENGTH);//номер позиции в строковом виде
      memcpy(pos, PQgetvalue(result, 0, 0), PQgetlength(result, 0, 0));//копирование данных во временную стековую переменную
      PQclear(result);
      return get_pos(pos);
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

QString CPGDatabase::Pay(const QString& cash) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, cash.toUtf8().constData(), strlen(cash.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=cash.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_closecheck", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      QString res(getSingleStr(0));
      PQclear(result);
      return res;
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

eCashSatate CPGDatabase::GetState() throw (CPGDBException)
{
  field_count=0;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
try
  {
    result=PQexecPrepared(connection, "ss_state", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
    ASSERT_TUPLES_OK(result);
    QString temp(getSingleStr(0));
    PQclear(result);
    return eCashSatate(temp.toInt());
  } catch(...)
  {
    PQclear(result);
    throw;
  }
}


CGood CPGDatabase::GetCurrentPos(const char* pos /*"0"*/) throw (CPGDBException)
{
  return get_pos(pos);
}

QString CPGDatabase::CashIn(const QString& money) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, money.toUtf8().constData(), strlen(money.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=money.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_cashin", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      QString res(getSingleStr(0));
      PQclear(result);
      bool ok;
      return res;
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

QString CPGDatabase::CashOut(const QString& money) throw (CPGDBException)
{
  field_count=1;
  paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
  char temp1[MAX_STR_LENGTH]; memset(temp1, 0, MAX_STR_LENGTH);
  memcpy(temp1, money.toUtf8().constData(), strlen(money.toUtf8().constData()));
  paramFormats[0]=RFORMAT_TEXT; paramLength[0]=money.length(); paramValues[0]=temp1;
  try
    {
      result=PQexecPrepared(connection, "ss_cashout", field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
      ASSERT_TUPLES_OK(result);
      QString res(getSingleStr(0));
      PQclear(result);
      bool ok;
      return res;
      //return locale->toDouble(res,&ok);
    } catch(...)
    {
      PQclear(result);
      throw;
    }
}

void CPGDatabase::set_locale() throw()
{
  locale=new QLocale(QLocale::Russian, QLocale::RussianFederation);//установка локали
}

void CPGDatabase::SetLoader(CLoad *ld)
{
CDatabase::SetLoader(ld);
}
//секция выгрузки

void CPGDatabase::UploadRecord() throw (CPGDBException)
{
    try
        {
        result=PQexecPrepared(connection, query, field_count, paramValues.constData(), paramLength.constData(), paramFormats.constData(), RFORMAT_TEXT);
        ASSERT_TUPLES_OK(result);
        QVector<Oid> oids;//вектор OID'jd
        for(int i=0;i<PQnfields(result); i++)//заполнение массива Oid
        {
            oids<<PQftype(result, i);
        }
        CInsertParams insert;
        for(int i=0; i<PQntuples(result); i++)//перебор строк
        {
            for(int j=0; j<PQnfields(result); j++)
            {
                insert.ApendValue(PQgetvalue(result, i, j), oids[j]);
                //qDebug()<<PQgetvalue(result, i, j)<<" "<<oids[j];
            }
            loader->UploadRecord(insert.GetInsertValues());
            insert.Clear();
        }
        } catch(...)
        {
        PQclear(result);
        throw;
        }
}

void CPGDatabase::CashsaleUpload(const ExchangeSettings& es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_cashsale", sizeof("sd_cashsale"));
    break;
    }
UploadRecord();
}

void CPGDatabase::CashdiscUpload(const ExchangeSettings &es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_cashdisc", sizeof("sd_cashdisc"));
    break;
    }
UploadRecord();
}

void CPGDatabase::CashpayUpload(const ExchangeSettings &es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_cashpay", sizeof("sd_cashpay"));
    break;
    }
UploadRecord();
}

void CPGDatabase::CashgoodUpload(const ExchangeSettings &es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_cashgood", sizeof("sd_cashgood"));
    break;
    }
UploadRecord();
}

void CPGDatabase::CurmoneyUpload(const ExchangeSettings &es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_curmoney", sizeof("sd_curmoney"));
    break;
    }
UploadRecord();
}

void CPGDatabase::CurrestsUpload(const ExchangeSettings &es) throw (CPGDBException)
{
    memset(query, 0, MAX_QUERY_LEN);
switch (es.type)
    {
    case ExchangeSettings::eWasOutput:
    field_count=0;
    paramValues.resize(field_count); paramFormats.resize(field_count); paramLength.resize(field_count);
    memcpy(query, "sd_currests", sizeof("sd_currests"));
    break;
    }
UploadRecord();
}

