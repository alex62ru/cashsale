#ifndef DATABASE_H
#define DATABASE_H

#include "common.h"

//#define STMT_BC "STMT_BC" //prepared statement добавление товара по ШК
#define RFORMAT_BINARY 1
#define RFORMAT_TEXT 0
#define MAX_QUERY_LEN 32

class CDBException;
class CPGDBException;
class CLoad;



class CDatabase
{
 public:
  enum eBitMask {eNull=0x00000000,
		 eLoad=0x00000001,//создание и настройка окружения для загрузчика
                 eFront=0x00000002,//настройка для фронта
                 eFirstRec=0x00000004,//первая запись в наборе
                 eLastRec=0x00000008,//последняя запись в наборе
                 eLoadInsert=0x00000010,//замена справочников
                 eLoadUpdate=0x00000020};//обновление справочников
  enum eTransactionMode{SERIALIZABLE, REPEATABLE_READ, READ_COMMITTED, READ_UNCOMMITTED};
  CDatabase(const QString& con, eBitMask bm);  //throw (CDBException)
  virtual ~ CDatabase()
    {
    }
  //virtual void* getConnection() throw() = 0;
  //функции фронта
  virtual CGood AddGoodBC(const QString& bc) = 0;//добавить НЕКОТОРОЕ КОЛИЧЕСТВО товара по штрихкоду
  virtual CGood AddGoodArt(const QString& art) = 0;//добавить единичное количество товара по артикулу
  virtual CGood GetCurrentPos(const char* pos = "0") = 0;//выбрать текущую, в БД нулевую позицию
  virtual const int OpenCheck(int linked) = 0;//открыть чек, аргумент больше 0 возврат по связанному чеку, возврат номер чека
  virtual int ReloadCheck(QMap<int, CGood>& goods, bool force) = 0;//перегрузить при необходимости чек из БД или принудительно force = 1, возврат - признак перегруженности 1 - перегрузка произведена
  virtual CGood Quantity(const QString& qnt) = 0;//установка количества
  virtual CGood Storno(const QString& id) = 0;//сторнирование позиции
  virtual QString GetTotal() = 0;//получить итог чека
  virtual CGood Subtotal() = 0;//подитог чека, с возвратом предыдущей позиции
  virtual QString Pay(const QString& cash) = 0;//закрытие чека, расчет, принимает сумму наличных, возвращает сумму сдачи
  virtual eCashSatate GetState() = 0;//чтение состояния БД, чек, окно кассира 
  virtual QString CashIn(const QString& money) = 0;//внесение денег
  virtual QString CashOut(const QString& money) = 0;//изъятие денег
  //секция функций обмена и настроек загрузки
  virtual void ClassifLoad(const Classif& rec, eBitMask ss) = 0;//загрузка классификатора
  //virtual void SetConstraint() throw (CDBException) = 0;//восстановить контроль сслылочной целостности
  virtual void DepartLoad(const Depart& rec, eBitMask ss)  = 0;
  virtual void ScalesLoad(const Scales& rec, eBitMask ss)  = 0;
  virtual void PricekinLoad(const Pricekin& rec, eBitMask ss) = 0;
  //void MesurimentLoad(Mesuriment& rec, eBitMask ss) throw (EOraException);
  virtual void PluLoad(const Plu&, eBitMask ss)  = 0;
  virtual void CostsLoad(const Costs& rec, eBitMask ss) = 0;
  virtual void BarLoad(const Barcode& rec, eBitMask ss) = 0;
  virtual void SizesLoad(const Sizes& rec, eBitMask ss) = 0;
  virtual void PersonalLoad(const Personal& rec, eBitMask ss) = 0;
  virtual void TaxLoad(const Tax& rec, eBitMask ss) = 0;
  virtual void DisccliLoad(const Disccli& rec, eBitMask ss) = 0;
  virtual void CliclassLoad(const Cliclass& rec, eBitMask ss) = 0;
  virtual void CredcardLoad(const Credcard& rec, eBitMask ss) = 0;
  virtual void DclilstLoad(const Dclilst& rec, eBitMask ss) = 0;
  void SetLoader(CLoad* ld);
  virtual void CashsaleUpload(const ExchangeSettings& es) = 0;//выгрузка контрольной ленты
  virtual void CashpayUpload(const ExchangeSettings& es) = 0;//выгрузка проведеных платежей
  virtual void CashdiscUpload(const ExchangeSettings& es) = 0;//проведеные скидки
  virtual void CashgoodUpload(const ExchangeSettings& es) = 0;//товарный отчет
  virtual void CurmoneyUpload(const ExchangeSettings& es) = 0;//работы с наличными
  virtual void CurrestsUpload(const ExchangeSettings& es) = 0;//закрытые смены
  //общие функции
  virtual void StartTransaction() = 0;//старт транзакции
  virtual void Commit() = 0;
  virtual void Rollback() = 0;
  virtual void GetLoaderSettings(LoadSettings& set) = 0;//чтение настроек загрузки
  virtual void GetPortSettings(CP& cp) = 0;//чтение настроек сканера
  virtual void SetTrasactionMode(eTransactionMode tm) = 0;//установка изолированности текущей транзакции
  virtual void GetButtonSettings(KeySettings& ks) = 0;//получение значений настроек кнопок
 protected:
  eBitMask bitSettings;//битовые настройки
  QString con_string;//строка подключения
  int field_count;//количество полей
  CLoad *loader;
};



//работа с Postgree
class CPGDatabase: public CDatabase 
{
 private:
  PGresult* result;//результат выполнения функций
  PGconn* connection;//соединение с БД
  QVector<const char*> paramValues;//массив значений
  QVector<int> paramLength;//длины значений
  QVector<int> paramFormats;//признак значений, binary - 1
  QLocale* locale;//используемая локаль
  //  int addGoodBCPrepared;//хранимка на добавление по ШК подготовленна
  void PreparedStatement() throw (CPGDBException);// подготовка statement всех хранимок и запросов
  int wait_connection;//задержка перед ресетом соединения
  int get_waitcon() throw (CPGDBException);//чтение таймаута задержки повтороной проверки коннеста
  //константы названий таблиц для загрузки
  /* static const QString classif_ins;
  static const QString classif_upd;
  static const QString plucash_ins;
  static const QString plucash_upd;
  static const QString depart_ins;
  static const QString depart_upd;
  static const QString scales_ins;
  static const QString scales_upd;
  static const QString costs_ins;
  static const QString pricekin_ins;
  static const QString pricekin_upd;
  static const QString bar_ins;
  static const QString bar_upd;
  static const QString sizes_ins;
  static const QString personal_ins;
  static const QString personal_upd;
  static const QString tax_upd;
  static const QString tax_ins;
  static const QString cliclass_ins;
  static const QString cliclass_upd;
  static const QString disccli_ins;
  static const QString disccli_upd;
  static const QString credcard_ins;
  static const QString credcard_upd;
  static const QString dclislst_ins;*/
  //общие константы
  static const QString wait_con;
  QString getSingleStr(int col_num, int row_num=0);//выбирает первую строку, используется в запросах, возвращающих единственную строчку
  void prepare(const QString& query);
  CGood get_pos(const char* pos) throw (CPGDBException);//получить заданную позицию
  void set_locale() throw();//установка локали, пока в качестве констант, в последстивии из настроек БД
  void PQP(const char *query, int fcount, ExecStatusType type, int result_format=RFORMAT_TEXT) throw (CPGDBException);//выполняет подготовленный запрос
  void UploadRecord() throw (CPGDBException);//выгружает одну строку
  char query[MAX_QUERY_LEN];//имя подготовленного запроса
 public:
  CPGDatabase(const QString& con, eBitMask bm) throw (CPGDBException);
  virtual ~CPGDatabase();//ат соединения с проверкой и при необходимости ресетом
  PGresult* getResult() throw();
  virtual CGood AddGoodBC(const QString& bc) throw (CPGDBException);//методы абсолютно одинаковые, если нужно изменить поведение, нужно ОБА РЕДАКТИРОВАТЬ
  virtual CGood AddGoodArt(const QString& art) throw (CPGDBException);
  virtual CGood GetCurrentPos(const char* pos = "0") throw (CPGDBException);
  virtual const int OpenCheck(int linked) throw (CPGDBException);
  virtual int ReloadCheck(QMap<int, CGood>& goods, bool force) throw (CPGDBException);
  virtual CGood Quantity(const QString& qnt) throw (CPGDBException);
  virtual CGood Storno(const QString& id) throw (CPGDBException);
  virtual QString GetTotal() throw (CPGDBException);
  virtual CGood Subtotal() throw (CPGDBException);
  virtual QString  Pay(const QString& cash) throw (CPGDBException);
  virtual eCashSatate GetState() throw (CPGDBException);
  virtual QString CashIn(const QString& money) throw (CPGDBException);//внесение денег
  virtual QString CashOut(const QString& money) throw (CPGDBException);//изъятие денег
  PGconn* getConnection() throw(CPGDBException);//возвр;
  //секция загрузки
  virtual void ClassifLoad(const Classif& rec, eBitMask ss) throw (CPGDBException);
  virtual void DepartLoad(const Depart& rec, eBitMask ss) throw (CPGDBException);
  virtual void ScalesLoad(const Scales& rec, eBitMask ss) throw (CPGDBException);
  virtual void PricekinLoad(const Pricekin& rec, eBitMask ss) throw (CPGDBException);
  //void MesurimentLoad(Mesuriment& rec, eBitMask ss) throw (EOraException);
  virtual void PluLoad(const Plu&, eBitMask ss) throw (CPGDBException);
  virtual void CostsLoad(const Costs& rec, eBitMask ss) throw (CPGDBException);
  virtual void BarLoad(const Barcode& rec, eBitMask ss) throw (CPGDBException);
  virtual void SizesLoad(const Sizes& rec, eBitMask ss) throw (CPGDBException);
  virtual void PersonalLoad(const Personal& rec, eBitMask ss) throw (CPGDBException);
  virtual void TaxLoad(const Tax& rec, eBitMask ss) throw (CPGDBException);
  virtual void DisccliLoad(const Disccli& rec, eBitMask ss) throw (CPGDBException);
  virtual void CliclassLoad(const Cliclass& rec, eBitMask ss) throw (CPGDBException);
  virtual void CredcardLoad(const Credcard& rec, eBitMask ss) throw (CPGDBException);
  virtual void DclilstLoad(const Dclilst& rec, eBitMask ss) throw (CPGDBException);
  virtual void CashsaleUpload(const ExchangeSettings& es) throw (CPGDBException);
  virtual void CashpayUpload(const ExchangeSettings& es) throw (CPGDBException);//выгрузка проведеных платежей
  virtual void CashdiscUpload(const ExchangeSettings& es) throw (CPGDBException);//проведеные скидки
  virtual void CashgoodUpload(const ExchangeSettings& es) throw (CPGDBException);//товарный отчет
  virtual void CurmoneyUpload(const ExchangeSettings& es) throw (CPGDBException);//работы с наличными
  virtual void CurrestsUpload(const ExchangeSettings& es) throw (CPGDBException);//закрытые смены
  void SetLoader(CLoad *ld);
  //общие функции
  virtual void StartTransaction()  throw (CPGDBException);
  virtual void Commit() throw (CPGDBException);
  virtual void Rollback() throw (CPGDBException);
  virtual void GetLoaderSettings(LoadSettings &set) throw (CPGDBException);
  virtual void SetTrasactionMode(eTransactionMode tm) throw (CPGDBException);
  virtual void GetButtonSettings(KeySettings& ks) throw (CPGDBException);
  virtual void GetPortSettings(CP& cp) throw (CPGDBException);
};

#define ASSERT_COMMAND_OK(_res) if(PQresultStatus(_res)!=PGRES_COMMAND_OK) throw CPGDBException(this)
#define ASSERT_TUPLES_OK(_res) if(PQresultStatus(_res)!=PGRES_TUPLES_OK) throw CPGDBException(this)

//#define htons(A) ((((unsigned short)(A) & 0xff00) >> 8) | \
//(((unsigned short)(A) & 0x00ff) << 8))

//#define htonl(A) ((((int)(A) & 0xff000000) >> 24) | \
//(((int)(A) & 0x00ff0000) >> 8) | \
//(((int)(A) & 0x0000ff00) << 8) | \
//(((int)(A) & 0x000000ff) << 24))

///*#define htond(A) ((((LONG64)(A) & 0xff00000000000000) >> 56) | \
//  (((LONG64)(A) & 0x00ff000000000000) >> 40) | \
//  (((LONG64)(A) & 0x0000ff0000000000) >> 24) | \
//  (((LONG64)(A) & 0x000000ff00000000) >> 8)  | \
//  (((LONG64)(A) & 0x00000000000000ff) << 56) | \
//  (((LONG64)(A) & 0x000000000000ff00) << 40) | \
//  (((LONG64)(A) & 0x0000000000ff0000) << 24) | \
//  (((LONG64)(A) & 0x00000000ff000000) << 8))*/

//#define htond(A) \
//( (LONG64)(A) << 56 ) & 0xFF00000000000000 | \
//( (LONG64)(A) << 40 ) & 0x00FF000000000000 | \
//( (LONG64)(A) << 24 ) & 0x0000FF0000000000 | \
//( (LONG64)(A) << 8  ) & 0x000000FF00000000 | \
//( (LONG64)(A) >> 8  ) & 0x00000000FF000000 | \
//( (LONG64)(A) >> 24 ) & 0x0000000000FF0000 | \
//( (LONG64)(A) >> 40 ) & 0x000000000000FF00 | \
//( (LONG64)(A) >> 56 ) & 0x00000000000000FF
#define htons(A) qToBigEndian<qint16>(A)
#define htond(A) qToBigEndian<qint64>(A)^0x8000000000000000
#define htonl(A) qToBigEndian<qint32>(A)


#define ntohs htons
#define ntohl htohl
#define ntohd htond

#endif //DATABASE_H
