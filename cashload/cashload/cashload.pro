TEMPLATE = app
CONFIG += console
CONFIG += qt exceptions warn_off debug
QT += sql
DEFINES += OS_WINDOWS # определяет платформу сборки(передается компилятору)

INCLUDEPATH+=\#"c:\Program Files\Microsoft SDKs\Windows\v7.1\Include." \
    c:\ocilib\include. \
    c:\postgresql-9.2.2\src\interfaces\libpq \
    c:\postgresql-9.2.3\src\interfaces\libpq \
    c:\postgresql-9.2.2\src\include. \
    c:\postgresql-9.2.3\src\include.
    ##c:\postgresql-9.2.2\src\include.\catalog \
    ##c:\postgresql-9.2.3\src\include.\catalog
    ##e:\Virtual Machines\exchange\cashsail_lin
LIBS+= c:\postgresql-9.2.2\src\interfaces\libpq\libpq.a

DESTDIR = ./rc
OBJECTS_DIR = ./debug

SOURCES += main.cpp \
    ##clistenserver.cpp \
    ##exceptions.cpp \
    ##cworkthread.cpp \
    database.cpp \
    loader.cpp \
    common.cpp \
    exceptions.cpp \
    codbcload.cpp \
    cdbfload.cpp

HEADERS += \
    common.h \
    ##clistenserver.h \
    ##cexception.h \
    ##cworkthread.h \
    database.h \
    loader.h \
    exceptions.h \
    codbcload.h \
    cdbfload.h

#CONFIG -=permissive
