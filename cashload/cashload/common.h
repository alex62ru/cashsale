#ifndef COMMON_H
#define COMMON_H
//общие данные и структуры, кучка маленьких удобных классов
//включаемые заголовки ТОЛЬКО QT ИЛИ С++
#include <libpq-fe.h>
#include <QObject>
#include <QString>
#include <QStringList>
#include <unistd.h>
#include <QMap>
#include <QVector>
#include <QDebug>
#include <QtEndian>
#include <QThread>
#include <QtGlobal>
#include <QTextCodec>
#include <QDateTime>
#include <QLocale> //отвечает за преобразование строк в соответствии с локалью
//зависящие от операционки header'ы и от вида приложения console-GUI
//#define OS_LINUX
#ifdef OS_LINUX
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <QDialog>
#include <QMessageBox>
#include <QErrorMessage>
#endif
//#define OS_WINDOWS
#ifdef OS_WINDOWS
#include <Windows.h>
//#include <winsock2.h>
#endif


class CGood
{
 public:
  int pos;//номер позиции
  QString articul;//артикул позиции, печатается на чеке, используется при возвратах
  QString description;//наименование
  QString cardsize;//размер
  QString price;//цена
  QString quantity;//колличество
  QString mesuriment;//единица измерения, может не потребоваться
  QString sum;//сумма без скидок
  QString discount;//сумма скидки
  QString total;//сумма с учетом скидки
  int department;//отдел
  CGood(int p, const QString& art, const QString& des, const QString& cs, int d, const QString& pr, const QString& q, const QString& s, const QString& dis, const QString& tl); //здесь 10 параметров
  //CGood(int p, const QString& art, const QString& des, double pr, double q, const QString& mes, double s, int d);
  CGood();
  /*CGood(const CGood& goo);
    CGood& operator=(const CGood& goo);*/
  virtual ~CGood();
};


class CInsertParams
{
public:
    //enum type{eInteger, eDouble, eDate, eString};
    void ApendValue(const char* value, short oid);//вставить параметр
    QString GetInsertValues();//возвращает форматированную строку параметров
    void Clear();//очищает массив параметров
    CInsertParams();
    ~CInsertParams();
protected:
    struct Param
    {
        QString value;
        Oid oid;
        Param(const QString& v, Oid o): value(v), oid(o)
            {

            }
        Param(): value(""), oid(0)
        {
            qDebug("Param(): value(""), oid(0) it's no good");
        }
    };
    QVector<Param> params;
};


class CDatabase;

struct WidgetParam//структура для передачи параметров в окна программы
{
  CDatabase* db;
};

struct LoadSettings //дополнить
{
    //bool single_cost; //работа только в розницу
    enum type {ODBC};
    QString flg_upd;//флаг добавления
    QString flg_ins;//флаг замены
    QString flg_lck;//флаг занятости каталога, выставляется загрузчиком
    QString load_dir;//каталог загрузки
    unsigned int timer;//период опроса каталога загрузки
    QString locale;//локаль обмена
    type class_loader;//класс-вид выгрузки по нему будет определяться, что нам нужно
    QString cashsale;//запрос формирования выгрузки контрольной ленты
    QString cashdisc;//-/-проведенных скидок
    QString currests;//-/-закрытых смен
    QString curmoney;//-/-движения наличности
    QString cashpay;//-/-проведенных платежей
    QString cashgood;//-/-товарный отчет
    QString alias;//псевдоним БД
};

struct ExchangeSettings //определяет способ выгрузки - по дате, по номеру отчета, по признаку невыгруженности
{
    enum exchange {eDate, eZnum, eWasOutput};
    exchange type;
    QString arg1;
    QString arg2;
    ExchangeSettings():type(eWasOutput), arg1(""), arg2("")
    {

    }
};

struct KeySettings//структура описателя кнопок
{
  QString object_name;//имя кнопки
  QString face_text;//надпись на кнопке
  QString shortcut;//клавиатурное сокращение
  QString face_font;//шрифт
  QString description;//описание, выводиться в подсказке
  QString icon_path;//путь к иконке
};

struct CP //описатель открываемого порта
{
  int number; //номер устройства, начинается с 0
  QString name;//имя, вид устройства, передается в виде 'scaner%'
  QString path; //путь, номер порта
  int mode; //режим открытия
  timeval timeout; //таймаут ожидания на select'е
CP(const QString& p, int m):path(p), mode(m)
  {
  }
CP():number(0), name(""), path(""), mode(0)
  {

  }
};


struct ScanerSettings //структура описателя сканера
{
  QString name;
  QString port;//путь порта
  //ScanerSettings(const QString& nm): name(nm)
};

struct Classif
{
  unsigned short groop1;
  unsigned short groop2;
  unsigned short groop3;
  unsigned short groop4;
  unsigned short groop5;
  QString description;
};

struct ClassifModel: Classif
{
  QString name;
  QString cost;
};

struct Plu //структура загрузки plu
{
    QString articul;
    QString description;
    QString mesuriment;// для текущей загрузки
    double mespresision;
    QString add1;
    QString add2;
    QString add3;
    double addnum1;
    double addnum2;
    double addnum3;
    QString scale;
    unsigned short groop1;
    unsigned short groop2;
    unsigned short groop3;
    unsigned short groop4;
    unsigned short groop5;
    unsigned short department;
    bool deleted;
    double price;
};


struct Depart
{
    unsigned short department;
    QString description;
};

struct Scales
{
    QString scale;
    QString description;
};

struct Pricekin
{
    unsigned short priecelist;
    QString description;
};

/*struct Mesuriment
{
    unsigned short mesuriment;
    QString description;
};*/

struct Costs
{
    QString articul;
    double price;
    unsigned short pricelist;
};

struct Barcode
{
    QString bar;
    QString articul;
    //QString scale;
    QString cardsize;
    double quantity;
};

struct Personal
{
    unsigned short ident;
    QString description;
    QString passw;
    unsigned short accesslevel;
};

struct Tax
{
    QString articul;
    unsigned short taxindex;
    double taxvalue;
    double taxsum;
};

struct Sizes
{
    QString scale;
    QString cardsize;
};

struct Disccli
{
    QString barcode;
    QString description;
    double discount;
    unsigned short pricelist;
    unsigned short clientindex;
};

struct Cliclass
{
    unsigned short groop1;
    unsigned short groop2;
    unsigned short groop3;
    unsigned short groop4;
    unsigned short groop5;
    QString barcode;
    double discount;
    unsigned short pricelist;
};

struct Credcard
{
    unsigned short CredCardIndex;
    QString description;
    double limitsum;
    unsigned short canreturn;
    QString soft;
    QString ident;
};

struct Dclilst
{
    QString codeend;
    QString codestart;
};

struct LoadTable
{
    short plu;
    short barcode;
    short clas;
    short price;
    short department;
    short scale;
    short pricekin;
    //unsigned short mesuriment;
    short costs;
    short sizes;
    short personal;
    short tax;
    short disccli;
    short cliclass;
    short credcard;
    short dclislst;
    //дополнить
};

//секция выгрузки
struct Index //индексное поле
{
    short shopindex;
    short cashnumber;
    short znumber;
    short checknumber;
    short id;
};

struct Cashsale
{
    Index idx;
    QDateTime dt;
    QString articul;
    QString size;
    double quantity;
    double price;
    double total;
    short department;
    short casher;
    short usingindex;
    short replace;
    short operation;
    short credcardindex;
    short discclinndex;
    short linked;
};



/*struct LoadSettings //дополнить
{
    QString plu_path;//путь к Plucash
    QString barcode_path;
    QString clas_path;
    QString price_path;
    QString depart_path;
    QString scales_path;
    QString pricekin_path;
    //QString mesuriment_path;
    QString costs_path;
    QString sizes_path;
    QString personal_path;
    QString tax_path;
    QString disccli_path;
    QString cliclass_path;
    QString credcard_path;
    QString dclislst_path;
    bool single_cost; //работа только в розницу
    QString flg_upd;//флаг добавления
    QString flg_change;//флаг замены
    QString load_dir;//каталог загрузки
    unsigned int timer;//период опроса каталога загрузки
};*/

enum eCashSatate{eCloseCheck=0, eOpenCheck=1, eSubtotal=2};//перечисление состояния чека в окне кассира

//структыры отображения
  class ItemText
  {
  public:
    QString text;
    Qt::Alignment align;
    Qt::GlobalColor back;
    //QFont col_font;
  ItemText(QString qs, Qt::Alignment a, Qt::GlobalColor b): text(qs), align(a), back(b)
    {

    }
  };


#endif //COMMON_H
