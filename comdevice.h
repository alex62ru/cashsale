#ifndef COMDEVICE_H
#define COMDEVICE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "common.h"
#include <termios.h> /* Объявления управления POSIX-терминалом */
//собраны классы работы через COM порты

#define SCANER_LENGTH 129 //размер буфера для сканера
#define PRINTER_LENGTH 1024 //размер буфера для принтера

class QMutex;
class CDatabase;
class CDBException;
class CPortException;//класс исключений компорта



class CComPort: public QThread//класс работы с портом, работа всегда асинхронная
{
  Q_OBJECT

    public:
  enum eDevice {eLast, eContinue, eNotConnected, eProtocolError, eBusy, eReadComplite};//для определения состояния чтения - последее устройство в списке, продолжить выполнение цикла чтения, не подключено, занято, цикл чтения завершенo
  CComPort(CDatabase* d);
  virtual ~CComPort();
  int read(char *buf) const;//чтение данных из порта
  void write(const char *buf, int length) throw (CPortException);//запись данных в порт
  void flush() throw (CPortException);//очистить очереди приема и передачи
  QMutex *r_mutex;
  QMutex *w_mutex;
 protected:
  char *read_bufer;//буфер чтения, с ним работают функции чтения из порта
  char *write_buffer;//буфер записи, с ним работают функции записи в порт
  int fd_port;//файловый дескриптор порта
  termios options;
  fd_set *r_set;//массив дискрипторов для чтения
  fd_set *w_set;//массив дескрипторов для записи
  int r_length;//количество ожидаемых байт для чтения
  int w_length;//количество байт для записи
  timeval *timeout;//структура таймаута ожидания
  CP cp;//структура с настройками порта
  CDatabase *db;//указатель на движек БД, чтение настроек
  //virtual void GetSettings() throw (CDBException) = 0; //чтение настроек порта
  virtual void SetTermios() throw (CPortException) = 0;//настройка com порта
  virtual void SetSelect() throw (CPortException) = 0;//настройка дескрипторов ожидания
  //void PortClear();
  virtual void InitFDSET() = 0;//инициализация массива дескрипторов ожидания
  virtual void run();
  virtual eDevice runon() throw () = 0;// операции чтения, убедиться, что исключений не будет...разбирает буфер чтения и выставляет признаки
 signals:
  void ReadCompleted();//сигнализирует о выполнении операции чтения
  void WriteCompleted();//сигнализирует о завершении операции чтения
  void ExceededTimeout();//сигнализирует о превышении таймаута ожидания
  void DeviceIsBusy();//сигнализирует о занятости устройства
  void DataError();//ошибка протокола
  void DeviceNotConnected();//устройство не подключено
};



class CScaner: public CComPort
{
  Q_OBJECT
    protected:
  static int number;//номер сканера
  virtual void SetTermios() throw (CPortException);
  virtual void SetSelect() throw (CPortException);
  virtual void InitFDSET();
  void GetSettings() throw (CDBException);
  eDevice runon() throw();
  protected slots:
  void ReadData();
 public:
  CScaner(CDatabase* db);
  virtual ~CScaner();
  //public slots:
 signals:
  virtual void Read(const QString& bc);//сигнализирует о данных в приемном буфере и передает их
};


#endif //COMDEVICE_H
