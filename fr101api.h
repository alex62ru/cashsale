﻿#ifndef _FR101API_H
#define _FR101API_H

#include "comdevice.h"
#include <ctime>

#ifndef longlong
#define longlong long long
#endif

#define LONGLONG_PREFIX "ll"

/* STARTUP COMMAND GROUP */
#define FR_INITIALIZE             0x00
#define FR_REGISTER               0x08
#define FR_EEJ_ACTIVATE           0x09
#define FR_EEJ_CLOSE_ARCHIEVE     0x0A
/* RECEIPT COMMAND GROUP */
#define FR_OPEN_RECEIPT           0x20
#define FR_PRINT_TEXT             0x21
#define FR_CLOSE_RECEIPT          0x22
#define FR_CANCEL_RECEIPT         0x23
#define FR_PRINT_BARCODE          0x24
#define FR_BANNER                 0x25
#define FR_STORE_RECEIPT          0x27
#define FR_ADD_ITEM               0x30
#define FR_VOID_ITEM              0x31
#define FR_ADD_DISCOUNT           0x32
#define FR_ADD_MARKUP             0x33
#define FR_SUBTOTAL               0x34
#define FR_PAYMENT                0x35
#define FR_PAID_IN_OUT            0x36
#define FR_SET_TAX_AMOUNT         0x37
/* REPORT COMMAND GROUP */
#define FR_PRINT_X_REP            0x60
#define FR_PRINT_Z_REP            0x61
#define FR_PRINT_FIS_SHIFT_REP    0x62
#define FR_PRINT_FIS_DATE_REP     0x63
#define FR_PRINT_EEJ_JOURNAL      0x64
#define FR_PRINT_EEJ_DOCUMENT     0x65
#define FR_PRINT_EEJ_SHIFT_REP    0x66
#define FR_PRINT_EEJ_DATE_REP     0x67
#define FR_PRINT_EEJ_ACTIVATE_REP 0x68
#define FR_PRINT_EEJ_SINGLE_SHIFT_REP 0x69
#define FR_EMERGENT_CLOSE_SHIFT   0x70
#define FR_OPEN_DRAWER            0x80
#define FR_PRINT_CUST_DISP_TEXT   0x81
/* SYSTEM COMMAND GROUP */
#define FR_GET_STATUS             0xA0
#define FR_READ_CONFIG            0xA1
#define FR_WRITE_CONFIG           0xA2
#define FR_SET_DATE_TIME          0xA3
#define FR_GET_EEJ_STATUS         0xA4
#define FR_GET_SHIFT_STATUS       0xA5
#define FR_GET_KKM_SERIAL_NUM     0xA6
#define FR_GET_DATE_TIME          0xA7
#define FR_GET_DRIVER_OPENED      0xA8
/* INTERNAL COMMAND GROUP */
#define FR_INSTALL_KKM            0xE0

/* ERROR CODES */
#define FR_SUCCESS                  0x00
#define FR_BUSY                     0x7F
/* I/O ERRORS */
#define FR_ERR_INVALID_STATUS       0x01
#define FR_ERR_INVALID_FUNCTION     0x02
#define FR_ERR_INVALID_PARAMETER    0x03
/* TRANSPORT LEVEL ERRORS */
#define FR_ERR_UART_OVERFLOW        0x04
#define FR_ERR_INTERCHAR_TIMEOUT    0x05
#define FR_ERR_INVALID_PASSWORD     0x06
#define FR_ERR_INVALID_CHECKSUM     0x07
/* PRINTER ERRORS */
#define FR_ERR_OUT_OF_PAPER         0x08
#define FR_ERR_PRINTER_NOT_READY    0x09
/* DATE/TIME ERRORS */
#define FR_ERR_24HOUR_OVERFLOW      0x0A
#define FR_ERR_TIMESYNC_OVERFLOW    0x0B
#define FR_ERR_TOO_LOW_DOC_TIME     0x0C
#define FR_ERR_NO_DOC_HEADER        0x0D
#define FR_ERR_NEGATIVE_TOTAL       0x0E
#define FR_ERR_DISPLAY_NOT_READY    0x0F

/* FATAL ERRORS - BY KOMAX */
#define FR_ERR_DATA_SEND            0x10
#define FR_ERR_NO_DATA              0x11
#define FR_ERR_PORT_OPEN            0x12
#define FR_ERR_DEVICE_UNPLUGGED     0x13
#define FR_ERR_DEVICE_BUSY          0x14
#define FR_ERR_PROTOCOL             0x15

/* FATAL ERRORS */
#define FR_ERR_FATAL                0x20
#define FR_ERR_OUT_OF_MEMORY        0x21
#define FR_ERR_DEVICE_TIMEOUT       0x30
/* EEJ ERRORS */
#define FR_ERR_EEJ_WRONG_ARG_FORMAT 0x41
#define FR_ERR_EEJ_INCORRECT_STATUS 0x42
#define FR_ERR_EEJ_EMERGENCY        0x43
#define FR_ERR_EEJ_CRYPTO_EMERGENCY 0x44
#define FR_ERR_EEJ_EXPIRED          0x45
#define FR_ERR_EEJ_OVERFLOW         0x46
#define FR_ERR_EEJ_WRONG_DATE_TIME  0x47
#define FR_ERR_EEJ_NO_DATA          0x48
#define FR_ERR_EEJ_DOC_OVERFLOW     0x49
#define FR_ERR_EEJ_TIMEOUT          0x4A
#define FR_ERR_EEJ_COMMUNICATION    0x4B

#ifndef BYTE
#define BYTE unsigned char
#endif

#ifndef SUCCESS
#define SUCCESS 0
#endif

#ifndef ERROR
#define ERROR -1
#endif

#define FR_INTERCHAR_TIMEOUT   1
#define FR_SIZEOF_BUFFER     300
#define FR_MAX_LENGTH         40



enum eDiscType
{
  DSC_PERCENT='%',
  DSC_ABSOLUTE='S'
};

enum eRequestType
{
  REQ_CURRENT_SHIFT='1',
  REQ_LAST_REG_SHIFT='2',
  REQ_SALE_TOTALS='3',
  REQ_RETURN_TOTALS='4'
};

#define FR_DOC_NFISCAL  0x01
#define FR_DOC_SALE     0x02
#define FR_DOC_RETURN   0x03
#define FR_DOC_PAID_IN  0x04
#define FR_DOC_PAID_OUT 0x05

#define FR_DOC_OPEN  0x10
#define FR_DOC_TOTAL 0x20
#define FR_DOC_MEDIA 0x30
#define FR_DOC_CLOSE 0x40

#pragma pack(1)

typedef struct
{
  unsigned short fbNVRIncorrectCRC    :1;
  unsigned short fbConfigIncorrectCRC :1;
  unsigned short fbSPIInterfaceError  :1;
  unsigned short fbMFIncorrectCRC     :1;
  unsigned short fbMFWriteError       :1;
  unsigned short fbMFNotInstalled     :1;
  unsigned short fbEEJFatalError      :1;
  unsigned short fbUnused1            :1;

  unsigned short fbNotInitialized     :1;
  unsigned short fbNonFiscalMode      :1;
  unsigned short fbShiftOpened        :1;
  unsigned short fb24HourOverflow     :1;
  unsigned short fbEEJArchieveClosed  :1;
  unsigned short fbEEJNotActivated    :1;
  unsigned short fbMFNoMemoryForShift :1;
  unsigned short fbMFWrongPassword    :1;

  unsigned char dbDocumentType;
} ST_FRSTATUS;

#pragma pack()

class CPortException;
class CDatabase;
class CDBException;

class CFR101_API: public CComPort
{
  virtual void SetTermios() throw (CPortException);
  virtual void InitFDSET();
  void GetSettings() throw (CDBException);
  static int number;
  CP cp;
  //фунция необходимая для "сборки" ответа и ее переменные в пределах класса
  virtual CComPort::eDevice runon() throw ();
  char bWaitACK;// = 0
  char bWaitEOT;// = 3
  char  bWaitSTX;// = 1;
  BYTE  bFR_Packet; // 0x20 ID команды, нарастает до 0xFD 
  BYTE bCommand;//команда регистратору
  typedef std::vector<char> BUFFER;
  BUFFER vFR_Command;//буфер запроса - изменяемая величина
  char* argument;//аргумент команды запроса
  BUFFER vFR_Answer;//буфер ответа - изменяемая величина
  enum eLRC{eCalc=false, eCheck=true};
  BYTE dbFR_GetLRC(BUFFER& b, eLRC lrc);//посчитать КС и вернуть ее, второй аргумент показывает считать или проверять
  bool nFR_CheckLRC(BUFFER& b);//(вектор для проверки КС, полный
  BYTE bFR_Command;//команда ФРу
  int  nFR_BuildPacket();//собирает пакет к отправке
  int  nFR_SendPacket();//отправить команду на регистратор

 public:
  CFR101_API(CDatabase *db);
  virtual ~CFR101_API();
  //функции общего назначения
  void fr_initialize(time_t tt = 0);//инициализация ККМ
  //функции снятия отчетов
  void nFR_PrintXReport(const char* oper);//печать X-отчета
};

#endif /* _FR101API_H */
