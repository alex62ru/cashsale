#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include "exceptions.h"
#include "common.h"
 
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  //QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));// необходимо разобраться с кодеками
  //setlocale(LCC_ALL, "ru_RU.UTF8");
  QTranslator translator;
  translator.load("cashsail_ru.qm", "./");
  a.installTranslator(&translator);
  //QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
  //QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
  try
    {
      MainWindow w;
      w.showFullScreen();
      //w.showMaximized();
      w.setCashWidget();
      return a.exec();
    }
  catch (CDBException e)
    {
      QMessageBox::critical(NULL, QObject::tr("Critical error"), QObject::tr("Application terminate"));
    }
}
