#include <stdio.h>   /* Стандартные объявления ввода/вывода */
#include <string.h>  /* Объявления строковых функций */
#include <unistd.h>  /* Объявления стандартных функций UNIX */
#include <fcntl.h>   /* Объявления управления файлами */
#include <errno.h>   /* Объявления кодов ошибок */
#include <QMutex>



#include "comdevice.h"
#include "exceptions.h"
#include "database.h"


CComPort::CComPort(CDatabase* d): fd_port(-1), r_set(NULL), w_set(NULL), w_length(0), r_length(0), timeout(NULL), r_mutex(NULL), w_mutex(NULL), db(d)
{
  //здесь только инициализируем структуры, порт открывается в конструкторах-потомках
  r_mutex=new QMutex();
  //w_mutex=new QMutex();
  r_set = new fd_set;
  w_set = new fd_set;
}

CComPort::~CComPort()
{
  if(r_set) delete r_set;
  if(w_set) delete w_set;
  if(fd_port!=-1) close(fd_port);
  //if(timeout) delete timeout;
  if(r_mutex) delete r_mutex;
  //if(w_mutex) delete w_mutex;
}


void CComPort::run()
{
  int result;//возвращаемый результат
  while(true)
    {
      //QMessageBox::warning(NULL, "void CComPort::run()", "void CComPort::run()");

      InitFDSET();
      r_mutex->lock();//лочим цикл чтения, в дальнейшем думаю будет только одна блокировка
      result=select(fd_port+1, r_set, w_set, NULL, timeout);
      //qDebug()<<"result=select(fd_port+1, r_set, w_set, NULL, timeout);"<<result;
      //if(result<0) throw CPortException();
      if(result==0)//превышен таймаут ожидания
	{
	  r_mutex->unlock();
	  emit ExceededTimeout();
	  //qDebug("CComPort::ExceededTimeout");
	}
      if(FD_ISSET(fd_port, r_set))//правильный способ работы, у сканера должен быть один суффикс или CR или LF
	{
	  //может быть проблема!!! подобрать или таймаут или канонические-неканонический ввод
	  memset(read_bufer, 0, r_length);
	  result=::read(fd_port, read_bufer, r_length);
	  //обработать ошибки чтения
	  r_length=result;
	  //qDebug("Data in bufer %s", read_bufer);
	  //emit ReadCompleted();
	  switch(runon())
	    {
	    case eContinue:
	      qDebug("switch(runon()) = eContinue");
	      continue;
	      break;
	    case eNotConnected:
	      qDebug("switch(runon()) = eNotConnected");
	      break;
	    case eProtocolError:
	      qDebug("switch(runon()) = eProtocolError");
	      break;
	    case eBusy:
	      qDebug("switch(runon()) = eBusy");
	      break;
	    case eReadComplite:
	      qDebug("switch(runon()) = eReadComplite");
	      emit ReadCompleted();
	      break;
	    default: continue;
	      break;
	    }
	}
      if(FD_ISSET(fd_port, w_set))
	{
	  //здесь еще предстоит разобраться
	  emit WriteCompleted();
	}
    }
}

/*void CComPort::runon() throw (eDevice)
{
  emit ReadCompleted();
}
*/
   
int CComPort::read(char *buf) const//передавать в функцию нужно ненулевой указатель с выделенной памятью, потом может быть передумаю
{
  Q_ASSERT(buf); 
  memcpy(buf, read_bufer, r_length);
  r_mutex->unlock();
  return r_length;
}

void CComPort::write(const char* buf, int length) throw (CPortException)
{
  Q_ASSERT(w_length>=length);
  memcpy(write_buffer, buf, length);
  r_mutex->unlock();
  if(::write(fd_port, write_buffer, length)==-1) throw CPortException();
}

void CComPort::flush() throw (CPortException)
{
  if(tcflush(fd_port, TCIOFLUSH)==-1) throw CPortException();
}

//*****************

int CScaner::number(1);

CScaner::CScaner(CDatabase *db): CComPort(db)
{
  GetSettings();
  if(cp.path.isEmpty()) throw eLast;//если пустая строка в имени порта - исключение
  fd_port=open(cp.path.toLocal8Bit().constData(), cp.mode);
  if(fd_port==-1) throw CPortException();//в конструкторе исключения реализовать проверку errno
  if(fcntl(fd_port, F_SETFL, FNDELAY)==-1) throw CPortException();
  read_bufer = new char[SCANER_LENGTH];//выделение паияти под буфер чтения сканера
  r_length=SCANER_LENGTH;
  SetTermios();
  //InitFDSET();
  connect(this, SIGNAL(ReadCompleted()), this, SLOT(ReadData()));
  start();
  qDebug("CScaner::start()");
  number++;//увеличение номера устройства
}

void CScaner::GetSettings() throw (CDBException)
{
  cp.number = number;//присвоение номера устройства
  char szTemp[]="scaner%dd";  
  snprintf(szTemp, sizeof("scaner%dd"), "scaner%02d", cp.number);
  qDebug("GetSettings() %s", szTemp);
  cp.name = szTemp;//вид устройств
  db->GetPortSettings(cp);
  cp.mode = O_RDONLY | O_NOCTTY | O_NDELAY;
}

CScaner::~CScaner()
{
  terminate();
  wait();
  delete [] read_bufer;
  number--;//уменьшение номера устройства
}

void CScaner::SetTermios() throw (CPortException)
{
  tcgetattr(fd_port, &options);
  cfsetispeed(&options, B9600);
  cfsetospeed(&options, B9600);
  options.c_cflag &= ~CRTSCTS;//отключение аппаратного контроля
  options.c_iflag &= ~(IXON | IXOFF | IXANY);//отключение программного контроля
  //options.c_lflag |= (ICANON | ECHO | ECHOE);//канонический ввод
  options.c_lflag |= ~(ICANON | ECHO | ECHOE | ISIG);//неканонический ввод
  options.c_cflag &= ~CSIZE; /* Маскирование битов размера символов */
  options.c_cflag |= CS8;    /* Установка 8 битов данных */
  options.c_cflag &= ~PARENB;//отсутствие проверки на четность
  options.c_cflag &= ~CSTOPB;
  if(tcsetattr(fd_port, TCSAFLUSH, &options)==-1) throw CPortException();//запись структуры termios
}

void CScaner::SetSelect() throw (CPortException)
{
  //подумать и удалить
}

void CScaner::ReadData()
{
  char buf[SCANER_LENGTH];
  memset(buf, 0, SCANER_LENGTH);
  read(buf);
  emit Read(QString(buf));
  //r_mutex->Unlock();//сформировали код, разлочиваем мьютекс
}

void CScaner::InitFDSET()
{
  FD_ZERO(r_set);
  FD_SET(fd_port, r_set);//добавляем дескриптор открытого порта в массив дескрипторов ожидания чтения
  FD_ZERO(w_set);
  qDebug()<<"InitFDSET();";
}

CComPort::eDevice CScaner::runon() throw()
{
  flush();
  return eReadComplite;
}


