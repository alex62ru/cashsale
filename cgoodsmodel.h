#ifndef CGOODSMODEL_H
#define CGOODSMODEL_H
/*
класс модели, определяет как будут выглядеть элементы(преимущественно)
обычные позиции с ненулевым количеством - обычный шрифт
позиции с нулевм количеством - красные
 */


#include <QAbstractTableModel>
//#include <QMap>
#include "common.h"



class CGoodsModel: public QAbstractTableModel
{
  Q_OBJECT
    QList<ItemText> columns;//список заголовков
  enum {eDescription=0, eCost=1, eQuantity=2, eSum=3, eDiscount=4, eTotal=5};
 public:
  QMap<int, CGood> goods;
  CGoodsModel(QObject* parent);
  void UpdateGood(const CGood& plu/*, bool upd*/);//добавить товарную позицию
  void SetGoods(const QMap<int, CGood>& goo);
  void SetColumns(const QList<ItemText>& col);
  //унаследованные
  int rowCount(const QModelIndex &parent) const;
  int columnCount(const QModelIndex &parent) const;
  QVariant data(const QModelIndex &index, int role) const;
  //bool setData(const QModelIndex &index, const QVariant &value, int role);
  QVariant headerData(int section, Qt::Orientation orientation, int role) const;
  Qt::ItemFlags flags(const QModelIndex &index) const;
  void ClearAll();// удалить все товарные позиции
};

#endif // CGOODSMODEL_H
