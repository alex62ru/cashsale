#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QWidgetCash;
class CDatabase;
class QErrorMessage;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
    
    public:
  explicit MainWindow(QWidget *parent = NULL);
  ~MainWindow();
  void setCashWidget();    
 private:
  Ui::MainWindow *ui;
  CDatabase *db;//указатель на движек БД
  QWidgetCash *cash;//указатель на окно кассира
  QErrorMessage* errorMessage;//указатель на окно сообщения об ошибке
};

#endif // MAINWINDOW_H
