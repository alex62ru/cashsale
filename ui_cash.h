/********************************************************************************
** Form generated from reading UI file 'cash.ui'
**
** Created: Thu Nov 14 17:44:12 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CASH_H
#define UI_CASH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QTableView>
#include <QtGui/QTreeView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CashForm
{
public:
    QGridLayout *gridLayout_2;
    QSplitter *hSplitter;
    QTableView *tvCheck;
    QSplitter *vSplitter;
    QTabWidget *tabWidget;
    QWidget *tabSale;
    QGridLayout *gridLayout;
    QSplitter *hSplitterTabWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_3;
    QLCDNumber *lcdNumber;
    QSpacerItem *verticalSpacer_5;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayoutGood;
    QLabel *label_1;
    QLabel *label_Product;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_Cost;
    QLabel *label_Quantity;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_Sum;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QLineEdit *lineEdit;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QWidget *tabClassif;
    QGridLayout *gridLayout_3;
    QSplitter *splitter;
    QTreeView *tvClassif;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pbOk;
    QPushButton *pbCancel;
    QWidget *tabGood;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayoutButton;
    QGridLayout *gridLayoutButton;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_Num;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_Art;
    QPushButton *pushButton_1;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_Quantity;
    QPushButton *pushButton_Pay;
    QPushButton *pushButton_0;
    QPushButton *pushButton_Decimal;
    QPushButton *pushButton_Function;
    QSpacerItem *verticalSpacer_6;

    void setupUi(QWidget *CashForm)
    {
        if (CashForm->objectName().isEmpty())
            CashForm->setObjectName(QString::fromUtf8("CashForm"));
        CashForm->setEnabled(true);
        CashForm->resize(1036, 781);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CashForm->sizePolicy().hasHeightForWidth());
        CashForm->setSizePolicy(sizePolicy);
        CashForm->setMinimumSize(QSize(800, 600));
        gridLayout_2 = new QGridLayout(CashForm);
        gridLayout_2->setSpacing(4);
        gridLayout_2->setContentsMargins(4, 4, 4, 4);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        hSplitter = new QSplitter(CashForm);
        hSplitter->setObjectName(QString::fromUtf8("hSplitter"));
        hSplitter->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(hSplitter->sizePolicy().hasHeightForWidth());
        hSplitter->setSizePolicy(sizePolicy1);
        hSplitter->setMinimumSize(QSize(800, 300));
        hSplitter->setFrameShape(QFrame::StyledPanel);
        hSplitter->setMidLineWidth(4);
        hSplitter->setOrientation(Qt::Vertical);
        tvCheck = new QTableView(hSplitter);
        tvCheck->setObjectName(QString::fromUtf8("tvCheck"));
        sizePolicy1.setHeightForWidth(tvCheck->sizePolicy().hasHeightForWidth());
        tvCheck->setSizePolicy(sizePolicy1);
        tvCheck->setMinimumSize(QSize(790, 250));
        hSplitter->addWidget(tvCheck);
        vSplitter = new QSplitter(hSplitter);
        vSplitter->setObjectName(QString::fromUtf8("vSplitter"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(vSplitter->sizePolicy().hasHeightForWidth());
        vSplitter->setSizePolicy(sizePolicy2);
        vSplitter->setMinimumSize(QSize(1012, 300));
        vSplitter->setOrientation(Qt::Horizontal);
        tabWidget = new QTabWidget(vSplitter);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tabWidget->setMinimumSize(QSize(593, 100));
        tabSale = new QWidget();
        tabSale->setObjectName(QString::fromUtf8("tabSale"));
        tabSale->setEnabled(true);
        sizePolicy2.setHeightForWidth(tabSale->sizePolicy().hasHeightForWidth());
        tabSale->setSizePolicy(sizePolicy2);
        tabSale->setMinimumSize(QSize(585, 386));
        gridLayout = new QGridLayout(tabSale);
        gridLayout->setSpacing(4);
        gridLayout->setContentsMargins(4, 4, 4, 4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        hSplitterTabWidget = new QSplitter(tabSale);
        hSplitterTabWidget->setObjectName(QString::fromUtf8("hSplitterTabWidget"));
        sizePolicy2.setHeightForWidth(hSplitterTabWidget->sizePolicy().hasHeightForWidth());
        hSplitterTabWidget->setSizePolicy(sizePolicy2);
        hSplitterTabWidget->setMinimumSize(QSize(577, 378));
        hSplitterTabWidget->setOrientation(Qt::Vertical);
        layoutWidget = new QWidget(hSplitterTabWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        sizePolicy2.setHeightForWidth(layoutWidget->sizePolicy().hasHeightForWidth());
        layoutWidget->setSizePolicy(sizePolicy2);
        layoutWidget->setMinimumSize(QSize(577, 99));
        verticalLayout_3 = new QVBoxLayout(layoutWidget);
        verticalLayout_3->setSpacing(4);
        verticalLayout_3->setContentsMargins(4, 4, 4, 4);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_3 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout_3->addItem(verticalSpacer_3);

        lcdNumber = new QLCDNumber(layoutWidget);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        sizePolicy2.setHeightForWidth(lcdNumber->sizePolicy().hasHeightForWidth());
        lcdNumber->setSizePolicy(sizePolicy2);
        lcdNumber->setMinimumSize(QSize(575, 33));
        lcdNumber->setSmallDecimalPoint(true);
        lcdNumber->setDigitCount(7);
        lcdNumber->setSegmentStyle(QLCDNumber::Filled);
        lcdNumber->setProperty("value", QVariant(8889));
        lcdNumber->setProperty("intValue", QVariant(8889));

        verticalLayout_3->addWidget(lcdNumber);

        verticalSpacer_5 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout_3->addItem(verticalSpacer_5);

        verticalLayout_3->setStretch(0, 1);
        verticalLayout_3->setStretch(1, 7);
        verticalLayout_3->setStretch(2, 1);
        hSplitterTabWidget->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(hSplitterTabWidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        sizePolicy2.setHeightForWidth(layoutWidget1->sizePolicy().hasHeightForWidth());
        layoutWidget1->setSizePolicy(sizePolicy2);
        layoutWidget1->setMinimumSize(QSize(577, 151));
        verticalLayoutGood = new QVBoxLayout(layoutWidget1);
        verticalLayoutGood->setSpacing(4);
        verticalLayoutGood->setContentsMargins(4, 4, 4, 4);
        verticalLayoutGood->setObjectName(QString::fromUtf8("verticalLayoutGood"));
        verticalLayoutGood->setSizeConstraint(QLayout::SetNoConstraint);
        verticalLayoutGood->setContentsMargins(0, 0, 0, 0);
        label_1 = new QLabel(layoutWidget1);
        label_1->setObjectName(QString::fromUtf8("label_1"));
        sizePolicy2.setHeightForWidth(label_1->sizePolicy().hasHeightForWidth());
        label_1->setSizePolicy(sizePolicy2);
        label_1->setMinimumSize(QSize(575, 23));
        QFont font;
        font.setPointSize(12);
        label_1->setFont(font);

        verticalLayoutGood->addWidget(label_1);

        label_Product = new QLabel(layoutWidget1);
        label_Product->setObjectName(QString::fromUtf8("label_Product"));
        sizePolicy2.setHeightForWidth(label_Product->sizePolicy().hasHeightForWidth());
        label_Product->setSizePolicy(sizePolicy2);
        label_Product->setMinimumSize(QSize(575, 24));
        QFont font1;
        font1.setPointSize(15);
        label_Product->setFont(font1);

        verticalLayoutGood->addWidget(label_Product);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayoutGood->addItem(verticalSpacer_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(4);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy2.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy2);
        label_3->setMinimumSize(QSize(129, 21));
        label_3->setFont(font);

        horizontalLayout->addWidget(label_3);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy2.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy2);
        label_4->setMinimumSize(QSize(130, 21));
        QFont font2;
        font2.setPointSize(12);
        font2.setItalic(false);
        label_4->setFont(font2);

        horizontalLayout->addWidget(label_4);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);
        label_5->setMinimumSize(QSize(129, 21));
        label_5->setFont(font);

        horizontalLayout->addWidget(label_5);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 3);
        horizontalLayout->setStretch(2, 4);
        horizontalLayout->setStretch(3, 3);

        verticalLayoutGood->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(4);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_Cost = new QLabel(layoutWidget1);
        label_Cost->setObjectName(QString::fromUtf8("label_Cost"));
        sizePolicy2.setHeightForWidth(label_Cost->sizePolicy().hasHeightForWidth());
        label_Cost->setSizePolicy(sizePolicy2);
        label_Cost->setMinimumSize(QSize(129, 21));
        label_Cost->setFont(font1);

        horizontalLayout_2->addWidget(label_Cost);

        label_Quantity = new QLabel(layoutWidget1);
        label_Quantity->setObjectName(QString::fromUtf8("label_Quantity"));
        sizePolicy2.setHeightForWidth(label_Quantity->sizePolicy().hasHeightForWidth());
        label_Quantity->setSizePolicy(sizePolicy2);
        label_Quantity->setMinimumSize(QSize(130, 21));
        label_Quantity->setFont(font1);

        horizontalLayout_2->addWidget(label_Quantity);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_Sum = new QLabel(layoutWidget1);
        label_Sum->setObjectName(QString::fromUtf8("label_Sum"));
        sizePolicy2.setHeightForWidth(label_Sum->sizePolicy().hasHeightForWidth());
        label_Sum->setSizePolicy(sizePolicy2);
        label_Sum->setMinimumSize(QSize(129, 21));
        label_Sum->setFont(font1);

        horizontalLayout_2->addWidget(label_Sum);

        horizontalLayout_2->setStretch(0, 3);
        horizontalLayout_2->setStretch(1, 3);
        horizontalLayout_2->setStretch(2, 4);
        horizontalLayout_2->setStretch(3, 3);

        verticalLayoutGood->addLayout(horizontalLayout_2);

        verticalLayoutGood->setStretch(0, 2);
        verticalLayoutGood->setStretch(1, 6);
        verticalLayoutGood->setStretch(2, 1);
        verticalLayoutGood->setStretch(3, 2);
        verticalLayoutGood->setStretch(4, 6);
        hSplitterTabWidget->addWidget(layoutWidget1);
        layoutWidget2 = new QWidget(hSplitterTabWidget);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        sizePolicy2.setHeightForWidth(layoutWidget2->sizePolicy().hasHeightForWidth());
        layoutWidget2->setSizePolicy(sizePolicy2);
        layoutWidget2->setMinimumSize(QSize(577, 122));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_3->setSpacing(4);
        horizontalLayout_3->setContentsMargins(4, 4, 4, 4);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout->addItem(verticalSpacer);

        lineEdit = new QLineEdit(layoutWidget2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        sizePolicy2.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy2);
        lineEdit->setMinimumSize(QSize(450, 40));
        QFont font3;
        font3.setPointSize(40);
        lineEdit->setFont(font3);

        verticalLayout->addWidget(lineEdit);

        verticalSpacer_2 = new QSpacerItem(130, 31, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout->addItem(verticalSpacer_2);

        verticalLayout->setStretch(0, 1);
        verticalLayout->setStretch(1, 3);
        verticalLayout->setStretch(2, 2);

        horizontalLayout_3->addLayout(verticalLayout);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        horizontalLayout_3->setStretch(0, 15);
        horizontalLayout_3->setStretch(1, 1);
        hSplitterTabWidget->addWidget(layoutWidget2);

        gridLayout->addWidget(hSplitterTabWidget, 0, 0, 1, 1);

        tabWidget->addTab(tabSale, QString());
        tabClassif = new QWidget();
        tabClassif->setObjectName(QString::fromUtf8("tabClassif"));
        sizePolicy2.setHeightForWidth(tabClassif->sizePolicy().hasHeightForWidth());
        tabClassif->setSizePolicy(sizePolicy2);
        tabClassif->setMinimumSize(QSize(585, 386));
        gridLayout_3 = new QGridLayout(tabClassif);
        gridLayout_3->setSpacing(4);
        gridLayout_3->setContentsMargins(4, 4, 4, 4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        splitter = new QSplitter(tabClassif);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        sizePolicy1.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy1);
        splitter->setOrientation(Qt::Vertical);
        tvClassif = new QTreeView(splitter);
        tvClassif->setObjectName(QString::fromUtf8("tvClassif"));
        splitter->addWidget(tvClassif);
        layoutWidget3 = new QWidget(splitter);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        horizontalLayout_4 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_4->setSpacing(4);
        horizontalLayout_4->setContentsMargins(4, 4, 4, 4);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        pbOk = new QPushButton(layoutWidget3);
        pbOk->setObjectName(QString::fromUtf8("pbOk"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(pbOk->sizePolicy().hasHeightForWidth());
        pbOk->setSizePolicy(sizePolicy3);

        horizontalLayout_4->addWidget(pbOk);

        pbCancel = new QPushButton(layoutWidget3);
        pbCancel->setObjectName(QString::fromUtf8("pbCancel"));
        sizePolicy3.setHeightForWidth(pbCancel->sizePolicy().hasHeightForWidth());
        pbCancel->setSizePolicy(sizePolicy3);

        horizontalLayout_4->addWidget(pbCancel);

        splitter->addWidget(layoutWidget3);

        gridLayout_3->addWidget(splitter, 0, 0, 1, 1);

        tabWidget->addTab(tabClassif, QString());
        tabGood = new QWidget();
        tabGood->setObjectName(QString::fromUtf8("tabGood"));
        sizePolicy1.setHeightForWidth(tabGood->sizePolicy().hasHeightForWidth());
        tabGood->setSizePolicy(sizePolicy1);
        tabWidget->addTab(tabGood, QString());
        vSplitter->addWidget(tabWidget);
        layoutWidget4 = new QWidget(vSplitter);
        layoutWidget4->setObjectName(QString::fromUtf8("layoutWidget4"));
        sizePolicy2.setHeightForWidth(layoutWidget4->sizePolicy().hasHeightForWidth());
        layoutWidget4->setSizePolicy(sizePolicy2);
        layoutWidget4->setMinimumSize(QSize(416, 418));
        verticalLayoutButton = new QVBoxLayout(layoutWidget4);
        verticalLayoutButton->setSpacing(4);
        verticalLayoutButton->setContentsMargins(4, 4, 4, 4);
        verticalLayoutButton->setObjectName(QString::fromUtf8("verticalLayoutButton"));
        verticalLayoutButton->setContentsMargins(0, 0, 0, 0);
        gridLayoutButton = new QGridLayout();
        gridLayoutButton->setSpacing(4);
        gridLayoutButton->setObjectName(QString::fromUtf8("gridLayoutButton"));
        gridLayoutButton->setSizeConstraint(QLayout::SetMaximumSize);
        pushButton_7 = new QPushButton(layoutWidget4);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        sizePolicy.setHeightForWidth(pushButton_7->sizePolicy().hasHeightForWidth());
        pushButton_7->setSizePolicy(sizePolicy);
        pushButton_7->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_7, 0, 0, 1, 1);

        pushButton_8 = new QPushButton(layoutWidget4);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        sizePolicy.setHeightForWidth(pushButton_8->sizePolicy().hasHeightForWidth());
        pushButton_8->setSizePolicy(sizePolicy);
        pushButton_8->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_8, 0, 1, 1, 1);

        pushButton_9 = new QPushButton(layoutWidget4);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        sizePolicy.setHeightForWidth(pushButton_9->sizePolicy().hasHeightForWidth());
        pushButton_9->setSizePolicy(sizePolicy);
        pushButton_9->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_9, 0, 2, 1, 1);

        pushButton_Num = new QPushButton(layoutWidget4);
        pushButton_Num->setObjectName(QString::fromUtf8("pushButton_Num"));
        sizePolicy.setHeightForWidth(pushButton_Num->sizePolicy().hasHeightForWidth());
        pushButton_Num->setSizePolicy(sizePolicy);
        pushButton_Num->setMinimumSize(QSize(89, 89));
        pushButton_Num->setCheckable(true);
        pushButton_Num->setChecked(false);

        gridLayoutButton->addWidget(pushButton_Num, 0, 3, 1, 1);

        pushButton_4 = new QPushButton(layoutWidget4);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        sizePolicy.setHeightForWidth(pushButton_4->sizePolicy().hasHeightForWidth());
        pushButton_4->setSizePolicy(sizePolicy);
        pushButton_4->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_4, 1, 0, 1, 1);

        pushButton_5 = new QPushButton(layoutWidget4);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        sizePolicy.setHeightForWidth(pushButton_5->sizePolicy().hasHeightForWidth());
        pushButton_5->setSizePolicy(sizePolicy);
        pushButton_5->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_5, 1, 1, 1, 1);

        pushButton_6 = new QPushButton(layoutWidget4);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        sizePolicy.setHeightForWidth(pushButton_6->sizePolicy().hasHeightForWidth());
        pushButton_6->setSizePolicy(sizePolicy);
        pushButton_6->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_6, 1, 2, 1, 1);

        pushButton_Art = new QPushButton(layoutWidget4);
        pushButton_Art->setObjectName(QString::fromUtf8("pushButton_Art"));
        sizePolicy.setHeightForWidth(pushButton_Art->sizePolicy().hasHeightForWidth());
        pushButton_Art->setSizePolicy(sizePolicy);
        pushButton_Art->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_Art, 1, 3, 1, 1);

        pushButton_1 = new QPushButton(layoutWidget4);
        pushButton_1->setObjectName(QString::fromUtf8("pushButton_1"));
        sizePolicy1.setHeightForWidth(pushButton_1->sizePolicy().hasHeightForWidth());
        pushButton_1->setSizePolicy(sizePolicy1);
        pushButton_1->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_1, 2, 0, 1, 1);

        pushButton_2 = new QPushButton(layoutWidget4);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        sizePolicy.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy);
        pushButton_2->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_2, 2, 1, 1, 1);

        pushButton_3 = new QPushButton(layoutWidget4);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);
        pushButton_3->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_3, 2, 2, 1, 1);

        pushButton_Quantity = new QPushButton(layoutWidget4);
        pushButton_Quantity->setObjectName(QString::fromUtf8("pushButton_Quantity"));
        sizePolicy.setHeightForWidth(pushButton_Quantity->sizePolicy().hasHeightForWidth());
        pushButton_Quantity->setSizePolicy(sizePolicy);
        pushButton_Quantity->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_Quantity, 2, 3, 1, 1);

        pushButton_Pay = new QPushButton(layoutWidget4);
        pushButton_Pay->setObjectName(QString::fromUtf8("pushButton_Pay"));
        sizePolicy.setHeightForWidth(pushButton_Pay->sizePolicy().hasHeightForWidth());
        pushButton_Pay->setSizePolicy(sizePolicy);
        pushButton_Pay->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_Pay, 3, 0, 1, 1);

        pushButton_0 = new QPushButton(layoutWidget4);
        pushButton_0->setObjectName(QString::fromUtf8("pushButton_0"));
        sizePolicy1.setHeightForWidth(pushButton_0->sizePolicy().hasHeightForWidth());
        pushButton_0->setSizePolicy(sizePolicy1);
        pushButton_0->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_0, 3, 1, 1, 1);

        pushButton_Decimal = new QPushButton(layoutWidget4);
        pushButton_Decimal->setObjectName(QString::fromUtf8("pushButton_Decimal"));
        sizePolicy.setHeightForWidth(pushButton_Decimal->sizePolicy().hasHeightForWidth());
        pushButton_Decimal->setSizePolicy(sizePolicy);
        pushButton_Decimal->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_Decimal, 3, 2, 1, 1);

        pushButton_Function = new QPushButton(layoutWidget4);
        pushButton_Function->setObjectName(QString::fromUtf8("pushButton_Function"));
        sizePolicy.setHeightForWidth(pushButton_Function->sizePolicy().hasHeightForWidth());
        pushButton_Function->setSizePolicy(sizePolicy);
        pushButton_Function->setMinimumSize(QSize(89, 89));

        gridLayoutButton->addWidget(pushButton_Function, 3, 3, 1, 1);

        gridLayoutButton->setRowStretch(0, 1);
        gridLayoutButton->setRowStretch(1, 1);
        gridLayoutButton->setRowStretch(2, 1);
        gridLayoutButton->setRowStretch(3, 1);
        gridLayoutButton->setColumnStretch(0, 1);
        gridLayoutButton->setColumnStretch(1, 1);
        gridLayoutButton->setColumnStretch(2, 1);
        gridLayoutButton->setColumnStretch(3, 1);
        gridLayoutButton->setColumnMinimumWidth(0, 95);
        gridLayoutButton->setColumnMinimumWidth(1, 95);
        gridLayoutButton->setColumnMinimumWidth(2, 95);
        gridLayoutButton->setColumnMinimumWidth(3, 95);
        gridLayoutButton->setRowMinimumHeight(0, 95);
        gridLayoutButton->setRowMinimumHeight(1, 95);
        gridLayoutButton->setRowMinimumHeight(2, 95);
        gridLayoutButton->setRowMinimumHeight(3, 95);

        verticalLayoutButton->addLayout(gridLayoutButton);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayoutButton->addItem(verticalSpacer_6);

        verticalLayoutButton->setStretch(0, 10);
        verticalLayoutButton->setStretch(1, 2);
        vSplitter->addWidget(layoutWidget4);
        hSplitter->addWidget(vSplitter);

        gridLayout_2->addWidget(hSplitter, 0, 0, 1, 1);


        retranslateUi(CashForm);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(CashForm);
    } // setupUi

    void retranslateUi(QWidget *CashForm)
    {
        CashForm->setWindowTitle(QApplication::translate("CashForm", "\320\232\320\260\321\201\321\201\320\270\321\200", 0, QApplication::UnicodeUTF8));
        label_1->setText(QApplication::translate("CashForm", "<html><head/><body><p>Position of goods</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_Product->setText(QString());
        label_3->setText(QApplication::translate("CashForm", "<html><head/><body><p>Cost</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("CashForm", "<html><head/><body><p>Quantity</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("CashForm", "<html><head/><body><p>Sum</p></body></html>", 0, QApplication::UnicodeUTF8));
        label_Cost->setText(QString());
        label_Quantity->setText(QString());
        label_Sum->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tabSale), QApplication::translate("CashForm", "Sale", 0, QApplication::UnicodeUTF8));
        pbOk->setText(QApplication::translate("CashForm", "Accept", 0, QApplication::UnicodeUTF8));
        pbCancel->setText(QApplication::translate("CashForm", "Cancel", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabClassif), QApplication::translate("CashForm", "Classif", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabGood), QApplication::translate("CashForm", "Good", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Num->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Art->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_1->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Quantity->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Pay->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_0->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Decimal->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_Function->setText(QApplication::translate("CashForm", "PushButton", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CashForm: public Ui_CashForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CASH_H
