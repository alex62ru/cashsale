#include "cgoodsmodel.h"
#include <QBrush>

#define NOSIZE "NOSIZE"
#define CURRENCY "%1 руб."

CGoodsModel::CGoodsModel(QObject* parent): QAbstractTableModel(parent)
{
}

void CGoodsModel::SetGoods(const QMap<int, CGood>& goo)
{
  goods=goo;
  reset();
}

int CGoodsModel::rowCount(const QModelIndex &parent) const
{
  return goods.size();
}

int CGoodsModel::columnCount(const QModelIndex &parent) const
{
  return columns.size();
}

QVariant CGoodsModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid()) return QVariant();
  if(role==Qt::TextAlignmentRole)//если запрос выравнивания, то возврат согласно установок столбца
    {
      return QVariant(columns[index.column()].align);
    } else
    if(role==Qt::DisplayRole)//если отображение данных
      {
	//номер позиции пойдет автоматом, иначе добавить здесь
	switch (index.column())
	  {
	  case eDescription:
	    {
	      //pos.description=(pos.description+="<em><font color=blue> %1</font></em>").arg(pos.cardsize);
	      QString description(goods[index.row()+1].description);
	      if(goods[index.row()].cardsize!=NOSIZE)
		{
		  description=(description+=" %1").arg(goods[index.row()].cardsize);
		}
	      return QVariant(description);
	    }
	  case eCost:
	    {
	      return QVariant(goods[index.row()+1].price);
	    }
	  case eQuantity:
	    {
	      return QVariant(goods[index.row()+1].quantity);
	    }
	  case eSum:
	    {
	      return QVariant(goods[index.row()+1].sum);
	    }
	  case eDiscount:
	    {
	      return QVariant(goods[index.row()+1].discount);
	    }
	  case eTotal:
	    {
	      return QVariant(goods[index.row()+1].total);
	    }
	  }
      } else
      if(role==Qt::ForegroundRole)
	{
	  if(index.column()==eDiscount && !goods[index.row()+1].discount.contains(QRegExp("^0[,.]00"))) return QBrush(QColor("blue"));
	  if(goods[index.row()+1].quantity.contains(QRegExp("^0[,.]000"))) return QBrush(QColor("red"));
	  else return QBrush(QColor("black"));
	}
  return QVariant();
}

QVariant CGoodsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role==Qt::DisplayRole)
    {
      if(orientation==Qt::Horizontal) return QVariant(columns[section].text);
      else if(orientation==Qt::Vertical) return QVariant(QString().setNum(section+1));
      //    if(orientation==Qt::Vertical) return QVariant(columns[section].text);
      //    else if(orientation==Qt::Horizontal) return QVariant(QString().setNum(section+1));
    } else
    if(role==Qt::TextAlignmentRole)
      {
	if(orientation==Qt::Horizontal) return QVariant(Qt::AlignCenter);
	else if(orientation==Qt::Vertical) return QVariant(Qt::AlignVCenter | Qt::AlignRight);
      } else
      if(role==Qt::ForegroundRole)
	{
	  if(orientation==Qt::Horizontal) return QVariant(QBrush(columns[section].back));
	}
  return QVariant();
}

Qt::ItemFlags CGoodsModel::flags(const QModelIndex &index) const
{
  return QAbstractItemModel::flags(index);
}


void CGoodsModel::UpdateGood(const CGood& plu/*, bool upd*/)
{
  if(plu.pos==0) return;//если идет нулевая позиция, тогда впросто возврат
  goods[plu.pos]=plu;
  reset();
}

void CGoodsModel::SetColumns(const QList<ItemText>& col)
{
  columns=col;
  reset();
}

void CGoodsModel::ClearAll()
{
  goods.clear();
  reset();
}







