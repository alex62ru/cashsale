#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cash.h"
#include "database.h"
#include "common.h"
#include "exceptions.h"
#include <QErrorMessage>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow), cash(NULL), db(NULL)
{
  ui->setupUi(this);
  errorMessage = new QErrorMessage(this);
  errorMessage->setModal(true);
  try
    {
      QString con_string(tr("host=localhost port=5432 dbname=cash user=cash password=cash connect_timeout=5"));
      db = new CPGDatabase(con_string, CPGDatabase::eFront);//создаем объект соединение
    } catch (CException e)//перехват исключения создания соединения
    {
      QMessageBox::critical(this, e.getTitle(), e.getErrorMessage());
      errorMessage->setWindowTitle(e.getTitle());
      errorMessage->showMessage(e.getErrorMessage());
      throw;
    }
}

MainWindow::~MainWindow()
{
  if (cash) delete cash;
  if(db) delete db;
  //delete ui;//возникала ошибка сегментации
}

void MainWindow::setCashWidget()
{
  WidgetParam wp;
  wp.db=db;
  cash = new QWidgetCash(wp, this);
  setCentralWidget(cash);
  // cash->hSplitter->setGeometry(this->geometry());
    cash->setWindowState(cash->windowState() ^ Qt::WindowFullScreen);
  //  cash->setWindowState(cash->windowState() ^ Qt::WindowMaximized);
  cash->activateWindow();
}
