#include "cash.h"
#include "exceptions.h"
#include "database.h"
#include "comdevice.h"
#include <QMessageBox>
#include "cgoodsmodel.h"
#include <QIcon>
#include <QPixmap>
#include <QErrorMessage>
#include <QKeyEvent>

#define SCANER_COUNT 2

QWidgetCash::QWidgetCash(const WidgetParam& wp, QWidget *parent): QWidget(parent), db(wp.db), current_pos(), previous_pos(), button_layout(eDigit/*по умолчанию грузяться цифры*/)
{try
    {
      setupUi(this);
      SetupTableView();
      //scaner = new CScaner("//dev//ttyS0");//создание объекта сканера, пока единственного
      SetupAction();
      SetupButtons();
      //      lineEdit->setText("0015244007036");
      ReloadCheck(false);
      UpdateState();
      SetupScaner();//здесь вызывается исключение, ПОЭТОМУ ВСЕ ЧТО ВСТАВИШЬ ПОСЛЕ ОБРАБАТЫВАТЬСЯ НЕ БУДЕТ!!!G
    } catch (CException e)
    {
      CatchHandler(e);
    }
}

QWidgetCash::~QWidgetCash()
{
  CScaner *scaner;
  foreach(scaner, scaners)//уничтожение объектов сканера
    {
      delete scaner;//возможно придеться предварительно вызывать функцию останова потока и ожидания
    }
  qDebug()<<"QWidgetCash::~QWidgetCash()";
}

//обработчик исключительных ситуаций с БД
void QWidgetCash::CatchHandler(CException e)
{
  QMessageBox::critical(this, e.getTitle(), e.getErrorMessage());
  /*QErrorMessage *msg = new QErrorMessage(this);//нужно сделать свой класс отображения ошибки 
  msg->setModal(true);
  msg->setWindowTitle(e.getTitle());
  msg->showMessage(e.getErrorMessage());
  msg->exec();
  delete msg;*/
  //реализовать журналирование ошибок
}



//получение аргумента, здесь также реализовать проверку корректности и проверку и передачу фокуса назад
void QWidgetCash::getArgument() throw() //идет возврат по ссылке - могут быть проблемы со сборкой мусора
{
  //lineEdit->selectAll();
  argument=lineEdit->text();
}

void QWidgetCash::DisplayCurrentPos()
{
  try
    {
      current_pos=db->GetCurrentPos();
      label_Product->setText(current_pos.description);
      label_Cost->setText(current_pos.price);
      label_Quantity->setText(current_pos.quantity);
      label_Sum->setText(current_pos.sum);
      modelGoods->UpdateGood(previous_pos);//обновить модель с чеком
      DisplayTotal();
      lineEdit->setFocus(Qt::OtherFocusReason);
      lineEdit->selectAll();
    } catch (CException e)
    {
      CatchHandler(e);
    }
}

void QWidgetCash::SetupTableView()
{
  modelGoods=new CGoodsModel(this);
  QList<ItemText> header;
  header<<ItemText(tr("Name"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black)
	<<ItemText(tr("Price"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black)
	<<ItemText(tr("Quantity"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black)
	<<ItemText(tr("Sum"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black)
	<<ItemText(tr("Discount"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black)
	<<ItemText(tr("Total"), Qt::AlignLeft|Qt::AlignVCenter, Qt::black);
  modelGoods->SetColumns(header);
  tvCheck->setModel(modelGoods);
  tvCheck->setAlternatingRowColors(true);
}

void QWidgetCash::ActionShortKey(eAction a)
{
  switch (a)
    {
    case eSearchArticul: SearchArticul();
      break;
    case eSearchBC: SearchBC();
      break;
    case eQuantity: SetQuantity();
      break;
    }
}


void QWidgetCash::SetupAction()
{
  connect(pushButton_0, SIGNAL(clicked()), this, SLOT(CashIn()));
  connect(pushButton_1, SIGNAL(clicked()), this, SLOT(Storno()));
  connect(pushButton_2, SIGNAL(clicked()), this, SLOT(pressDown()));
  connect(pushButton_3, SIGNAL(clicked()), this, SLOT(CashOut()));
  connect(pushButton_4, SIGNAL(clicked()), this, SLOT(pressLeft()));
  connect(pushButton_5, SIGNAL(clicked()), this, SLOT(Backspace()));
  connect(pushButton_6, SIGNAL(clicked()), this, SLOT(pressRight()));
  connect(pushButton_7, SIGNAL(clicked()), this, SLOT(Clearing()));
  connect(pushButton_8, SIGNAL(clicked()), this, SLOT(pressUp()));
  connect(pushButton_9, SIGNAL(clicked()), this, SLOT(Discount()));
  connect(pushButton_Num, SIGNAL(clicked()), this, SLOT(NumAction()));
  connect(pushButton_Art, SIGNAL(clicked()), this, SLOT(SearchArticul()));
  connect(pushButton_Quantity, SIGNAL(clicked()), this, SLOT(SetQuantity()));
  connect(pushButton_Function, SIGNAL(clicked()), this, SLOT(Function()));
  connect(pushButton_Decimal, SIGNAL(clicked()), this, SLOT(pressDecimal()));
  connect(pushButton_Pay, SIGNAL(clicked()), this, SLOT(Subtotal()));
}

void QWidgetCash::SetupScaner()
{
  CScaner *scaner;
try
  {
  for(int i=1; i<=SCANER_COUNT; i++)
    {
      //ss.name=QObject::tr("scaner0%1").arg(i);
      //QMessageBox::warning(this, tr("SetupScaner()"), ss.name);
      //db->GetScanerSettings(ss);
      //if(ss.port.isEmpty()) continue;
      scaner=new CScaner(db);//создание объекта сканера
      scaners<<scaner;
      connect(scaner, SIGNAL(Read(QString)), this, SLOT(SearchScanerBC(QString)));
    }
  } catch (CComPort::eDevice e)
  {
    qDebug()<<"Last scaner";
    //delete scaner;//ничего не делаем, просто выходим, достигли послднего устройства
  }
}

void QWidgetCash::SetupButtons()
{
  const QRegExp re("^pushButton{1}[A-Za-z]*");
  QList<QPushButton*>buttons=this->findChildren<QPushButton*>(re);//поиск детей-кнопок
  KeySettings ks;
  for(short i=0;i<buttons.size();i++)
    {
      switch(button_layout)
	{
	case eKeyAction:
	  ks.object_name=static_cast<QPushButton*>(buttons[i])->objectName()+"_eKeyAction";	 
	  break;
	case eDigit:
	  ks.object_name=static_cast<QPushButton*>(buttons[i])->objectName()+"_eDigit";
	  break;
	}
      db->GetButtonSettings(ks);
      SetupButton(static_cast<QPushButton*>(buttons[i]), ks);
    }
  //UpdateState();
}

void QWidgetCash::InvalidArgument() throw()
{
  QMessageBox::warning(this, tr("WARNING"), tr("An invalid argument"));
}

void QWidgetCash::ReloadCheck(bool force) throw (CException)
{
  db->ReloadCheck(goods, force);
  modelGoods->SetGoods(goods);
  DisplayTotal();
}


  //обработчики действий, слоты
void QWidgetCash::SearchArticul()
{
  switch (button_layout)
    {
    case eDigit:
      {
	try
	  {
	    VALID_ARGUMENT
	      db->StartTransaction();
	    previous_pos=db->AddGoodArt(argument);
	    DisplayCurrentPos();
	    UpdateState();
	    db->Commit();
	  } catch(CDBException e)
	  {
	    CatchHandler(e);
	    db->Rollback();
	  }
	break;
      }
    case eKeyAction:
      SearchBC();
      break;
    }
}

void QWidgetCash::SearchBC()
{
  try
    {
      getArgument();
      if(argument.isEmpty())
	{
	  InvalidArgument();
	  return;
	}
      db->StartTransaction();
      previous_pos=db->AddGoodBC(argument);
      //db->AddGoodBC(argument);
      DisplayCurrentPos();
      UpdateState();
      db->Commit();
    } catch (CDBException e)
    {
      CatchHandler(e);
      db->Rollback();
    }
}

void QWidgetCash::SearchScanerBC(const QString& bc)
{
  lineEdit->setText(bc);
  lineEdit->selectAll();
}

void QWidgetCash::SearchCost()
{

}

void QWidgetCash::NumAction()
{
  button_layout=pushButton_Num->isChecked()?eKeyAction:eDigit;
  SetupButtons();//перечитываем раскладку
  UpdateState();
}

void QWidgetCash::SetQuantity()
{
  switch (button_layout)
    {
    case eDigit:
      try
	{
	  VALID_ARGUMENT
	    db->StartTransaction();
	  previous_pos=db->Quantity(argument);
	  DisplayCurrentPos();
	  db->Commit();
	} catch(CDBException e)
	{
	  CatchHandler(e);
	  db->Rollback();
	}
      break;
    case eKeyAction:
      OpenCashBox();
      break;
    }
}

 
void QWidgetCash::Storno()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_1);
      break;
    case eKeyAction:
      try
	{
	  getArgument();
	  db->StartTransaction();
	  previous_pos=db->Storno(argument);//получить предыдущую позицию
	  DisplayCurrentPos();
	  previous_pos=db->GetCurrentPos(argument.toUtf8().constData());//обновить сторнированную позицию(изменить количество и подкрасить)
	  modelGoods->UpdateGood(previous_pos);
	  db->Commit();
	} catch (CDBException e)
	{
	  CatchHandler(e);
	  db->Rollback();
	}
      break;
    }
}

void QWidgetCash::Discount()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_9);
      break;
    case eKeyAction:

      break;
    }
}

void QWidgetCash::CashIn()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_0);
      break;
    case eKeyAction:
      try
	{
	  getArgument();
	  VALID_ARGUMENT
	    db->StartTransaction();
	  lcdNumber->display(db->CashIn(argument));
	  ClearWidget();
	  db->Commit();
	} catch (CDBException e)
	{
	  CatchHandler(e);
	  db->Rollback();
	    }
      break;
    }
}

void QWidgetCash::CashOut()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_3);
      break;
    case eKeyAction:
      try
	{
	  getArgument();
	  VALID_ARGUMENT
	    db->StartTransaction();
	  lcdNumber->display(db->CashOut(argument));
	  ClearWidget();
	  db->Commit();
	} catch (CDBException e)
	{
	  CatchHandler(e);
	  db->Rollback();
	    }
      break;
    }
}

void QWidgetCash::Pay()
{
  try
    {
      getArgument();
      VALID_ARGUMENT
	db->StartTransaction();
      lineEdit->setText(db->Pay(argument));
      lcdNumber->display(lineEdit->text());
      goods.clear();//очистка списка товаров
      modelGoods->SetGoods(goods);//удаление товаров в модели предстваления
      lineEdit->setFocus(Qt::OtherFocusReason);
      lineEdit->selectAll();
      UpdateState();
      db->Commit();
    } catch (CDBException e)
    {
      CatchHandler(e);
      db->Rollback();
    }
}

void QWidgetCash::Subtotal()//является обработчиком
{
  try
    {
      if(state==eSubtotal)//если был подведен итог по чеку, тогда оплата
	{
	  Pay();
	  return;
	}
      db->StartTransaction();
      previous_pos=db->Subtotal();
      lineEdit->setText(db->GetTotal());
      DisplayCurrentPos();
      UpdateState();
      db->Commit();
    } catch (CDBException e)
    {
      CatchHandler(e);
      db->Rollback();
    }
}

void QWidgetCash::Clearing()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_7);
      break;
    case eKeyAction:

      break;
    }
}

void QWidgetCash::pressUp()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_8);
      break;
    case eKeyAction:

      break;
    }
}

void QWidgetCash::pressDown()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_2);
      break;
    case eKeyAction:

      break;
    }
}

void QWidgetCash::pressRight()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_6);
      break;
    case eKeyAction:
      PostEvent(Qt::Key_Right);
      break;
    }
}

void QWidgetCash::pressLeft()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_4);
      break;
    case eKeyAction:
      PostEvent(Qt::Key_Left);
      break;
    }
}

void QWidgetCash::Backspace()
{
  switch (button_layout)
    {
    case eDigit:
      PostEvent(Qt::Key_5);
      break;
    case eKeyAction:
      PostEvent(Qt::Key_Backspace);
      break;
    }
}

void QWidgetCash::OpenCashBox()
{

}

void QWidgetCash::pressDecimal()
{

}

void QWidgetCash::Function()
{

}

void QWidgetCash::PostEvent(int key) throw()
{
  QChar ch(key);
  QKeyEvent* pe = new QKeyEvent(QEvent::KeyPress, key, Qt::NoModifier, ch); 
  QCoreApplication::postEvent(lineEdit, pe);
  lineEdit->setFocus(Qt::OtherFocusReason);
}


void QWidgetCash::DisplayTotal() throw()
{
try
  {
    lcdNumber->display(db->GetTotal());
  } catch (CDBException e)
  {
    CatchHandler(e);
    //db->Rollback();//здесь ничего не меняется
  }
}

void QWidgetCash::SetupButton(QPushButton* pb, const KeySettings& ks) throw()
{
  pb->setText(ks.face_text);
  pb->setShortcut(QKeySequence(ks.shortcut));
  QFont font; font.fromString(ks.face_font);
  pb->setFont(font);
  pb->setIcon(QIcon(ks.icon_path));
  pb->setIconSize((pb->size()-QSize(4, 4)));
  //на данный момент отображение правильное, но нет масштабирования при изменении виджета
}


void QWidgetCash::UpdateState() throw ()
{
  try
    {
      state=db->GetState();
      const QRegExp re("^pushButton{1}[A-Za-z]*");
      QList<QPushButton*>buttons=this->findChildren<QPushButton*>(re);//поиск детей-кнопок
      KeySettings ks;
      qDebug()<<"db->GetState() "<<state;
      qDebug()<<"button_layout="<<button_layout;
      switch (state)
	{
	case eCloseCheck:
	  if(button_layout==eKeyAction)
	    {
	      buttons.removeOne(pushButton_7); pushButton_7->setEnabled(false);//подбезнал
	      buttons.removeOne(pushButton_1); pushButton_1->setEnabled(false);//сторно
	      buttons.removeOne(pushButton_Pay); pushButton_Pay->setEnabled(false);//изменение цены
	      foreach(QPushButton* pb, buttons)//разблокирование оставшихся кнопок
		pb->setEnabled(true);
	    }
	  if(button_layout==eDigit)
	    {
	      qDebug()<<"(button_layout==eDigit) TRUE";
	      buttons.removeOne(pushButton_Pay); pushButton_Pay->setEnabled(false);//подитог-расчет
	      buttons.removeOne(pushButton_Quantity); pushButton_Quantity->setEnabled(false);//изменение количества
	      foreach(QPushButton *pb, buttons)
		pb->setEnabled(true);
	      ks.object_name="pushButton_Subtotal_eDigit";
	      db->GetButtonSettings(ks);
	      SetupButton(pushButton_Pay, ks);
	    }
	  break;
	case eOpenCheck:
	  if(button_layout==eKeyAction)
	    {
	      buttons.removeOne(pushButton_0); pushButton_0->setEnabled(false);//внесение
	      buttons.removeOne(pushButton_3); pushButton_3->setEnabled(false);//изъятие
	      foreach(QPushButton *pb, buttons)
		pb->setEnabled(true);
	    }
	  if(button_layout==eDigit)
	    {
	      foreach(QPushButton *pb, buttons)
		pb->setEnabled(true);
	      ks.object_name="pushButton_Subtotal_eDigit";
	      db->GetButtonSettings(ks);
	      SetupButton(pushButton_Pay, ks);
	    }
	  break;
	case eSubtotal:
	  if(button_layout==eKeyAction)
	    {
	      buttons.removeOne(pushButton_5);
	      buttons.removeOne(pushButton_Num);
	      buttons.removeOne(pushButton_Function);
	      buttons.removeOne(pushButton_Pay);
	      foreach(QPushButton* pb, buttons)//блокирование оставшихся кнопок
		pb->setEnabled(false);
	      pushButton_Pay->setEnabled(true);
	      pushButton_5->setEnabled(true);
	      pushButton_Num->setEnabled(true);
	      pushButton_Function->setEnabled(true);
	     ks.object_name="pushButton_Pay_eKeyAction";//иконка изменения цены
		db->GetButtonSettings(ks);
		SetupButton(pushButton_Pay, ks);
	    }
	  if(button_layout==eDigit)
	    {
	      ks.object_name="pushButton_Pay_eDigit";//выбрать иконку расчета
	      db->GetButtonSettings(ks);
	      SetupButton(pushButton_Pay, ks);
	      buttons.removeOne(pushButton_Art); pushButton_Art->setEnabled(false);
	      buttons.removeOne(pushButton_Quantity); pushButton_Quantity->setEnabled(false);
	      foreach(QPushButton* pb, buttons)//разблокирование оставшихся кнопок
		pb->setEnabled(true);
	    }
	  break;
	}
    } catch (CDBException e)
    {
      CatchHandler(e);
    }
}

void QWidgetCash::ClearWidget() throw()
{
  label_Product->setText(QString());
  label_Cost->setText(QString());
  label_Quantity->setText(QString());
  label_Sum->setText(QString());
  lineEdit->selectAll();
  lineEdit->setFocus(Qt::OtherFocusReason);
}
