﻿#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "fr101api.h"
#include "exceptions.h"
#include "database.h"
#include <algorithm>


#define	STX	0x02
#define	ETX	0x03
#define	ENQ	0x05
#define	ACK	0x06
#define	CAN	0x18
#define	FS	0x1C
#define OPER_NAME 20

//#define STR_FS "\x1C"


int CFR101_API::number(1);

CFR101_API::CFR101_API(CDatabase *db): CComPort(db), bFR_Packet(0x20), bWaitEOT(3), bWaitSTX(1), bWaitACK(0), bCommand(ENQ), argument(NULL)
{
  GetSettings();
  if(cp.path.isEmpty()) throw eLast;//если пустая строка в имени порта - исключение
  fd_port=open(cp.path.toLocal8Bit().constData(), cp.mode);
  if(fd_port==-1) throw CPortException();//в конструкторе исключения реализовать проверку errno
  if(fcntl(fd_port, F_SETFL, FNDELAY)==-1) throw CPortException();
  r_length = FR_SIZEOF_BUFFER;
  w_length = FR_SIZEOF_BUFFER;
  argument = new char[FR_SIZEOF_BUFFER];
  SetTermios();
  start();//запуск асинхронного ожидания завершения операции
  number++;
}

CFR101_API::~CFR101_API()
{
  terminate();
  wait();
  if(argument) delete[] argument;
  //if(read_bufer) delete[] read_bufer;
  //if(write_buffer) delete[] write_buffer;
}

void CFR101_API::SetTermios() throw (CPortException)
{
  tcgetattr(fd_port, &options);
  cfsetispeed(&options, B19200);
  cfsetospeed(&options, B19200);
  options.c_cflag &= CRTSCTS;//включение аппаратного контроля
  options.c_iflag &= ~(IXON | IXOFF | IXANY);//отключение программного контроля
  //options.c_lflag |= (ICANON | ECHO | ECHOE);//канонический ввод
  options.c_lflag |= ~(ICANON | ECHO | ECHOE | ISIG);//неканонический ввод
  options.c_cflag &= ~CSIZE; /* Маскирование битов размера символов */
  options.c_cflag |= CS8;    /* Установка 8 битов данных */
  options.c_cflag &= ~PARENB;//отсутствие проверки на четность
  options.c_cflag &= ~CSTOPB;
  if(tcsetattr(fd_port, TCSANOW, &options)==-1) throw CPortException();//запись структуры termios
}

void CFR101_API::InitFDSET()
{
  FD_ZERO(r_set);
  FD_ZERO(w_set);
  FD_SET(fd_port, r_set);
}

void CFR101_API::GetSettings() throw (CDBException)
{
  cp.number = number;//присвоение номера устройства
  char szTemp[]="printer%dd";  
  snprintf(szTemp, sizeof("printer%dd"), "printer%02d", cp.number);
  qDebug("GetSettings() %s", szTemp);
  cp.name = szTemp;//вид устройств
  db->GetPortSettings(cp);
  cp.mode = O_RDONLY | O_NOCTTY | O_NDELAY;
  timeout = &cp.timeout;//производим присвоение структуры таймаутов ожидания - для фр и принтера это важно
}


BYTE CFR101_API::dbFR_GetLRC(BUFFER& b, eLRC lrc)
/* Calculate LRC for data */
{
  BYTE dbLRC = 0;
  for(BUFFER::iterator iter = b.begin()+1; iter < (lrc?b.end()-2:b.end()); iter++)
  {
    dbLRC ^= (*iter);
  }
  return dbLRC;
}

bool CFR101_API::nFR_CheckLRC(BUFFER& b)
{
  char szTemp[] = {0, 0};
  sscanf(szTemp, "%02X", dbFR_GetLRC(b, eCheck));
  if(szTemp[0]==b[b.size()-2] && szTemp[1]==b[b.size()-1]) return true;
  return false;
}

int CFR101_API::nFR_BuildPacket()
{
  /*
    на момент вызова функции аргументы запроса должны быть подготовленны, паравозом вставляеся стартовый байт и пароль PONE, затем в конец вставляется ETX и передается на формирование контрольной суммы
  */
  char start[] = {STX, 'P', 'O', 'N', 'E', 0, 0, 0};//формируем префикс запроса STX(1) Пароль(4), ID(1), Команда(2)
  if(bFR_Packet == 0xFD)
    {
      bFR_Packet = 0x20;
    }
  else
    {
      bFR_Packet ++;
    }
  *(start+5)=bFR_Packet;
  char szTemp[] = {0, 0};
  sscanf(szTemp, "%02X", bFR_Command);
  memcpy(start+6, szTemp, 2);
  vFR_Command.insert(vFR_Command.begin(), start, start+7);
  vFR_Command.resize(vFR_Command.size()+sizeof(argument)-1, 0);//вычитаем 1 из длины, чтобы убрать завершающий /0
  std::copy(argument, argument+sizeof(argument)-1, vFR_Command.end());//копирование строковой команды в вектор
  vFR_Command.push_back(ETX);
  sscanf(szTemp, "%02X", dbFR_GetLRC(vFR_Command, eCalc)); 
  vFR_Command.push_back(*szTemp);
  vFR_Command.push_back(*(szTemp+1));
}

int CFR101_API::nFR_SendPacket()
{
  int nRet = SUCCESS;
  flush();//на данный момент производиться очистка буфера передачи и приема
  write(vFR_Command.data(), vFR_Command.size());
  //осуществили запись, усекаем вектор
  vFR_Command.clear();
  return nRet;
}

CComPort::eDevice CFR101_API::runon() throw ()
{
  /*
    1)отправляем команду выполнения, выставляем признак что будем ждать(ответ на команду
    2)уходим на ожидание данных
    а)отвалили по таймауту(разлочиваем мьютекс, сигнал таймаута)
    б)данные есть
    *runon проверяем что пришло
    **признак занято ACK, вновь команда запроса статуса ENQ, сигнал занятости устройства, продолжаем ждать ИЛИ ВНОВЬ ПОДТВЕРЖДЕНИЕ ЗАНЯТОСТИ
    ***первый байт ответ
  */
  //попали сюда, значит в буфере что-то появилось
  //проверяем признак занятости
  if(r_length==1 && *read_bufer==ACK) return eBusy;//если принят один байт И он ACK - регистратор занят и на связи
  if(r_length>1)//если принят более одного байта - добавляем их в конец вектора буфера чтения
    {
      std::vector<char>::iterator it=vFR_Answer.end();
      vFR_Answer.resize(vFR_Answer.size()+r_length);
      std::copy(read_bufer, read_bufer+r_length, it);//здесь нужно проверить, возможно будет правильно после получения порции ответа очищать приемный буфер
      BUFFER::iterator start;
      if(vFR_Answer.size()>3) start=vFR_Answer.end()-3;
      else start=vFR_Answer.begin();
      BUFFER::iterator pos=std::find(start, vFR_Answer.end(), ETX);//ищем ETX среди последних трех элементов
      if(pos!=start) return eContinue;//если ETX не первый элемент среди трех крайних, тогда продолжаем прием
      //сюда попали, если получен полностью ответ от регитсратора
      if(!nFR_CheckLRC(vFR_Answer)) return eProtocolError;//если КС не совпадают - возврат признака ошибка протокола
      return eReadComplite;
    }
}

//функции общего назначения
void CFR101_API::fr_initialize(time_t tt)
{
  if(!tt) time(&tt);
  struct tm* t=localtime(&tt);
  size_t length(13);
  memset(argument, 0, FR_SIZEOF_BUFFER);
  bFR_Command = FR_INITIALIZE;
  if(snprintf(argument, length, "%02d%02d%02d%c%02d%02d%02d", t->tm_mday, t->tm_mon, t->tm_year%100, FS, t->tm_hour, t->tm_min, t->tm_sec) > 0)
    {
      nFR_BuildPacket();
      nFR_SendPacket();
    }
}

void CFR101_API::nFR_PrintXReport(const char* oper)
{
  memset(argument, 0, FR_SIZEOF_BUFFER);
  bFR_Command = FR_PRINT_X_REP;
  memcpy(argument, oper, sizeof(oper)>OPER_NAME?OPER_NAME:sizeof(oper));
  nFR_BuildPacket();
  nFR_SendPacket();
}
