start transaction;
delete from cashsettings;
commit;
start transaction;
--������ ������
insert into cashsettings values ('classif_ins', 'insert into classif values($1::smallint, $2::smallint, $3::smallint, $4::smallint, $5::smallint, $6::varchar)', 0, 0, 0, now(), '������ ����������� ������������� �������');
--insert into cashsettings values ('classif_ins', 'insert into classif values($1, $2, $3, $4, $5, $6)', 0, 0, 0, now(), '������ ����������� ������������� �������');
insert into cashsettings values ('classif_upd', 'update classif set description=$6::varchar where (groop1=$1::smallint) and (groop2=$2::smallint) and (groop3=$3::smallint) and (groop4=$4::smallint) and (groop5=$5::smallint)', 0, 0, 0,
	now(), '���������� ����������� ������������� �������');
insert into cashsettings values ('depart_ins', 'insert into depart values($1::smallint, $2::varchar)', 0, 0, 0, now(), '������ ����������� ������');
insert into cashsettings values ('depart_upd', 'update depart set description=$2::varchar where department = $1::smallint', 0, 0, 0, now(), '���������� ����������� ������');
-- insert into cashsettings values ('plucash_ins', 'insert into plucash values($1::varchar, $2::varchar, $3::varchar, $4::numeric, $5::varchar, $6::varchar, $7::varchar, $8::numeric, $9::numeric, $10::numeric,
-- 					$11::varchar, $12::smallint,  $13::smallint, $14::smallint, $15::smallint, $16::smallint, $17::smallint, $18::smallint)', 0, 0, 0, now(), '������ ����������� ������');
insert into cashsettings values ('plucash_ins', 'insert into plucash values($1::varchar, $2::varchar, $3::varchar, $4::double precision, $5::varchar, $6::varchar, $7::varchar, $8::double precision, $9::double precision, $10::double precision,
					$11::varchar, $12::smallint,  $13::smallint, $14::smallint, $15::smallint, $16::smallint, $17::smallint, $18::smallint)', 0, 0, 0, now(), '������ ����������� ������');
-- insert into cashsettings values ('plucash_ins', 'insert into plucash values($1::varchar, $2::varchar, $3::varchar, $4::double precision, $5::varchar, $6::varchar, $7::varchar, $8::double precision, $9::double precision, $10::double precision)', 0, 0, 0, now(), '������ ����������� ������');
insert into cashsettings values ('plucash_upd', 'update plucash set description = $2::varchar,
									mesuriment = $3::varchar,
									mespresision = $4::numeric,
									add1 = $5::varchar,
									add2 = $6::varchar,
									add3 = $7::varchar,
									addnum1 = $8::numeric,
									addnum2 = $9::numeric,
									addnum3 = $10::numeric,
									scale = $11::varchar,
									groop1 = $12::smallint,
									groop2 = $13::smallint,
									groop3 = $14::smallint,
									groop4 = $15::smallint,
									groop5 = $16::smallint,
									department = $17::smallint,
									deleted = $18::smallint where articul = $1::varchar', 0, 0, 0, now(), '���������� ����������� ������');
insert into cashsettings values ('scales_ins', 'insert into scales values($1::varchar, $2::varchar)', 0, 0, 0, now(), '������ ����������� ����� ��������');
insert into cashsettings values ('scales_upd', 'update scales set description=$2::varchar where scale = $1::varchar', 0, 0, 0, now(), '���������� ����������� ����� ��������');	
insert into cashsettings values ('costs_ins', 'insert into costs values($1::varchar, $2::double precision, $3::smallint, now())', 0, 0, 0, now(), '���������� ����������� ����');
insert into cashsettings values ('pricekin_ins', 'insert into pricekin values($1::smallint, $2::varchar)', 0, 0, 0, now(), '������ ����������� ����������');
insert into cashsettings values ('pricekin_upd', 'update pricekin set description=$2::varchar where pricelist = $1::smallint', 0, 0, 0, now(), '���������� ����������� ����������');
insert into cashsettings values ('bar_ins', 'insert into bar values($1::varchar, $2::varchar, $3::varchar, $4::numeric)', 0, 0, 0, now(), '������ ����������� ����������');
insert into cashsettings values ('bar_upd', 'update bar set cardarticul=$2::varchar, cardsize=$3::varchar, quantity=$4::numeric where barcode = $1::varchar', 0, 0, 0, now(), '���������� ����������� ����������');
insert into cashsettings values ('sizes_ins', 'insert into sizes values($1::varchar, $2::varchar)', 0, 0, 0, now(), '������ ����������� ��������');
insert into cashsettings values ('personal_ins', 'insert into personal values($1::smallint, $2::varchar, $3::varchar, $4::smallint)', 0, 0, 0, now(), '������ ����������� ��������');
insert into cashsettings values ('personal_upd', 'update personal set description=$2::varchar, passw=$3::varchar, accesslevel=$4::smallint where ident=$1::smallint', 0, 0, 0, now(), '���������� ����������� ��������');
insert into cashsettings values ('tax_ins', 'insert into tax values($1::varchar, $2::smallint, $3::numeric, $4::numeric)', 0, 0, 0, now(), '������ ����������� ������');
insert into cashsettings values ('tax_upd', 'update tax set taxvalue=$3::numeric, taxsum=$4::numeric where (articul=$1::varchar) and (taxindex=$2::smallint)', 0, 0, 0, now(), '���������� ����������� ������');
insert into cashsettings values ('cliclass_ins', 'insert into cliclass values($1::smallint, $2::smallint, $3::smallint, $4::smallint, $5::smallint, $6::varchar, $7::double precision, $8::smallint)', 0, 0, 0, now(), '������ ����������� ������������ ������ �� ��������������');
insert into cashsettings values ('cliclass_upd', 'update cliclass set percent=$7::double precision where (groop1 = $1::smallint) and (groop2 = $2::smallint) and (groop3 = $3::smallint) and (groop4 = $4::smallint) and (groop5 = $5::smallint) and (barcode = $6::varchar) and (pricelist = $8::smallint)', 0, 0, 0, now(), '���������� ����������� ������������ ������ �� ��������������');
insert into cashsettings values ('disccli_ins', 'insert into disccli values($1::varchar, $2::varchar, $3::numeric, $4::smallint, $5::smallint)', 0, 0, 0, now(), '������ ����������� ���������� ����');
insert into cashsettings values ('disccli_upd', 'update disccli set description=$2::varchar, percent=$3::numeric, clientindex=$5::smallint where (barcode=$1::varchar) and (pricelist=$4::smallint)', 0, 0, 0, now(), '���������� ����������� ���������� ����');
insert into cashsettings values ('credcard_ins', 'insert into credcard values($1::smallint, $2::varchar, $3::numeric, $4::smallint, $5::varchar, $6::varchar)', 0, 0, 0, now(), '������ ����������� ��������� ����');
insert into cashsettings values ('credcard_upd', 'update credcard set description=$2::varchar, limitsum=$3::numeric, canreturn=$4::smallint, soft=$5::varchar, ident=$6::varchar where (credcardindex=$1::smallint)', 0, 0, 0, now(), '���������� ����������� ��������� ����');
insert into cashsettings values ('dclislst_ins', 'insert into dclislst values($1::varchar, $2::varchar)', 0, 0, 0, now(), '������ ����������� ����-���� ������������ ������');
--���������, ����� ������������� ������ �� ������� ������� ������
insert into cashsettings values ('sd_cashsettings', 'select * from cashsettings where var_name=$1::varchar', 0, 0, 0, now(), '������ �� ��������� ���������� ���������');
--��������� sd- ���������������� � ����� PostgreSQL
insert into cashsettings values ('sd_cashsale', 'select * from v_cashsale', 0, 0, 0, now(), '�� ����������� ����������� �����');
insert into cashsettings values ('sd_cashdisc', 'select * from v_cashdisc', 0, 0, 0, now(), '�� ����������� ����������� ������');
insert into cashsettings values ('sd_cashpay', 'select * from v_cashpay', 0, 0, 0, now(), '�� ����������� ����������� �������');
insert into cashsettings values ('sd_currests', 'select * from v_currests', 0, 0, 0, now(), '�� ����������� ����� �����');
insert into cashsettings values ('sd_curmoney', 'select * from v_curmoney', 0, 0, 0, now(), '�� ����������� �������� ����������');
insert into cashsettings values ('sd_cashgood', 'select *, sum_total as totalcur from v_cashgood', 0, 0, 0, now(), '�� ����������� �������� �����');
insert into cashsettings values ('uq_cashsale', 'insert into cashsail values ', 0, 0, 0, now(), '������ ������� ������ ����������� �����');
insert into cashsettings values ('uq_cashdisc', 'insert into cashdisc values ', 0, 0, 0, now(), '������ ������� ����������� ������');
insert into cashsettings values ('uq_cashpay', 'insert into cashpay values ', 0, 0, 0, now(), '������ ������� ����������� ��������');
insert into cashsettings values ('uq_currests', 'insert into currests values ', 0, 0, 0, now(), '������ ������� ������� �������� ����');
insert into cashsettings values ('uq_cashgood', 'insert into cashgood values ', 0, 0, 0, now(), '������ ������� ������ ��������� ������');
insert into cashsettings values ('uq_curmoney', 'insert into curmoney values ', 0, 0, 0, now(), '������ ������� ������ �������� ����������');
insert into cashsettings values ('uq_outdb', 'out_db', 0, 0, 0, now(), 'ODBC ��������� ���� ������');
insert into cashsettings values ('uq_locale', 'KOI8-R', 0, 0, 0, now(), 'ODBC ��������� ���� ������');

-- struct LoadSettings //���������
-- {
--     //bool single_cost; //������ ������ � �������
--     QString flg_upd;//���� ����������
--     QString flg_ins;//���� ������
--     QString load_dir;//������� ��������
--     unsigned int timer;//������ ������ �������� ��������
-- };
insert into cashsettings values ('flg_upd', 'cash041.db', 0, 0, 0, now(), '���� ����������');
insert into cashsettings values ('flg_ins', 'cash041.db', 0, 0, 0, now(), '���� ������');
insert into cashsettings values ('flg_lck', 'flg.lck', 0, 0, 0, now(), '���� ����������');
-- insert into cashsettings values ('load_dir', 'c:\\Users\\alex\\Documents\\Datasour\\', 0, 0, 0, now(), '������� ������');--����������� ��� ����� work
insert into cashsettings values ('load_dir', 'e:\\Virtual Machines\\exchange\\Datasour\\', 0, 0, 0, now(), '������� ������');--����������� ��� ����� home
insert into cashsettings values ('timer', '', 60, 0, 0, now(), '���� ������');

-- insert into pricekin values(1, '��������� ���������');--���������������� ��� ������ ��������
-- insert into credcard values(0, '��������', 0, 1);
--������ ������

insert into cashsettings values('current_id', 'NOVALUE', 3, 0, 0, now(), '����� �������, ������������ �������');
insert into cashsettings values('check_open', 'NOVALUE', 0, 0, 0, now(), '������� ��������� ����');
insert into cashsettings values('z_open', 'NOVALUE', 1, 0, 0, now(), '������� �������� �����');
insert into cashsettings values('currency', '���.', 0, 0, 0, now(), '�������� �������� ������');
insert into cashsettings values('reload_check', 'NOVALUE', 1, 0, 0, now(), '������� ���������� ����������� ���');
insert into cashsettings values('value_sale', 'NOVALUE', 1, 0, 0, now(), '������� ���� �������');
insert into cashsettings values('value_return', 'NOVALUE', 0, 0, 0, now(), '������� ���� ��������');
insert into cashsettings values('value_pay_cash', 'NOVALUE', 0, 0, 0, now(), '������� ������� �� ��������');
insert into cashsettings values('value_pay_card', 'NOVALUE', 1, 0, 0, now(), '������� ������� �� �����');
insert into cashsettings values('value_ret_cash', 'NOVALUE', 2, 0, 0, now(), '������� �������� �� ��������');
insert into cashsettings values('value_ret_card', 'NOVALUE', 3, 0, 0, now(), '������� �������� �� �����');
insert into cashsettings values('value_disc_limit', 'NOVALUE', 1, 0, 0, now(), '������� ����������� ������');
insert into cashsettings values('value_disc_good', 'NOVALUE', 2, 0, 0, now(), '������� ������ �� ����� ��� ������ �������');
insert into cashsettings values('value_disc_clients', 'NOVALUE', 3, 0, 0, now(), '������� ������ �� ����� �������');
insert into cashsettings values('value_disc_sum_time', 'NOVALUE', 4, 0, 0, now(), '������� ������ �� ����� � �������');
insert into cashsettings values('value_disc_quantity', 'NOVALUE', 5, 0, 0, now(), '������� ������ �� ���������� ������');
insert into cashsettings values('value_storno', 'NOVALUE', 7, 0, 0, now(), '������� �������������� �������');
insert into cashsettings values('use_plulim', 'NOVALUE', 1, 0, 0, now(), '������������ ����������� ������ �� �����');
insert into cashsettings values('weight_prefix', '21', 1, 0, 0, now(), '������� �������� ������');
insert into cashsettings values('use_dclislst', 'NOVALUE', 1, 0, 0, now(), '������������ ������������ ������ �� ������ �������');
insert into cashsettings values('use_clients', 'NOVALUE', 1, 0, 0, now(), '������������ ������� ��������');
insert into cashsettings values('use_claslim', 'NOVALUE', 1, 0, 0, now(), '������������ ����������� ������ �� ������');
insert into cashsettings values('use_disccard', 'NOVALUE', 1, 0, 0, now(), '������������ �������������� ���� � ������ �� ����������');
insert into cashsettings values('use_disccli', 'NOVALUE', 1, 0, 0, now(), '������������ ������������ ������ �� �����');
insert into cashsettings values('use_cliclass', 'NOVALUE', 1, 0, 0, now(), '������������ ������������ ������ �� ������ �������');
insert into cashsettings values('use_clasdisc', 'NOVALUE', 1, 0, 0, now(), '������������ ������ �� ������ �������');
insert into cashsettings values('use_pludisc', 'NOVALUE', 1, 0, 0, now(), '������������ ������������ ������ �� �����');
insert into cashsettings values('linked', 'NOVALUE', 0, 0, 0, now(), '����� ���������� ����');
insert into cashsettings values('reason_inserting', 'NOVALUE', 1, 0, 0, now(), '������� ���������� �������� �������');--��������� ������ ��������� � ���� ����������
insert into cashsettings values('reason_subtotal', 'NOVALUE', 2, 0, 0, now(), '������� reason_subtotal');
insert into cashsettings values('reason_close', 'NOVALUE', 0, 0, 0, now(), '������� reason_close');
insert into cashsettings values('replace', 'NOVALUE', 0, 0, 0, now(), '����������� ��������');
insert into cashsettings values('casher', 'NOVALUE', 8, 0, 0, now(), 'ID �������� �������');
insert into cashsettings values('numeric_round', 'NOVALUE', 2, 0, 0, now(), '������� ���������� �������� �������');
insert into cashsettings values('shop_index', 'NOVALUE', 1, 0, 0, now(), '����� ��������');
insert into cashsettings values('cash_number', 'NOVALUE', 1, 0, 0, now(), '����� �����');
insert into cashsettings values('znumber', 'NOVALUE', 0, 0, 0, now(), '������� ����� �����');
insert into cashsettings values('check_number', 'NOVALUE', 1, 0, 0, now(), '������� ����� ����');
insert into cashsettings values('current_pricelist', 'NOVALUE', 1, 0, 0, now(), '��������� ��� ����');
insert into cashsettings values('disccli_bc', '', 0, 0, 0, now(), '������������� ������������ ������');
insert into cashsettings values('starting_check', 'NOVALUE', 0, 0, 0, now(), '����� ������ ����');
--���� �������
insert into cashsettings values('ss_opencheck', 'select open_check($1::int)', 0, 0, 0, now(), '������� ���');--���������� ������ ������ ��������
insert into cashsettings values('ss_addart', 'select add_art($1::varchar)', 0, 0, 0, now(), '������� �� ��������');
insert into cashsettings values('ss_addbc', 'select add_bc($1::varchar)', 0, 0, 0, now(), '������� �� ��');
insert into cashsettings values('ss_pos', 'select * from check_pos where id=$1::int', 0, 0, 0, now(), '������� �������� �������');
insert into cashsettings values('ss_check', 'select * from check_pos', 0, 0, 0, now(), '������� ���� ���');
insert into cashsettings values('ss_buttons', 'select face_text, shortcut, description, face_font, icon_path from buttons where (object_name = $1::varchar)', 0, 0, 0, now(), '������ �� ���������� ������ � �������');
insert into cashsettings values('ss_reloadcheck', 'select reload_check()', 0, 0, 0, now(), '������ �������� ������������� ����������� ���');
insert into cashsettings values('ss_quantity', 'select set_quantity($1::varchar)', 0, 0, 0, now(), '��������� ����������');
insert into cashsettings values('ss_storno', 'select storno($1::varchar)', 0, 0, 0, now(), '������������� �������');
insert into cashsettings values('ss_total', 'select get_total()', 0, 0, 0, now(), '����� ����');
insert into cashsettings values('ss_subtotal', 'select subtotal()', 0, 0, 0, now(), '������� ����');
insert into cashsettings values('ss_closecheck', 'select close_cash($1::varchar)', 0, 0, 0, now(), '�������� ����, ������');
insert into cashsettings values('ss_state', 'select get_checkopen()', 0, 0, 0, now(), '��������� ����(������, ������, �������� ����)');
insert into cashsettings values('ss_cashin', 'select cash_in($1::numeric)', 0, 0, 0, now(), '�������� �����');
insert into cashsettings values('ss_cashout', 'select cash_out($1::numeric)', 0, 0, 0, now(), '������� �����');
--������ �������� ������
delete from buttons;
insert into buttons values('pushButton_0_eDigit', '', '0', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/0.png');
insert into buttons values('pushButton_1_eDigit', '', '1', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/1.png');
insert into buttons values('pushButton_2_eDigit', '', '2', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/2.png');
insert into buttons values('pushButton_3_eDigit', '', '3', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/3.png');
insert into buttons values('pushButton_4_eDigit', '', '4', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/4.png');
insert into buttons values('pushButton_5_eDigit', '', '5', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/5.png');
insert into buttons values('pushButton_6_eDigit', '', '6', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/6.png');
insert into buttons values('pushButton_7_eDigit', '', '7', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/7.png');
insert into buttons values('pushButton_8_eDigit', '', '8', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/8.png');
insert into buttons values('pushButton_9_eDigit', '', '9', '', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/9.png');  
insert into buttons values('pushButton_Subtotal_eDigit', '', 'F5', '�������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/purchases_refresh_256.png');
insert into buttons values('pushButton_Pay_eDigit', '', 'F5', '������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/purchases.png'); 
insert into buttons values('pushButton_Decimal_eDigit', '.', '', '���������� �����', 'MS Shell Dlg 2,50,-1,5,50,0,0,0,0,0', ''); 
insert into buttons values('pushButton_Function_eDigit', '', 'F9', '�������������� �������, ����� ������� �� ����������� ������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/gear_reload_256.png'); 
insert into buttons values('pushButton_Quantity_eDigit', '', 'F4', '�������� ����������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/chocolate_add_256.png'); 
insert into buttons values('pushButton_Art_eDigit', '', 'CTRL+A', '����� �� ��������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/a.png'); 
insert into buttons values('pushButton_Num_eDigit', '', 'F12', '������������ ���������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/ellipse_refresh_256.png'); 
insert into buttons values('pushButton_0_eKeyAction', '', 'CTRL+F2', '��������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/cash_in.png');
insert into buttons values('pushButton_1_eKeyAction', '', 'CTRL+F3', '�������������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/storno1.png');
insert into buttons values('pushButton_2_eKeyAction', '', 'CTRL+F4', '����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/Fleche bas bleue.png');
insert into buttons values('pushButton_3_eKeyAction', '', 'CTRL+F5', '�������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/cash_out.png');
insert into buttons values('pushButton_4_eKeyAction', '', 'CTRL+F6', '�����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/Fleche gauche bleue.png');
insert into buttons values('pushButton_5_eKeyAction', '', 'CTRL+F7', '�����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/clear_log_256.png');
insert into buttons values('pushButton_6_eKeyAction', '', 'CTRL+F8', '������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/Fleche droite bleue.png');
insert into buttons values('pushButton_7_eKeyAction', '', 'CTRL+F9', '����������� ������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/cashless3.png');
-- insert into buttons values('pushButton_7_eKeyAction', '', 'CTRL+F9', '����������� ������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/admin.png');--���������� ��� �������� ��������
insert into buttons values('pushButton_8_eKeyAction', '', 'CTRL+F10', '�����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/Fleche haut bleue.png');
insert into buttons values('pushButton_9_eKeyAction', '', 'CTRL+F11', '������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/discount2.png');  
insert into buttons values('pushButton_Pay_eKeyAction', '', 'F6', '��������� ����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/sign_euro_reload_256.png'); 
insert into buttons values('pushButton_Decimal_eKeyAction', '', 'F7', '���������� �����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/search_cost2.png'); 
insert into buttons values('pushButton_Function_eKeyAction', '', 'F9', '�������������� �������, ����� ������� �� ����������� ������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/gear_reload_256.png'); 
insert into buttons values('pushButton_Quantity_eKeyAction', '', 'CTRL+F12', '������� �������� ����', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/cashbox_ok_256.png'); 
insert into buttons values('pushButton_Art_eKeyAction', '', 'CTRL+B', '����� �� ��', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/barcode_add.png'); 
insert into buttons values('pushButton_Num_eKeyAction', '', 'F12', '������������ ���������', 'MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0', ':/rc/ellipse_refresh_256.png'); 
--������ �������� ������������
--�������
insert into cashsettings values('ss_port', 'select strvalue, intvalue from cashsettings where (var_name like $1::varchar) order by var_name', 0, 0, 0, now(), '������ �� ���������� ������ � ��������'); 
insert into cashsettings values('scaner01', '/dev/ttyS1', 10000, 0, 0, now(), '���� ������� �������');--����� �� ����� ��� �������� �����
insert into cashsettings values('scaner02', '', 0, 0, 0, now(), '���� ������� �������');
--��� � ��������

commit;
--������ ����������� ��������-- 
select strvalue, intvalue from cashsettings where (var_name like 'scaner%') order by var_name
-- select initialize_db();
-- select * from cashsettings where (var_name like 'ss_%');
-- select face_text, shortcut, description, face_font, icon_path from buttons where (object_name = 'pushButton_7_eKeyAction');
-- select open_day();
-- select add_bc('00061104410430');
-- select add_art('0070749');
-- select FindPos0();
-- --select subtotal();
-- -- select get_discclibc();
-- -- delete from cashsettings where var_name like '%close_check%';
-- -- select get_total();
-- select close_cash('1000'); 
-- select * from check_pos;
-- select get_checkopen();
-- select sum(cs.totalrub - (select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and 
-- 	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber))) from cashsail cs where (cs.shopindex = get_shopindex()) and (cs.cashnumber = get_cashnumber()) and (cs.znumber = get_znumber()) and (cs.linked = 0);

-- select cash_out(1000);
-- select cash_money();
-- select cs.shopindex, cs.cashnumber, cs.znumber, cs.cardarticul, cs.cardsize, sum(quantity), sum(cs.totalrub-(select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and
-- 	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber) and (cs.checknumber = cd.checknumber) and (cs.id = cd.id))) from cashsail cs where (cs.shopindex = get_shopindex()) and
-- 	(cs.cashnumber = get_cashnumber()) and (cs.znumber = get_znumber()) and (cs.linked = 0) group by cs.shopindex, cs.cashnumber, cs.znumber, cs.cardarticul, cs.cardsize 
-- 	order by cs.shopindex, cs.cashnumber, cs.znumber, cs.cardarticul, cs.cardsize;

-- select count(1), sum(totalrub) from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber())
-- 	and (operation in (get_reason_inserting(), get_reason_subtotal()));
select close_z();
-- commit;
-- select * from v_cashsale;
select * from cashsettings where var_name='uq_outdb'

select *, sum_total as totalcur  from v_cashgood
select cp.shopindex, cp.cashnumber, cp.znumber, cp.checknumber, cp.credcardindex as payment, cp.cardnumb, cp.payedrub as payedmoney, cp.discountrub as discountmoney, cp.payedrub,
	cp.payedrub as payedcur, cp.discountrub, cp.discountrub as discountcur, cp.discclirub, cp.discclirub as disc�li�ur from cashpay cp, currests cr
	where (cp.shopindex = cr.shopindex) and (cp.cashnumber = cr.cashnumber) and (cp.znumber = cr.znumber) and (cr.wasoutput = 0);

--���������� ��������������
-- select p.articul, c.price from plucash p, costs c where (p.articul = c.articul) and (c.modified = (select max(c1.modified) from
-- 	costs c1 where c.articul = c1.articul))

(select vp.groop1, vp.groop2, vp.groop3, vp.groop4, vp.groop5, vp.articul, vp.description, vp.price from v_plucash vp
union all
select c.groop1, c.groop2, c.groop3, c.groop4, c.groop5, '' as c_art, c.description, 0 as c_pr  from classif c)
order by groop1, groop2, groop3, groop4, groop5

-- rollback; 
