﻿-- CREATE ROLE cash WITH PASSWORD '5110'
--   NOSUPERUSER INHERIT CREATEDB NOCREATEROLE NOREPLICATION;
-- 
-- 
-- DROP DATABASE cash;
-- 
-- CREATE DATABASE cash
--   WITH OWNER = cash
--        ENCODING = 'UTF8'
--        --TABLESPACE = pg_default
--        LC_COLLATE = 'ru_RU.UTF-8'
--        LC_CTYPE = 'ru_RU.UTF-8'
--        CONNECTION LIMIT = -1;
-- 
-- DROP TABLE CASHCMNT CASCADE;
-- DROP TABLE CASHDCRD CASCADE;
-- DROP TABLE CASHDISC CASCADE;
-- DROP TABLE CASHPAY CASCADE;
-- DROP TABLE CASHSAIL CASCADE;
-- DROP TABLE CLASDISC CASCADE;
-- DROP TABLE CLASLIM CASCADE;
-- DROP TABLE CLASSIF CASCADE;
-- DROP TABLE CLICLASS CASCADE;
-- DROP TABLE CLIENTS CASCADE;
-- DROP TABLE COSTS CASCADE;
-- DROP TABLE CREDCARD CASCADE;
-- DROP TABLE CREDPREF CASCADE;
-- DROP TABLE CURMONEY CASCADE;
-- DROP TABLE CURRESTS CASCADE;
-- DROP TABLE CashSettings CASCADE;
-- DROP TABLE DCLISLST CASCADE;
-- DROP TABLE DEPART CASCADE;
-- DROP TABLE DISCCARD CASCADE;
-- DROP TABLE DISCCLI CASCADE;
-- DROP TABLE DISCSUM CASCADE;
-- DROP TABLE ERROR_TAB CASCADE;
-- DROP TABLE Keyboard CASCADE;
-- DROP TABLE PERSONAL CASCADE;
-- DROP TABLE PLUCASH CASCADE;
-- DROP TABLE PLUDISC CASCADE;
-- DROP TABLE PLULIM CASCADE;
-- DROP TABLE PRICEKIN CASCADE;
-- DROP TABLE SCALES CASCADE;
-- DROP TABLE SIZES CASCADE;
-- DROP TABLE Tax CASCADE;
-- drop table taxes cascade;
-- DROP TABLE bar CASCADE;
-- DROP TABLE cashini CASCADE;
-- drop table checkformat cascade;
-- DROP TABLE cashaut cascade;
-- --drop table session_var_tbl cascade;
-- drop table cashsettings cascade;
-- drop table buttons cascade;




-- Table: scales

DROP TABLE scales;

CREATE TABLE scales
(
  scale character varying(20) NOT NULL,
  description character varying(80),
  CONSTRAINT scales_pk PRIMARY KEY (scale)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE scales
  OWNER TO cash;

-- Index: scales_pkx

-- DROP INDEX scales_pkx;

CREATE UNIQUE INDEX scales_pkx
  ON scales
  USING btree
  (scale COLLATE pg_catalog."default");

-- Table: sizes

-- DROP TABLE sizes;

CREATE TABLE sizes
(
  scale character varying(20) NOT NULL DEFAULT 'NOSCALE'::character varying,
  cardsize character varying(20) NOT NULL DEFAULT 'NOSIZE'::character varying,
  CONSTRAINT sizes_scales_fk FOREIGN KEY (scale)
      REFERENCES scales (scale) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sizes
  OWNER TO cash;

-- Index: sizes__idx_cardsize

-- DROP INDEX sizes__idx_cardsize;

CREATE INDEX sizes__idx_cardsize
  ON sizes
  USING btree
  (cardsize COLLATE pg_catalog."default");

-- Index: sizes__idx_scale

-- DROP INDEX sizes__idx_scale;

CREATE INDEX sizes__idx_scale
  ON sizes
  USING btree
  (scale COLLATE pg_catalog."default");

-- Table: personal

-- DROP TABLE personal;

CREATE TABLE personal
(
  ident integer NOT NULL,
  description character varying(80) NOT NULL,
  passw character varying(15) NOT NULL,
  accesslevel integer NOT NULL DEFAULT 0,
  CONSTRAINT personal_pk PRIMARY KEY (ident),
  CONSTRAINT personal__un UNIQUE (passw)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE personal
  OWNER TO cash;

-- Index: personal__unx

-- DROP INDEX personal__unx;

CREATE UNIQUE INDEX personal__unx
  ON personal
  USING btree
  (passw COLLATE pg_catalog."default");

-- Index: personal_pkx

-- DROP INDEX personal_pkx;

CREATE UNIQUE INDEX personal_pkx
  ON personal
  USING btree
  (ident);



-- Table: keyboard

-- DROP TABLE keyboard;

CREATE TABLE keyboard
(
  objectname character varying(80) NOT NULL,
  facetext character varying(80),
  shortcut character varying(120),
  facefonts character varying(200),
  description character varying(120),
  CONSTRAINT keaboard_pk PRIMARY KEY (objectname)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE keyboard
  OWNER TO cash;

-- Index: keaboard_pkx

-- DROP INDEX keaboard_pkx;

CREATE UNIQUE INDEX keaboard_pkx
  ON keyboard
  USING btree
  (objectname COLLATE pg_catalog."default");


-- Table: error_tab

-- DROP TABLE error_tab;

CREATE TABLE error_tab
(
  err_num integer NOT NULL,
  description character varying(150),
  CONSTRAINT error_tab_pk PRIMARY KEY (err_num)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE error_tab
  OWNER TO cash;

-- Index: error_tab_pkx

-- DROP INDEX error_tab_pkx;

CREATE UNIQUE INDEX error_tab_pkx
  ON error_tab
  USING btree
  (err_num);


-- Table: depart

-- DROP TABLE depart;

CREATE TABLE depart
(
  department integer NOT NULL,
  description character varying(40) NOT NULL,
  CONSTRAINT depart_pk PRIMARY KEY (department),
  CONSTRAINT depart_description_un UNIQUE (description)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE depart
  OWNER TO cash;

-- Index: depart_description_unx

-- DROP INDEX depart_description_unx;

CREATE UNIQUE INDEX depart_description_unx
  ON depart
  USING btree
  (description COLLATE pg_catalog."default");

-- Index: depart_pkx

-- DROP INDEX depart_pkx;

CREATE UNIQUE INDEX depart_pkx
  ON depart
  USING btree
  (department);

-- Table: dclislst

-- DROP TABLE dclislst;

CREATE TABLE dclislst
(
  codeend character varying(20) NOT NULL,
  codestart character varying(20),
  CONSTRAINT dclislst_pk PRIMARY KEY (codeend)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dclislst
  OWNER TO cash;

-- Index: dclislst__idx

-- DROP INDEX dclislst__idx;

CREATE INDEX dclislst__idx
  ON dclislst
  USING btree
  (codestart COLLATE pg_catalog."default");

-- Index: dclislst_pkx

-- DROP INDEX dclislst_pkx;

CREATE UNIQUE INDEX dclislst_pkx
  ON dclislst
  USING btree
  (codeend COLLATE pg_catalog."default");



-- Table: credcard

-- DROP TABLE credcard;

CREATE TABLE credcard
(
  credcardindex integer NOT NULL,
  description character varying(80),
  limitsum numeric(12, 2) NOT NULL DEFAULT 0,
  canreturn integer,
  soft character varying(16),
  ident character varying(4),
  CONSTRAINT credcard_pk PRIMARY KEY (credcardindex)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE credcard
  OWNER TO cash;

-- Index: credcard_pkx

-- DROP INDEX credcard_pkx;

CREATE UNIQUE INDEX credcard_pkx
  ON credcard
  USING btree
  (credcardindex);


-- Table: credpref

-- DROP TABLE credpref;

CREATE TABLE credpref
(
  prefix character varying(20) NOT NULL,
  credcardindex integer,
  CONSTRAINT credpref_pk PRIMARY KEY (prefix),
  CONSTRAINT credpref_credcard_fk FOREIGN KEY (credcardindex)
      REFERENCES credcard (credcardindex) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE credpref
  OWNER TO cash;

-- Index: credpref_credcard_fkx

-- DROP INDEX credpref_credcard_fkx;

CREATE INDEX credpref_credcard_fkx
  ON credpref
  USING btree
  (credcardindex);

-- Index: credpref_pkx

-- DROP INDEX credpref_pkx;

CREATE UNIQUE INDEX credpref_pkx
  ON credpref
  USING btree
  (prefix COLLATE pg_catalog."default");



-- Table: clients

-- DROP TABLE clients;

CREATE TABLE clients
(
  id integer NOT NULL,
  ident character varying(20),
  description character varying(80),
  tel character varying(40),
  fax character varying(40),
  country character varying(80),
  addres character varying(100),
  mfo character varying(12),
  accounts character varying(80),
  bank character varying(100),
  bankaddress character varying(100),
  comments character varying(255),
  groop1 integer,
  groop2 integer,
  groop3 integer,
  groop4 integer,
  groop5 integer,
  deleted integer,
  CONSTRAINT clients_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE clients
  OWNER TO cash;

-- Index: clients_pkx

-- DROP INDEX clients_pkx;

CREATE UNIQUE INDEX clients_pkx
  ON clients
  USING btree
  (id);



-- Table: classif

-- DROP TABLE classif;

CREATE TABLE classif
(
  groop1 integer NOT NULL DEFAULT 0,
  groop2 integer NOT NULL DEFAULT 0,
  groop3 integer NOT NULL DEFAULT 0,
  groop4 integer NOT NULL DEFAULT 0,
  groop5 integer NOT NULL DEFAULT 0,
  description character varying(80),
  CONSTRAINT classif_pk PRIMARY KEY (groop1, groop2, groop3, groop4, groop5)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE classif
  OWNER TO cash;

-- Index: classif_pkx

-- DROP INDEX classif_pkx;

CREATE UNIQUE INDEX classif_pkx
  ON classif
  USING btree
  (groop1, groop2, groop3, groop4, groop5);


-- Table: pricekin

-- DROP TABLE pricekin;

CREATE TABLE pricekin
(
  pricelist integer NOT NULL,
  description character varying(80) NOT NULL,
  CONSTRAINT pricekin_pk PRIMARY KEY (pricelist),
  CONSTRAINT pricekin_description_un UNIQUE (description)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pricekin
  OWNER TO cash;

-- Index: pricekin_description_unx

-- DROP INDEX pricekin_description_unx;

CREATE UNIQUE INDEX pricekin_description_unx
  ON pricekin
  USING btree
  (description COLLATE pg_catalog."default");

-- Index: pricekin_pkx

-- DROP INDEX pricekin_pkx;

CREATE UNIQUE INDEX pricekin_pkx
  ON pricekin
  USING btree
  (pricelist);

-- Table: claslim

-- DROP TABLE claslim;

CREATE TABLE claslim
(
  groop1 integer NOT NULL DEFAULT 0,
  groop2 integer NOT NULL DEFAULT 0,
  groop3 integer NOT NULL DEFAULT 0,
  groop4 integer NOT NULL DEFAULT 0,
  groop5 integer NOT NULL DEFAULT 0,
  pricelist integer NOT NULL,
  percent numeric(4,2),
  CONSTRAINT claslim_pk PRIMARY KEY (groop1, groop2, groop3, groop4, groop5, pricelist),
  CONSTRAINT claslim_classif_fk FOREIGN KEY (groop1, groop2, groop3, groop4, groop5)
      REFERENCES classif (groop1, groop2, groop3, groop4, groop5) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT claslim_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE claslim
  OWNER TO cash;

-- Index: claslim_classif_fkx

-- DROP INDEX claslim_classif_fkx;

CREATE INDEX claslim_classif_fkx
  ON claslim
  USING btree
  (groop1, groop2, groop3, groop4, groop5);

-- Index: claslim_pkx

-- DROP INDEX claslim_pkx;

CREATE UNIQUE INDEX claslim_pkx
  ON claslim
  USING btree
  (groop1, groop2, groop3, groop4, groop5, pricelist);

-- Index: claslim_pricekin_fkx

-- DROP INDEX claslim_pricekin_fkx;

CREATE INDEX claslim_pricekin_fkx
  ON claslim
  USING btree
  (pricelist);


-- Table: clasdisc

-- DROP TABLE clasdisc;

CREATE TABLE clasdisc
(
  groop1 integer NOT NULL,
  groop2 integer NOT NULL,
  groop3 integer NOT NULL,
  groop4 integer NOT NULL,
  groop5 integer NOT NULL,
  pricelist integer NOT NULL,
  percent numeric(4,2) NOT NULL DEFAULT 0.0,
  CONSTRAINT clasdisc_pk PRIMARY KEY (groop1, groop2, groop3, groop4, groop5, pricelist),
  CONSTRAINT clasdisc_classif_fk FOREIGN KEY (groop1, groop2, groop3, groop4, groop5)
      REFERENCES classif (groop1, groop2, groop3, groop4, groop5) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT clasdisc_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE clasdisc
  OWNER TO cash;

-- Index: clasdisc_idx_classif

-- DROP INDEX clasdisc_idx_classif;

CREATE INDEX clasdisc_idx_classif
  ON clasdisc
  USING btree
  (groop1, groop2, groop3, groop4, groop5);

-- Index: clasdisc_pkx

-- DROP INDEX clasdisc_pkx;

CREATE UNIQUE INDEX clasdisc_pkx
  ON clasdisc
  USING btree
  (groop1, groop2, groop3, groop4, groop5, pricelist);

-- Index: clasdisc_pricekin_fkx

-- DROP INDEX clasdisc_pricekin_fkx;

CREATE INDEX clasdisc_pricekin_fkx
  ON clasdisc
  USING btree
  (pricelist);



-- Table: plucash

-- DROP TABLE plucash;

CREATE TABLE plucash
(
  articul character varying(30) NOT NULL,
  description character varying(80) NOT NULL,
  mesuriment character varying(30) NOT NULL,
  mespresision numeric(6,3) NOT NULL DEFAULT 0.0,
  add1 character varying(40),
  add2 character varying(40),
  add3 character varying(40),
  addnum1 numeric,
  addnum2 numeric,
  addnum3 numeric,
  scale character varying(20) NOT NULL,
  groop1 integer NOT NULL,
  groop2 integer NOT NULL,
  groop3 integer NOT NULL,
  groop4 integer NOT NULL,
  groop5 integer NOT NULL,
  department integer NOT NULL,
  deleted integer NOT NULL DEFAULT 0,
  CONSTRAINT plucash_pk PRIMARY KEY (articul),
  CONSTRAINT plucash_classif_fk FOREIGN KEY (groop1, groop2, groop3, groop4, groop5)
      REFERENCES classif (groop1, groop2, groop3, groop4, groop5) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT plucash_depart_fk FOREIGN KEY (department)
      REFERENCES depart (department) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT plucash_scales_fk FOREIGN KEY (scale)
      REFERENCES scales (scale) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE plucash
  OWNER TO cash;

-- Index: plucash_classif_fkx

-- DROP INDEX plucash_classif_fkx;

CREATE INDEX plucash_classif_fkx
  ON plucash
  USING btree
  (groop1, groop2, groop3, groop4, groop5);

-- Index: plucash_depart_fkx

-- DROP INDEX plucash_depart_fkx;

CREATE INDEX plucash_depart_fkx
  ON plucash
  USING btree
  (department);

-- Index: plucash_pkx

-- DROP INDEX plucash_pkx;

CREATE UNIQUE INDEX plucash_pkx
  ON plucash
  USING btree
  (articul COLLATE pg_catalog."default");

-- Index: plucash_scales_fkx

-- DROP INDEX plucash_scales_fkx;

CREATE INDEX plucash_scales_fkx
  ON plucash
  USING btree
  (scale COLLATE pg_catalog."default");

-- DROP TABLE taxes;

CREATE TABLE taxes
(
  id integer NOT NULL,
  priority integer NOT NULL,
  description character varying(80),
  CONSTRAINT taxes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taxes
  OWNER TO cash;


-- Table: tax

-- DROP TABLE tax;

CREATE TABLE tax
(
  articul character varying(30) NOT NULL,
  taxindex integer NOT NULL,
  taxvalue numeric(4,2),
  taxsum numeric(12, 2),
  CONSTRAINT tax_pk PRIMARY KEY (articul, taxindex),
  CONSTRAINT tax_plucash_fk FOREIGN KEY (articul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tax_taxes_fk FOREIGN KEY (taxindex)
      REFERENCES taxes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tax
  OWNER TO cash;

-- Index: tax_pkx

-- DROP INDEX tax_pkx;

CREATE UNIQUE INDEX tax_pkx
  ON tax
  USING btree
  (articul COLLATE pg_catalog."default", taxindex);

-- Index: tax_plucash_fkx

-- DROP INDEX tax_plucash_fkx;

CREATE INDEX tax_plucash_fkx
  ON tax
  USING btree
  (articul COLLATE pg_catalog."default");

-- Index: tax_plucash_fkx

-- DROP INDEX tax_plucash_fkx;

CREATE INDEX tax_plucash_fkx
  ON tax
  USING btree
  (articul COLLATE pg_catalog."default");


-- Table: plulim

-- DROP TABLE plulim;

CREATE TABLE plulim
(
  cardarticul character varying(30) NOT NULL,
  cardsize character varying(20) NOT NULL,
  pricelist integer NOT NULL,
  percent numeric(4,2) NOT NULL,
  CONSTRAINT plulim_pk PRIMARY KEY (cardarticul, pricelist, cardsize),
  CONSTRAINT plulim_plucash_fk FOREIGN KEY (cardarticul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT plulim_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE plulim
  OWNER TO cash;

-- Index: plulim_pkx

-- DROP INDEX plulim_pkx;

CREATE UNIQUE INDEX plulim_pkx
  ON plulim
  USING btree
  (cardarticul COLLATE pg_catalog."default", pricelist, cardsize COLLATE pg_catalog."default");

-- Index: plulim_plucash_fkx

-- DROP INDEX plulim_plucash_fkx;

CREATE INDEX plulim_plucash_fkx
  ON plulim
  USING btree
  (cardarticul COLLATE pg_catalog."default");

-- Index: plulim_pricekin_fkx

-- DROP INDEX plulim_pricekin_fkx;

CREATE INDEX plulim_pricekin_fkx
  ON plulim
  USING btree
  (pricelist);

-- Table: pludisc

-- DROP TABLE pludisc;

CREATE TABLE pludisc
(
  cardarticul character varying(30) NOT NULL,
  cardsize character varying(20) NOT NULL,
  pricelist integer NOT NULL,
  percent numeric(4,2),
  CONSTRAINT pludisc_pk PRIMARY KEY (cardarticul, pricelist),
  CONSTRAINT pludisc_plucash_fk FOREIGN KEY (cardarticul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pludisc_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pludisc
  OWNER TO cash;

-- Index: pludisc_pkx

-- DROP INDEX pludisc_pkx;

CREATE UNIQUE INDEX pludisc_pkx
  ON pludisc
  USING btree
  (cardarticul COLLATE pg_catalog."default", pricelist);

-- Index: pludisc_plucash_fkx

-- DROP INDEX pludisc_plucash_fkx;

CREATE INDEX pludisc_plucash_fkx
  ON pludisc
  USING btree
  (cardarticul COLLATE pg_catalog."default");

-- Index: pludisc_pricekin_fkx

-- DROP INDEX pludisc_pricekin_fkx;

CREATE INDEX pludisc_pricekin_fkx
  ON pludisc
  USING btree
  (pricelist);

-- Table: discsum

-- DROP TABLE discsum;

CREATE TABLE discsum
(
  pricelist integer NOT NULL,
  starttime time(0) without time zone NOT NULL,
  summa numeric(12, 2) NOT NULL DEFAULT 0.0,
  percent numeric(4,2) NOT NULL DEFAULT 0.0,
  CONSTRAINT discsum_pk PRIMARY KEY (pricelist),
  CONSTRAINT discsum_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE discsum
  OWNER TO cash;

-- Index: discsum_pkx

-- DROP INDEX discsum_pkx;

CREATE UNIQUE INDEX discsum_pkx
  ON discsum
  USING btree
  (pricelist);

-- Index: discsum_pricekin_fkx

-- DROP INDEX discsum_pricekin_fkx;

CREATE INDEX discsum_pricekin_fkx
  ON discsum
  USING btree
  (pricelist);

-- Table: disccli

-- DROP TABLE disccli;

CREATE TABLE disccli
(
  barcode character varying(20) NOT NULL,
  description character varying(80),
  percent numeric(4,2) NOT NULL DEFAULT 0.0,
  pricelist integer NOT NULL DEFAULT 1,
  clientindex integer,
  CONSTRAINT disccli_pk PRIMARY KEY (barcode),
  CONSTRAINT disccli_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE disccli
  OWNER TO cash;

-- Index: disccli_pkx

-- DROP INDEX disccli_pkx;

CREATE UNIQUE INDEX disccli_pkx
  ON disccli
  USING btree
  (barcode COLLATE pg_catalog."default");

-- Index: disccli_pricekin_fkx

-- DROP INDEX disccli_pricekin_fkx;

CREATE INDEX disccli_pricekin_fkx
  ON disccli
  USING btree
  (pricelist);

-- Table: disccard

-- DROP TABLE disccard;

CREATE TABLE disccard
(
  cardarticul character varying(30) NOT NULL,
  pricelist integer NOT NULL,
  quantity numeric(7,3) NOT NULL DEFAULT 0,
  pricerub numeric(12, 2) NOT NULL,
  percent numeric(4,2) NOT NULL,
  CONSTRAINT disccard_pk PRIMARY KEY (cardarticul, pricelist),
  CONSTRAINT disccard_plucash_fk FOREIGN KEY (cardarticul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT disccard_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT disccard_unkey UNIQUE (cardarticul, pricelist, quantity)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE disccard
  OWNER TO cash;

-- Index: disccard_pkx

-- DROP INDEX disccard_pkx;

CREATE UNIQUE INDEX disccard_pkx
  ON disccard
  USING btree
  (cardarticul COLLATE pg_catalog."default", pricelist);

-- Index: disccard_plucash_fkx

-- DROP INDEX disccard_plucash_fkx;

CREATE INDEX disccard_plucash_fkx
  ON disccard
  USING btree
  (cardarticul COLLATE pg_catalog."default");

-- Index: disccard_pricekin_fkx

-- DROP INDEX disccard_pricekin_fkx;

CREATE INDEX disccard_pricekin_fkx
  ON disccard
  USING btree
  (pricelist);

-- Index: disccard_unkeyx

-- DROP INDEX disccard_unkeyx;

CREATE UNIQUE INDEX disccard_unkeyx
  ON disccard
  USING btree
  (cardarticul COLLATE pg_catalog."default", pricelist, quantity);


-- Table: costs

-- DROP TABLE costs;

CREATE TABLE costs
(
  articul character varying(30) NOT NULL,
  price numeric(12, 2) NOT NULL,
  pricelist integer NOT NULL,
  modified timestamp(2) with time zone NOT NULL DEFAULT now(),
  CONSTRAINT costs_pk PRIMARY KEY (articul, pricelist, modified),
  CONSTRAINT costs_plucash_fk FOREIGN KEY (articul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT costs_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE costs
  OWNER TO cash;

-- Index: costs_idx

-- DROP INDEX costs_idx;

CREATE INDEX costs_idx
  ON costs
  USING btree
  (articul COLLATE pg_catalog."default", pricelist);

-- Index: costs_plucash_fkx

-- DROP INDEX costs_plucash_fkx;

CREATE INDEX costs_plucash_fkx
  ON costs
  USING btree
  (articul COLLATE pg_catalog."default");

-- Index: costs_pricekin_fkx

-- DROP INDEX costs_pricekin_fkx;

CREATE INDEX costs_pricekin_fkx
  ON costs
  USING btree
  (pricelist);


-- Table: cliclass

-- DROP TABLE cliclass;

CREATE TABLE cliclass
(
  groop1 integer NOT NULL,
  groop2 integer NOT NULL,
  groop3 integer NOT NULL,
  groop4 integer NOT NULL,
  groop5 integer NOT NULL,
  barcode character varying(20) NOT NULL,
  percent numeric(4,2) NOT NULL DEFAULT 0.0,
  pricelist integer NOT NULL DEFAULT 1,
  CONSTRAINT cliclass_pk PRIMARY KEY (barcode, pricelist, groop1, groop2, groop3, groop4, groop5),
  CONSTRAINT cliclass_classif_fk FOREIGN KEY (groop1, groop2, groop3, groop4, groop5)
      REFERENCES classif (groop1, groop2, groop3, groop4, groop5) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cliclass_disccli_fk FOREIGN KEY (barcode)
      REFERENCES disccli (barcode) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cliclass_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cliclass
  OWNER TO cash;

-- Index: cliclass_disccli_fkx

-- DROP INDEX cliclass_disccli_fkx;

CREATE INDEX cliclass_disccli_fkx
  ON cliclass
  USING btree
  (barcode COLLATE pg_catalog."default");

-- Index: cliclass_idx

-- DROP INDEX cliclass_idx;

CREATE INDEX cliclass_idx
  ON cliclass
  USING btree
  (groop1, groop2, groop3, groop4, groop5);

-- Index: cliclass_pkx

-- DROP INDEX cliclass_pkx;

CREATE UNIQUE INDEX cliclass_pkx
  ON cliclass
  USING btree
  (barcode COLLATE pg_catalog."default", pricelist, groop1, groop2, groop3, groop4, groop5);

-- Index: cliclass_pricekin_fkx

-- DROP INDEX cliclass_pricekin_fkx;

CREATE INDEX cliclass_pricekin_fkx
  ON cliclass
  USING btree
  (pricelist);

-- Table: checkformat

-- DROP TABLE checkformat;

CREATE TABLE checkformat
(
  setname character varying(40) NOT NULL,
  setorder integer NOT NULL,
  setstringvalue character varying(100),
  setintvalue integer NOT NULL,
  description character varying(100),
  CONSTRAINT checkformat_pk PRIMARY KEY (setname, setorder, setintvalue)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE checkformat
  OWNER TO cash;


-- Table: cashini

-- DROP TABLE cashini;

CREATE TABLE cashini
(
  id_int integer NOT NULL,
  value_name character varying(80) NOT NULL,
  value_int numeric(7,3),
  value_char character varying(40),
  CONSTRAINT cashini_pk PRIMARY KEY (id_int),
  CONSTRAINT cashini__un UNIQUE (value_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashini
  OWNER TO cash;

-- Index: cashini__unx

-- DROP INDEX cashini__unx;

CREATE UNIQUE INDEX cashini__unx
  ON cashini
  USING btree
  (value_name COLLATE pg_catalog."default");

-- Index: cashini_pkx

-- DROP INDEX cashini_pkx;

CREATE UNIQUE INDEX cashini_pkx
  ON cashini
  USING btree
  (id_int);

-- Table: bar

-- DROP TABLE bar;

CREATE TABLE bar
(
  barcode character varying(30) NOT NULL,
  cardarticul character varying(30) NOT NULL,
  cardsize character varying(20) NOT NULL DEFAULT 'NOSIZE'::character varying,
  quantity numeric(7,3) NOT NULL DEFAULT 0.0,
  CONSTRAINT bar_pk PRIMARY KEY (barcode),
  CONSTRAINT bar_plucash_fk FOREIGN KEY (cardarticul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bar
  OWNER TO cash;

-- Index: bar_pkx

-- DROP INDEX bar_pkx;

CREATE UNIQUE INDEX bar_pkx
  ON bar
  USING btree
  (barcode COLLATE pg_catalog."default");

-- Index: bar_plucash_fkx

-- DROP INDEX bar_plucash_fkx;

CREATE INDEX bar_plucash_fkx
  ON bar
  USING btree
  (cardarticul COLLATE pg_catalog."default");

-- Table: currests

-- DROP TABLE currests;

CREATE TABLE currests
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  modifed timestamp(2) with time zone NOT NULL,
  sale numeric(12, 2) NOT NULL DEFAULT 0.0,
  returnz numeric(12, 2) NOT NULL DEFAULT 0.0,
  storno numeric(12, 2) NOT NULL DEFAULT 0.0,
  stornret numeric(12, 2) NOT NULL DEFAULT 0.0,
  resultz numeric(12, 2) NOT NULL DEFAULT 0.0,
  wasoutput integer NOT NULL,
  CONSTRAINT currests_pk PRIMARY KEY (shopindex, cashnumber, znumber)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE currests
  OWNER TO cash;

-- Index: currests_pkx

-- DROP INDEX currests_pkx;

CREATE UNIQUE INDEX currests_pkx
  ON currests
  USING btree
  (shopindex, cashnumber, znumber);

-- Table: curmoney

-- DROP TABLE curmoney;

CREATE TABLE curmoney
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  beforez numeric(12, 2) NOT NULL,
  beforein numeric(12, 2) NOT NULL,
  beforeout numeric(12, 2) NOT NULL,
  periodin numeric(12, 2) NOT NULL,
  periodout numeric(12, 2) NOT NULL,
  afterz numeric(12, 2) NOT NULL,
  CONSTRAINT curmoney_pk PRIMARY KEY (shopindex, cashnumber, znumber),
  CONSTRAINT curmoney_currests_fk FOREIGN KEY (shopindex, cashnumber, znumber)
      REFERENCES currests (shopindex, cashnumber, znumber) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE curmoney
  OWNER TO cash;

-- Index: curmoney_currests_fkx

-- DROP INDEX curmoney_currests_fkx;

CREATE INDEX curmoney_currests_fkx
  ON curmoney
  USING btree
  (shopindex, cashnumber, znumber);

-- Index: curmoney_pkx

-- DROP INDEX curmoney_pkx;

CREATE UNIQUE INDEX curmoney_pkx
  ON curmoney
  USING btree
  (shopindex, cashnumber, znumber);


-- Table: cashsettings

-- DROP TABLE cashsettings;

CREATE TABLE cashsettings
(
  var_name character varying(100) NOT NULL,
  strvalue character varying(1024),
  intvalue integer,
  moneyvalue numeric(12, 2),
  numvalue numeric(9,3),
  datevalue timestamp(2) with time zone,
  description character varying(100),
  CONSTRAINT name_pk PRIMARY KEY (var_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashsettings
  OWNER TO cash;
 
-- Table: cashsail

-- DROP TABLE cashsail;

CREATE TABLE cashsail
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  id integer NOT NULL,
  modifed timestamp(2) with time zone NOT NULL DEFAULT now(),
  cardarticul character varying(30),
  cardsize character varying(20) NOT NULL DEFAULT 'NO_SIZE'::character varying,
  quantity numeric(8,3) NOT NULL DEFAULT 1,
  pricelist integer,
  pricerub numeric(12, 2) NOT NULL,
  totalrub numeric(12, 2) NOT NULL,
  department integer NOT NULL,
  ident integer NOT NULL,
  repl integer NOT NULL DEFAULT 1,
  operation integer NOT NULL DEFAULT 9,
  credcardindex integer NOT NULL DEFAULT 0,
  disccliindex integer NOT NULL DEFAULT 0,
  linked integer NOT NULL DEFAULT 0,
  CONSTRAINT cashsail_pk PRIMARY KEY (shopindex, cashnumber, znumber, checknumber, id),
  CONSTRAINT cashsail_credcard_fk FOREIGN KEY (credcardindex)
      REFERENCES credcard (credcardindex) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashsail_currests_fk FOREIGN KEY (shopindex, cashnumber, znumber)
      REFERENCES currests (shopindex, cashnumber, znumber) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashsail_depart_fk FOREIGN KEY (department)
      REFERENCES depart (department) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashsail_personal_fk FOREIGN KEY (ident)
      REFERENCES personal (ident) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashsail_plucash_fk FOREIGN KEY (cardarticul)
      REFERENCES plucash (articul) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashsail_pricekin_fk FOREIGN KEY (pricelist)
      REFERENCES pricekin (pricelist) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashsail
  OWNER TO cash;

-- Index: cashsail_credcard_fkx

-- DROP INDEX cashsail_credcard_fkx;

CREATE INDEX cashsail_credcard_fkx
  ON cashsail
  USING btree
  (credcardindex);

-- Index: cashsail_currests_fkx

-- DROP INDEX cashsail_currests_fkx;

CREATE INDEX cashsail_currests_fkx
  ON cashsail
  USING btree
  (shopindex, cashnumber, znumber);

-- Index: cashsail_depart_fkx

-- DROP INDEX cashsail_depart_fkx;

CREATE INDEX cashsail_depart_fkx
  ON cashsail
  USING btree
  (department);

-- Index: cashsail_personal_fkx

-- DROP INDEX cashsail_personal_fkx;

CREATE INDEX cashsail_personal_fkx
  ON cashsail
  USING btree
  (ident);

-- Index: cashsail_pkx

-- DROP INDEX cashsail_pkx;

CREATE UNIQUE INDEX cashsail_pkx
  ON cashsail
  USING btree
  (shopindex, cashnumber, znumber, checknumber, id);

-- Index: cashsail_plucash_fkx

-- DROP INDEX cashsail_plucash_fkx;

CREATE INDEX cashsail_plucash_fkx
  ON cashsail
  USING btree
  (cardarticul COLLATE pg_catalog."default");

-- Index: cashsail_pricekin_fkx

-- DROP INDEX cashsail_pricekin_fkx;

CREATE INDEX cashsail_pricekin_fkx
  ON cashsail
  USING btree
  (pricelist);


-- Table: cashpay

-- DROP TABLE cashpay;

CREATE TABLE cashpay
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  credcardindex integer NOT NULL,
  cardnumb character varying(20) NOT NULL DEFAULT 'cash'::character varying,
  payedrub numeric(12, 2),
  discountrub numeric(12, 2),
  discclirub numeric(12, 2),
  CONSTRAINT cashpay_pk PRIMARY KEY (shopindex, cashnumber, znumber, checknumber, credcardindex, cardnumb),
  CONSTRAINT cashpay_credcard_fk FOREIGN KEY (credcardindex)
      REFERENCES credcard (credcardindex) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashpay
  OWNER TO cash;

-- Index: cashpay_credcard_fkx

-- DROP INDEX cashpay_credcard_fkx;

CREATE INDEX cashpay_credcard_fkx
  ON cashpay
  USING btree
  (credcardindex);

-- Index: cashpay_pkx

-- DROP INDEX cashpay_pkx;

CREATE UNIQUE INDEX cashpay_pkx
  ON cashpay
  USING btree
  (shopindex, cashnumber, znumber, checknumber, credcardindex, cardnumb COLLATE pg_catalog."default");



-- Table: cashdisc

-- DROP TABLE cashdisc;

CREATE TABLE cashdisc
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  id integer NOT NULL,
  discountindex integer NOT NULL,
  percent numeric(4,2) NOT NULL,
  discountrub numeric(12, 2) NOT NULL,
  CONSTRAINT cashdisc_cashsail_fk FOREIGN KEY (shopindex, cashnumber, znumber, checknumber, id)
      REFERENCES cashsail (shopindex, cashnumber, znumber, checknumber, id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT discounts UNIQUE (shopindex, cashnumber, znumber, checknumber, id, discountindex)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashdisc
  OWNER TO cash;

-- Index: cashdisc_cashsail_fkx

-- DROP INDEX cashdisc_cashsail_fkx;

CREATE INDEX cashdisc_cashsail_fkx
  ON cashdisc
  USING btree
  (shopindex, cashnumber, znumber, checknumber, id);

CREATE UNIQUE INDEX cashdisc_pkx
  ON cashdisc
  USING btree
  (shopindex, cashnumber, znumber, checknumber, id, discountindex);

-- Table: cashdcrd

-- DROP TABLE cashdcrd;

CREATE TABLE cashdcrd
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  cardtype integer,
  cardnumber character varying(20) NOT NULL,
  discountrub numeric(12, 2),
  id integer NOT NULL,
  CONSTRAINT cashdcrd_pk PRIMARY KEY (shopindex, cashnumber, znumber, checknumber, cardnumber),
  CONSTRAINT cashdcrd_cashsail_fk FOREIGN KEY (shopindex, cashnumber, znumber, checknumber, id)
      REFERENCES cashsail (shopindex, cashnumber, znumber, checknumber, id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cashdcrd_disccli_fk FOREIGN KEY (cardnumber)
      REFERENCES disccli (barcode) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashdcrd
  OWNER TO cash;

-- Index: cashdcrd_cashsail_fkx

-- DROP INDEX cashdcrd_cashsail_fkx;

CREATE INDEX cashdcrd_cashsail_fkx
  ON cashdcrd
  USING btree
  (shopindex, cashnumber, znumber, checknumber, id);

-- Index: cashdcrd_disccli_fkx

-- DROP INDEX cashdcrd_disccli_fkx;

CREATE INDEX cashdcrd_disccli_fkx
  ON cashdcrd
  USING btree
  (cardnumber COLLATE pg_catalog."default");

-- Table: cashcmnt

-- DROP TABLE cashcmnt;

CREATE TABLE cashcmnt
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  comments character varying(10),
  credcardindex integer NOT NULL,
  CONSTRAINT cashcmnt_pk PRIMARY KEY (shopindex, cashnumber, znumber, checknumber)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashcmnt
  OWNER TO cash;

-- Index: cashcmnt_cashpay_fkx

-- DROP INDEX cashcmnt_cashpay_fkx;

CREATE INDEX cashcmnt_cashpay_fkx
  ON cashcmnt
  USING btree
  (shopindex, cashnumber, znumber, checknumber, credcardindex);


-- Table: cashauth

-- DROP TABLE cashauth;

CREATE TABLE cashauth
(
  shopindex integer NOT NULL,
  cashnumber integer NOT NULL,
  znumber integer NOT NULL,
  checknumber integer NOT NULL,
  id integer NOT NULL,
  cardnumb character varying(20),
  authcode character varying(10),
  CONSTRAINT cashauth_cashsail_fk FOREIGN KEY (shopindex, cashnumber, znumber, checknumber, id)
      REFERENCES cashsail (shopindex, cashnumber, znumber, checknumber, id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cashauth
  OWNER TO cash;

-- Index: cashauth_cashsail_fkx

-- DROP INDEX cashauth_cashsail_fkx;

CREATE INDEX cashauth_cashsail_fkx
  ON cashauth
  USING btree
  (shopindex, cashnumber, znumber, checknumber, id);



-- DROP TABLE buttons;

CREATE TABLE buttons
(
  object_name character varying(30) NOT NULL,
  face_text character varying(30),
  shortcut character varying(30),
  face_font character varying(100),
  description character varying(100),
  icon_path character varying(255),
  CONSTRAINT buttons_pkey PRIMARY KEY (object_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE buttons
  OWNER TO cash;









