--drop function findbar(bcd bar.barcode%type);
--**********************************************************
--������� ������ � �����������
--**********************************************************

create or replace function get_checkopen() returns cashsettings.intvalue%type as $$--1-��� ������, 0-��� ������, 2-�������� ����...get_reason
declare
check_open cashsettings.intvalue%type;
begin
select intvalue into check_open from cashsettings where var_name = 'check_open';
if not found then
	raise exception '����������� �������� ���������� ���������� check_open';
	end if;
return check_open;
end;
$$ language 'plpgsql';

create or replace function get_vstorno() returns integer as $$--������� �������������� �������
declare
vstorno integer;
begin
select intvalue into vstorno from cashsettings where var_name = 'value_storno';
if not found then
	raise exception '����������� �������� ���������� ���������� value_storno';
	end if;
return vstorno;
end;
$$ language 'plpgsql';

create or replace function get_currentid() returns cashsettings.intvalue%type as $$--���������� ������� �������
declare
current_id cashsettings.intvalue%type;
begin
select intvalue into current_id from cashsettings where var_name = 'current_id';
if not found then
	raise exception '����������� �������� ���������� ���������� current_id';
	end if;
return current_id;
end;
$$ language 'plpgsql';


create or replace function get_currency() returns cashsettings.strvalue%type as $$--������� ��������� �������� ������
declare
currency cashsettings.strvalue%type;
begin
select strvalue into currency from cashsettings where var_name = 'currency';
if not found then
	raise exception '����������� �������� ���������� ���������� currency';
	end if;
return currency;
end;
$$ language 'plpgsql';

create or replace function get_zopen() returns cashsettings.intvalue%type as $$--1-����� �������, 0-����� �������
declare
z_open cashsettings.intvalue%type;
begin
select intvalue into z_open from cashsettings where var_name = 'z_open';
if not found then
	raise exception '����������� �������� ���������� ���������� z_open';
	end if;
return z_open;
end;
$$ language 'plpgsql';

create or replace function reload_check() returns integer as $$--������� ������������� ����������� ���
declare
reload_check integer;
begin
if(get_checkopen() = get_reason_close()) then return 0; end if;--���� ��� �������� �����, ���������� ����� � ���������� ����
select intvalue into reload_check from cashsettings where var_name = 'reload_check';
if not found then
	raise exception '����������� �������� ���������� ���������� reload_check';
	end if;
return reload_check;
end;
$$ language 'plpgsql';

create or replace function get_vsale() returns integer as $$--������� ���� �������
declare
value_sale integer;
begin
select intvalue into value_sale from cashsettings where var_name = 'value_sale';
if not found then
	raise exception '����������� �������� ���������� ���������� value_sale';
	end if;
return value_sale;
end;
$$ language 'plpgsql';

create or replace function get_vreturn() returns integer as $$--������� ���� ��������
declare
value_return integer;
begin
select intvalue into value_return from cashsettings where var_name = 'value_return';
if not found then
	raise exception '����������� �������� ���������� ���������� value_return';
	end if;
return value_return;
end;
$$ language 'plpgsql';

create or replace function get_vpay_cash() returns integer as $$--������� ������� �� ��������
declare
value_pay_cash integer;
begin
select intvalue into value_pay_cash from cashsettings where var_name = 'value_pay_cash';
if not found then
	raise exception '����������� �������� ���������� ���������� value_pay_cash';
	end if;
return value_pay_cash;
end;
$$ language 'plpgsql';

create or replace function get_vpay_card() returns integer as $$--������� ������� �� �����
declare
value_pay_card integer;
begin
select intvalue into value_pay_card from cashsettings where var_name = 'value_pay_card';
if not found then
	raise exception '����������� �������� ���������� ���������� value_pay_card';
	end if;
return value_pay_card;
end;
$$ language 'plpgsql';

create or replace function get_vret_cash() returns integer as $$--������� �������� �� ��������
declare
value_ret_cash integer;
begin
select intvalue into value_ret_cash from cashsettings where var_name = 'value_ret_cash';
if not found then
	raise exception '����������� �������� ���������� ���������� value_ret_cash';
	end if;
return value_ret_cash;
end;
$$ language 'plpgsql';

create or replace function get_vret_card() returns integer as $$--������� �������� �� �����
declare
value_ret_card integer;
begin
select intvalue into value_ret_card from cashsettings where var_name = 'value_ret_card';
if not found then
	raise exception '����������� �������� ���������� ���������� value_ret_card';
	end if;
return value_ret_card;
end;
$$ language 'plpgsql';

create or replace function get_vdisc_limit() returns integer as $$--������� ����������� ������
declare
value_disc_limit integer;
begin
select intvalue into value_disc_limit from cashsettings where var_name = 'value_disc_limit';
if not found then
	raise exception '����������� �������� ���������� ���������� value_disc_limit';
	end if;
return value_disc_limit;
end;
$$ language 'plpgsql';

create or replace function get_vdisc_good() returns integer as $$--������� ������ �� ����� ��� ������ �������
declare
value_disc_good integer;
begin
select intvalue into value_disc_good from cashsettings where var_name = 'value_disc_good';
if not found then
	raise exception '����������� �������� ���������� ���������� value_disc_good';
	end if;
return value_disc_good;
end;
$$ language 'plpgsql';

create or replace function get_vdisc_cli() returns integer as $$--������� ������ �� ����� �������
declare
value_disc_clients integer;
begin
select intvalue into value_disc_clients from cashsettings where var_name = 'value_disc_clients';
if not found then
	raise exception '����������� �������� ���������� ���������� value_disc_clients';
	end if;
return value_disc_clients;
end;
$$ language 'plpgsql';

create or replace function get_vdisc_sum() returns integer as $$--������� ������ g ����� � �������
declare
value_disc_sum_time integer;
begin
select intvalue into value_disc_sum_time from cashsettings where var_name = 'value_disc_sum_time';
if not found then
	raise exception '����������� �������� ���������� ���������� value_disc_sum_time';
	end if;
return value_disc_sum_time;
end;
$$ language 'plpgsql';

create or replace function get_vdisc_quantity() returns integer as $$--������� ������ �� ���������� ������
declare
value_disc_quantity integer;
begin
select intvalue into value_disc_quantity from cashsettings where var_name = 'value_disc_quantity';
if not found then
	raise exception '����������� �������� ���������� ���������� value_disc_quantity';
	end if;
return value_disc_quantity;
end;
$$ language 'plpgsql';


create or replace function get_uplulim() returns integer as $$--������������ ����������� ������ �� �����
declare
use_plulim integer;
begin
select intvalue into use_plulim from cashsettings where var_name = 'use_plulim';
if not found then
	raise exception '����������� �������� ���������� ���������� use_plulim';
	end if;
return use_plulim;
end;
$$ language 'plpgsql';

create or replace function get_weight_prefix() returns varchar(2) as $$--������� �������� ������
declare
weight_prefix varchar(2);
begin
select strvalue into weight_prefix from cashsettings where var_name = 'weight_prefix';
if not found then
	raise exception '����������� �������� ���������� ���������� weight_prefix';
	end if;
return weight_prefix;
end;
$$ language 'plpgsql';


create or replace function get_udclislst() returns integer as $$--������������ ������������ ������ �� ������ �������
declare
use_dclislst integer;
begin
select intvalue into use_dclislst from cashsettings where var_name = 'use_dclislst';
if not found then
	raise exception '����������� �������� ���������� ���������� use_dclislst';
	end if;
return use_dclislst;
end;
$$ language 'plpgsql';

create or replace function get_uclients() returns integer as $$--������������ ������� ��������
declare
use_clients integer;
begin
select intvalue into use_clients from cashsettings where var_name = 'use_clients';
if not found then
	raise exception '����������� �������� ���������� ���������� use_clients';
	end if;
return use_clients;
end;
$$ language 'plpgsql';

create or replace function get_uclaslim() returns integer as $$--������������ ������������ ������ �� �����
declare
use_claslim integer;
begin
select intvalue into use_claslim from cashsettings where var_name = 'use_claslim';
if not found then
	raise exception '����������� �������� ���������� ���������� use_claslim';
	end if;
return use_claslim;
end;
$$ language 'plpgsql';

create or replace function get_udisccard() returns integer as $$--������������ �������������� ���� � ������ �� ����������
declare
use_disccard integer;
begin
select intvalue into use_disccard from cashsettings where var_name = 'use_disccard';
if not found then
	raise exception '����������� �������� ���������� ���������� use_disccard';
	end if;
return use_disccard;
end;
$$ language 'plpgsql';

create or replace function get_uclasdisc() returns integer as $$--������������ ������ �� ������ �������
declare
use_clasdisc integer;
begin
select intvalue into use_clasdisc from cashsettings where var_name = 'use_clasdisc';
if not found then
	raise exception '����������� �������� ���������� ���������� use_clasdisc';
	end if;
return use_clasdisc;
end;
$$ language 'plpgsql';


create or replace function get_upludisc() returns integer as $$--������������ ������������ ������ �� �����
declare
use_pludisc integer;
begin
select intvalue into use_pludisc from cashsettings where var_name = 'use_pludisc';
if not found then
	raise exception '����������� �������� ���������� ���������� use_pludisc';
	end if;
return use_pludisc;
end;
$$ language 'plpgsql';



create or replace function get_linked() returns integer as $$--�������� ����� ���������� ����
declare
linked integer;
begin
select intvalue into linked from cashsettings where var_name = 'linked';
if not found then
	raise exception '����������� �������� ���������� ���������� linked';
	end if;
return linked;
end;
$$ language 'plpgsql';

create or replace function get_reason_inserting() returns integer as $$--�������� ������� ���������� �������� �������
declare
reason_inserting integer;
begin
select intvalue into reason_inserting from cashsettings where var_name = 'reason_inserting';
if not found then
	raise exception '����������� �������� ���������� ���������� reason_inserting';
	end if;
return reason_inserting;
end;
$$ language 'plpgsql';

create or replace function get_reason_subtotal() returns integer as $$--�������� ������� preclose_check
declare
preclose_check integer;
begin
select intvalue into preclose_check from cashsettings where var_name = 'reason_subtotal';
if not found then
	raise exception '����������� �������� ���������� ���������� reason_subtotal';
	end if;
return preclose_check;
end;
$$ language 'plpgsql';

create or replace function get_reason_close() returns integer as $$--�������� ������� reason_close
declare
reason_close integer;
begin
select intvalue into reason_close from cashsettings where var_name = 'reason_close';
if not found then
	raise exception '����������� �������� ���������� ���������� reason_close';
	end if;
return reason_close;
end;
$$ language 'plpgsql';


create or replace function get_replace() returns integer as $$--�������� ����������� ��������
declare
repl integer;
begin
select intvalue into repl from cashsettings where var_name = 'replace';
if not found then
	raise exception '����������� �������� ���������� ���������� replace';
	end if;
return repl;
end;
$$ language 'plpgsql';

create or replace function get_casher() returns integer as $$--�������� ID �������� �������
declare
casher integer;
begin
select intvalue into casher from cashsettings where var_name = 'casher';
if not found then
	raise exception '����������� �������� ���������� ���������� casher';
	end if;
return casher;
end;
$$ language 'plpgsql';


create or replace function get_numeric_round() returns integer as $$--�������� ������� ���������� �������� �������
declare
numeric_round integer;
begin
select intvalue into numeric_round from cashsettings where var_name = 'numeric_round';
if not found then
	raise exception '����������� �������� ���������� ���������� numeric_round';
	end if;
return numeric_round;
end;
$$ language 'plpgsql';

create or replace function get_shopindex() returns integer as $$--�������� ������� ����� ��������
declare
shop_index integer;
begin
select intvalue into shop_index from cashsettings where var_name = 'shop_index';
if not found then
	raise exception '����������� �������� ���������� ���������� shop_index';
	end if;
return shop_index;
end;
$$ language 'plpgsql';


create or replace function get_cashnumber() returns integer as $$--�������� ������� ����� �����
declare
cash_number integer;
begin
select intvalue into cash_number from cashsettings where var_name = 'cash_number';
if not found then
	raise exception '����������� �������� ���������� ���������� cash_number';
	end if;
return cash_number;
end;
$$ language 'plpgsql';

create or replace function get_znumber() returns integer as $$--�������� ������� ����� �����
declare
znumber integer;
begin
select intvalue into znumber from cashsettings where var_name = 'znumber';
if not found then
	raise exception '����������� �������� ���������� ���������� znumber';
	end if;
return znumber;
end;
$$ language 'plpgsql';

create or replace function get_checknumber() returns integer as $$--�������� ������� ����� ����
declare
check_number integer;
begin
select intvalue into check_number from cashsettings where var_name = 'check_number';
if not found then
	raise exception '����������� �������� ���������� ���������� check_number';
	end if;
return check_number;
end;
$$ language 'plpgsql';

create or replace function get_idpos() returns integer as $$--��������� ������� � ����, ������ ��������� �� ������������, ������� ������� ����� ��������
declare
id_pos integer;
begin
select max(id)+1 into id_pos from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber());
if id_pos is NULL then
	id_pos:=1;
	end if;
return id_pos;
end;
$$ language 'plpgsql';

create or replace function get_cpricelist() returns integer as $$--��������� ��� ����
declare
current_pricelist integer;
begin
select intvalue into current_pricelist from cashsettings where var_name = 'current_pricelist';
if not found then
	raise exception '����������� �������� ���������� ���������� current_pricelist';
	end if;
return current_pricelist;
end;
$$ language 'plpgsql';

create or replace function get_discclibc() returns disccli.barcode%type as $$--������������� ������������� ������
declare
disccli_bc disccli.barcode%type;
begin
select strvalue into disccli_bc from cashsettings where var_name = 'disccli_bc';
if not found then
	raise exception '����������� �������� ���������� ���������� disccli_bc';
	end if;
return disccli_bc;
end;
$$ language 'plpgsql';

create or replace function get_scheck() returns disccli.barcode%type as $$--starting_check, ����� ������ ����
declare
starting_check costs.modified%type;
begin
select datevalue into starting_check from cashsettings where var_name = 'starting_check';
if not found then
	raise exception '����������� �������� ���������� ���������� starting_check';
	end if;
return starting_check;
end;
$$ language 'plpgsql';

create or replace function get_udisccli() returns integer as $$--������������ ������������ ������ �� �����
declare
use_disccli integer;
begin
select intvalue into use_disccli from cashsettings where var_name = 'use_disccli';
if not found then
	raise exception '����������� �������� ���������� ���������� use_disccli';
	end if;
return use_disccli;
end;
$$ language 'plpgsql';

create or replace function get_ucliclass() returns integer as $$--������������ ������������ ������ �� ������ �������
declare
use_cliclass integer;
begin
select intvalue into use_cliclass from cashsettings where var_name = 'use_cliclass';
if not found then
	raise exception '����������� �������� ���������� ���������� use_cliclass';
	end if;
return use_cliclass;
end;
$$ language 'plpgsql';

create or replace function nvl_numeric(arg numeric) returns numeric as $$--��������� �� NULL �������� � ���� ��� NULL ���������� 0
begin
if(arg is null) then
	return 0;
	end if;
return arg;
end;
$$ language 'plpgsql';

--********************************************************************************************************
--������� ������ � �����
--********************************************************************************************************

create or replace function get_percent_disccli() returns disccli.percent%type as $$--������� ������������ ������(������������ ���������� ������)
declare
percent_discount disccli.percent%type;
--use_disccli integer;
--current_pricelist integer;
--disccli_bc disccli.barcode%type;
begin
--current_pricelist = get_cpricelist();
--disccli_bc = get_discclibc();
--use_disccli = get_udisccli();--������������ ������� ������������ ������
percent_discount := 0;
if(get_udisccli() = 1) then
	select percent into percent_discount from disccli where (barcode = get_discclibc()) and (pricelist = get_cpricelist());
	if not found then
		percent_discount := 0;
		end if;
	end if;
return percent_discount;
end;
$$ language 'plpgsql';

create or replace function get_percent_disccli(disccli_bc disccli.barcode%type, current_pricelist integer) returns disccli.percent%type as $$--������� ������������ ������(�������� - �� ������)
declare
percent_discount disccli.percent%type;
--use_disccli integer;
--current_pricelist integer;
--disccli_bc disccli.barcode%type;
begin
--current_pricelist = get_cpricelist();
--disccli_bc = get_discclibc();
--use_disccli = get_udisccli();--������������ ������� ������������ ������
percent_discount := 0;
if(get_udisccli() = 1) then
	select percent into percent_discount from disccli where (barcode = get_discclibc()) and (pricelist = get_cpricelist());
	if not found then
		percent_discount := 0;
		end if;
	end if;
return percent_discount;
end;
$$ language 'plpgsql';


create or replace function FindBar(bcd bar.barcode%type) returns bc as $$--����� � ������� ���������-(������, ���������� � ��������, �������)
declare
ret bc;
begin
if(left(bcd, 2) = get_weight_prefix()) then--���� ����� �������, ������������ � ����������
	ret.cardarticul := substr(bcd, 3, 5);
	ret.cardsize := 'NOSIZE';
	ret.quantity := cast(substr(bcd, 8, 5) as int)/1000;
	return ret;
	end if;
select cardarticul, cardsize, quantity into ret.cardarticul, ret.cardsize, ret.quantity
from bar
where upper(bar.barcode) = upper(bcd);
if not found then
	raise exception '�������� % �� ������', bcd;
	end if;
return ret;
end;
$$ language plpgsql;

create or replace function FindArticul(bcd bc) returns art as $$--����� �� ��������-(�������������� ������, ��������� ����)
declare
ret art;
current_pricelist integer;
pr numeric(12, 2);
starting_check costs.modified%type;
numeric_round integer;
tmp integer;
begin
ret.id := 0;
starting_check := get_scheck();
current_pricelist := get_cpricelist();
bcd.cardarticul := upper(bcd.cardarticul);
numeric_round := get_numeric_round();
if(bcd.cardarticul is null) then--���� ������� ������ ������� - ������� � ������� �����������
	begin
	return ret;
	end;
	end if;
select ret.id, plucash.articul, plucash.description, 0 as price, 1 as quantity, 1 as current_quantity, 0 as summ,  mesuriment, mespresision, add1, add2, add3, addnum1, addnum2, addnum3, PLUCASH.SCALE, 'NOSIZE' as cardsize,
	groop1, groop2, groop3, groop4, groop5, department, deleted into ret
	from plucash where (plucash.articul = bcd.cardarticul);
if not found then
	raise exception '������� % �� ������', bcd.cardarticul;
	end if;
ret.cardsize := bcd.cardsize;
ret.quantity := bcd.quantity;
select costs.price into pr from costs
	where (PRICELIST = current_pricelist) and (articul = bcd.cardarticul) and
	modified = (select max(modified) from costs where (PRICELIST = current_pricelist) and (articul = bcd.cardarticul) and (modified < starting_check)); 
if not found then
	raise exception '��� ������ % ���� �� ������', ret.description;
	end if;
if mod(ret.quantity, ret.mespresision)>0 then
-- 	ret.quantity := ret.mespresision;
	raise exception '��� ������ % ������������ �������� ������� ��������� %', ret.description, ret.mespresision;
	end if;
ret.price := round(pr, numeric_round);--��������� ���� ������, � �����������
ret.summ := round(ret.price*ret.quantity, numeric_round);--��������, ��������, ����������
--������ ���������� ������ � ������� �������, ������� �������� �� �������������
select id into tmp from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = 0);
if FOUND then
	update cashsail set modifed = now(), cardarticul = bcd.cardarticul, cardsize = ret.cardsize, quantity = ret.quantity, pricelist = get_cpricelist(), pricerub = ret.price, totalrub = ret.summ, department = ret.department,
		ident = get_casher(), repl = get_replace(), operation = get_reason_inserting(), linked = 0 where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = 0);
	--�������� ������� cashdisc ��� �������������
	else
	insert into cashsail (shopindex, cashnumber, znumber, checknumber, id, modifed, cardarticul, cardsize, quantity, pricelist, pricerub, totalrub, department, ident, repl, operation, linked)
		values (get_shopindex(), get_cashnumber(), get_znumber(), get_checknumber(), 0, now(), bcd.cardarticul, ret.cardsize, ret.quantity, get_cpricelist(), ret.price, ret.summ, ret.department, get_casher(), get_replace(), get_reason_inserting(), 0);
	insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
		values (get_shopindex(), get_cashnumber(), get_znumber(), get_checknumber(), 0, 0, 0, 0);
	end if;
return ret;
end;
$$ language 'plpgsql';

create or replace function get_percent_cliclass(cl clas) returns cliclass.percent%type as $$--������� ������������ ������ �� ������ �������� ������
declare
percent_discount cliclass.percent%type;
--use_cliclass integer;
current_pricelist integer;
cliclass_bc disccli.barcode%type;
begin
current_pricelist := get_cpricelist();
cliclass_bc := get_discclibc();
--use_cliclass := get_ucliclass();--������������ ������� ������������ ������ �� ������
percent_discount := 0;
if((cliclass_bc is not null) AND (get_ucliclass()=1)) then
	for i in 1..5 loop
		if (i = 1) then
		select percent into percent_discount from cliclass where (barcode = cliclass_bc) and (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = cl.groop5);
			end if;
		if (i = 2) then
		select percent into percent_discount from cliclass where (barcode = cliclass_bc) and (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = 0);
			end if;
		if (i = 3) then
		select percent into percent_discount from cliclass where (barcode = cliclass_bc) and (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 4) then
		select percent into percent_discount from cliclass where (barcode = cliclass_bc) and (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 5) then
		select percent into percent_discount from cliclass where (barcode = cliclass_bc) and (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = 0) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if FOUND then
			return percent_discount;
			end if;
		end loop;
	end if;
return 0;
end;
$$ language 'plpgsql';

create or replace function get_percent_pludisc(bcd bc) returns pludisc.percent%type as $$
declare
percent_discount pludisc.percent%type;
current_pricelist integer;
begin
current_pricelist := get_cpricelist();
percent_discount := 0;
if(get_upludisc() = 1) then--������������ ������� ������ �� �����
	select percent into percent_discount from pludisc where (cardarticul = bcd.cardarticul) and (cardsize = bcd.cardsize) and (pricelist = current_pricelist);
	if not found then
		percent_discount := 0;
		end if;
	end if;
return percent_discount;
end;
$$language 'plpgsql';

create or replace function get_percent_clasdisc(cl clas) returns clasdisc.percent%type as $$--������� ������������ ������ �� ������ �������� ������
declare
percent_discount clasdisc.percent%type;
current_pricelist integer;
begin
current_pricelist := get_cpricelist();
percent_discount := 0;
if(get_uclasdisc() = 1) then--������������ ������� ������ �� ������ �������
	for i in 1..5 loop
		if (i = 1) then
		select percent into percent_discount from clasdisc where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = cl.groop5);
			end if;
		if (i = 2) then
		select percent into percent_discount from clasdisc where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = 0);
			end if;
		if (i = 3) then
		select percent into percent_discount from clasdisc where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 4) then
		select percent into percent_discount from clasdisc where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 5) then
		select percent into percent_discount from clasdisc where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = 0) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if FOUND then
			return percent_discount;
			end if;
		end loop;
	end if;
return 0;
end;
$$ language 'plpgsql';

create or replace function get_percent_disccard(bcd bc) returns disccard.percent%type as $$--�������������� ���� � ������ �� ����������
declare
current_pricelist integer;
percent_discount disccard.percent%type;
pr costs.price%type;
pr_disc disccard.pricerub%type;--���� �� ��������������� ����������
begin
percent_discount := 0;
if(get_udisccard()=1) then
	current_pricelist := get_cpricelist();
	select pricerub into pr_disc from disccard where (cardarticul = bcd.cardarticul) and (pricelist = current_pricelist) and (pricerub > 0) and
		(quantity = (select max(quantity) from disccard where (cardarticul = bcd.cardarticul) and (pricelist = current_pricelist) and (pricerub > 0) and (quantity <= bcd.quantity)));
	if not found then--���� ��� ������ � ������ ���������� ������� ������, ������� ������� ������
		pr_disc := 0;
		end if;
	if (pr_disc >0) then--���� ������ ����������� ���� 
		select costs.price into pr from costs
		where (PRICELIST = current_pricelist) and (articul = bcd.cardarticul) and
		modified = (select max(modified) from costs where (PRICELIST = current_pricelist) and (articul = bcd.cardarticul) and (modified < starting_check));
		percent_discount := (1-pr_disc/pr)*100;--��������� ������� ������?????????????????
		return percent_discount;--������� �������� ������
		end if;
	select percent into percent_discount from disccard where (cardarticul = bcd.cardarticul) and (pricelist = current_pricelist) and (pricerub = 0) and
		quantity = (select max(quantity) from disccard where (cardarticul = bcd.cardarticul) and (pricelist = current_pricelist) and (quantity <= bcd.quantity) and (pricerub = 0));
	if not found then--���� ���������� ������ ������ ����� ������, ������� �������� ��������
		return 0;
		end if;
	end if;
return percent_discount;
end;
$$ language 'plpgsql';

create or replace function get_percent_claslim(cl clas) returns claslim.percent%type as $$
declare
percent_limit claslim.percent%type;
current_pricelist integer;
begin
current_pricelist := get_cpricelist();
percent_limit := 0;
if(get_uclaslim() = 1) then--������������ ������� ����������� ������ �� ������ �������
	for i in 1..5 loop
		if (i = 1) then
		select percent into percent_limit from claslim where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = cl.groop5);
			end if;
		if (i = 2) then
		select percent into percent_limit from claslim where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = cl.groop4) and (groop5 = 0);
			end if;
		if (i = 3) then
		select percent into percent_limit from claslim where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = cl.groop3) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 4) then
		select percent into percent_limit from claslim where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = cl.groop2) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if (i = 5) then
		select percent into percent_limit from claslim where (pricelist = current_pricelist) and
			(groop1 = cl.groop1) and (groop2 = 0) and (groop3 = 0) and (groop4 = 0) and (groop5 = 0);
			end if;
		if FOUND then
			return percent_limit;
			end if;
		end loop;
	end if;
return 0;
end;
$$ language 'plpgsql';

create or replace function get_percent_plulim(bcd bc) returns plulim.percent%type as $$
declare
percent_limit plulim.percent%type;
begin
percent_limit := 0;
select percent into percent_limit from plulim where (cardarticul = bcd.cardarticul) and (cardsize = bcd.cardsize) and (pricelist = get_cpricelist());
if not FOUND then
	return 0;
	end if;
return percent_limit;
end;
$$ language 'plpgsql';


-- drop function open_check(linked integer);
create or replace function open_check(linked integer) returns int as $$--�������� ����(����������� �������� > 0, ����� ���� ��������, 0 �������)
declare
max_check cashsail.checknumber%type;
begin
if (linked > 0) then--���� �������
	select max(checknumber) into max_check from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = linked) and (cashsail.linked = 0);
	if not FOUND then
		raise '������� ��� ����������� ��� �� ���� ��� ���� ����������� �������� ��������';
		end if;
	update cashsettings set intvalue = linked, datevalue = now() where var_name = 'linked';
	update cashsettings set intvalue = get_vreturn(), datevalue = now() where var_name = 'replace';--��������� �������� ���� ��������
	else
	update cashsettings set intvalue = get_vsale(), datevalue = now() where var_name = 'replace';--��������� �������� ���� �������
	update cashsettings set intvalue = 0, datevalue = now() where var_name = 'linked';
	end if;
update cashsettings set intvalue = (intvalue+1), datevalue = now() where var_name = 'check_number';
update cashsettings set datevalue = now() where var_name = 'starting_check';--��������� ������� ������ ����
update cashsettings set intvalue = get_reason_inserting() where var_name = 'check_open';
return -1;
end;
$$ language 'plpgsql';


-- drop function open_day();
create or replace function open_day() returns int as $$--��������� ����� �����
declare
sum_before curmoney.beforez%type;
z_num currests.znumber%type;
begin
if(get_zopen()=0) then
	z_num:=get_znumber();
	select afterz into sum_before from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = z_num);
	if (not found) then sum_before:=0; end if;
	z_num:=z_num+1;	
	insert into currests values(get_shopindex(), get_cashnumber(), z_num, now(), 0, 0, 0, 0, 0, 0);
	insert into curmoney values(get_shopindex(), get_cashnumber(), z_num, sum_before, 0, 0, 0, 0, 0);
	update cashsettings set intvalue = z_num, datevalue=now() where var_name = 'znumber';--������� ����� �����, ���� �������� �����
	update cashsettings set intvalue = 1, datevalue = now() where var_name = 'z_open';--���������� ������� �������� �����
	end if;
return -1;
end;
$$ language 'plpgsql';


create or replace function FindPos0() returns art as $$
declare
ret art;
begin
select cardarticul, cardsize, quantity, pricerub, totalrub, department into ret.articul, ret.cardsize, ret.quantity, ret.price, ret.summ, ret.department from cashsail where (shopindex = get_shopindex())
 and (cashnumber =  get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id=0);
return ret;
end;
$$ language 'plpgsql';

create or replace function add_position(bcd bc) RETURNS art AS $$--���������� �������� ������� � ��������� ������������ ������ � ���� �����������, ���������� ���������� �������, ����� ������ �����
DECLARE
shop_index integer;
cash_number integer;
z_number integer;
check_number integer;
qnt cashsail.quantity%type;
current_pricelist integer;--������� ��������� ��� ����
disccli_bc disccli.barcode%type;--�������� ������� �� ������
discount numeric(12, 2);--��� ���������
percent_discount numeric(4, 2);--������� ������
current_discount numeric(4, 2);--����������� �� ������ ����� �������
limit_discount numeric(4, 2);--����� �����������
ret art;
previous_pos art;--���������� �������
cla clas;--����� �������������� �������� ������
--bcd bc;--����� ������
use_disccli integer;
use_cliclass integer;
upd_count int;
numeric_round integer;
tmp integer;
BEGIN
--*************
if(get_zopen() = 0) then
	tmp:=open_day();
	end if;--���� ����� �� ���� �������, ��������� �����
if(get_checkopen() = get_reason_close()) then
	tmp:=open_check(0);
	end if;--���� ��� �� ��� ������, ��������� ��� �������
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� �������� ����'; end if;--���� �� ���� �������� ����, ����� ����������(���������� ���������� �� rollback)
--*************
current_discount := 0;
--���� �������� �������
--id_pos := get_idpos();
shop_index := get_shopindex();
cash_number := get_cashnumber();
z_number := get_znumber();
check_number := get_checknumber();
current_pricelist := get_cpricelist();
numeric_round := get_numeric_round();
--**************
previous_pos:=FindPos0();--���� �����, ��������� � ������� �������, �������� ������ � ����� ������������������, � ��������� ������ ����������� 
ret := FindArticul(bcd);--���� ������� ������� � ���������� �� � ������� �������
-- ret.current_quantity := bcd.quantity;
-- ret.summ := round(ret.price*ret.quantity, numeric_round);
--��������� ��� �������� cashsail
-- previous_pos.articul:='';
-- previous_pos:=FindPos0();--���� �����, ��������� � ������� �������
if(previous_pos.articul is null OR  previous_pos.summ = 0) then --���� ���������� ������� ���(���� ������ ������, ���� ������ �������) ��� ����� �� ��� ����� ����
	--ret.id:=get_idpos(); ����� id ������������� ��������� �����
	return ret;
	end if;
select id, quantity into previous_pos.id, qnt from cashsail where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number)
	and (cardarticul = previous_pos.articul) and (cardsize = previous_pos.cardsize) and (operation = get_reason_inserting()) and (id > 0);--���� �������� ������� ��� ������������ � ����
if FOUND then --��������� ����������� ������� ������ � ����
	--����� ����� ������� �����, � ��������� ����������
	previous_pos.quantity := previous_pos.quantity + qnt;--�������� ����������
	previous_pos.summ := round(previous_pos.price*previous_pos.quantity, numeric_round);--������������ �����	
	update cashsail set quantity = previous_pos.quantity, totalrub = previous_pos.summ,  modifed = now() where 
		(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id);
	update cashsettings set intvalue = previous_pos.id, datevalue = now() where var_name = 'current_id';--��������� ������ ������� �������
	--id_pos:=previous_pos.id;
	else --�� ����� ����� �����
	previous_pos.id := get_idpos();--����� � ����������� ����� �������
	insert into cashsail (shopindex, cashnumber, znumber, checknumber, id, modifed, cardarticul, cardsize, quantity, pricelist, pricerub, totalrub, department, ident, repl, operation, linked)
		values (shop_index, cash_number, z_number, check_number, previous_pos.id, now(), previous_pos.articul, previous_pos.cardsize, previous_pos.quantity, current_pricelist, previous_pos.price,
			previous_pos.summ, previous_pos.department, get_casher(), get_replace(), get_reason_inserting(), get_linked());
	insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
				values(shop_index, cash_number, z_number, check_number, previous_pos.id, 0/*������� ������ ���*/, current_discount/*� ���� ����� ����� ����*/, /*round(previous_pos.summ*current_discount/100, numeric_round)*/0);
	update cashsettings set intvalue = previous_pos.id, datevalue = now() where var_name = 'current_id';--��������� ������ ������� �������
	end if;
--previous_pos.cardsize := previous_pos.cardsize;--�� ������ �������
--��������� �� ����� ���������� �����������
select percent into percent_discount from cashdisc where
	(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_limit());
if FOUND then
--����� �����������
--����� ��� �������, ���������� �����, � ����� ������� ����� ������� � ������� ����
	update cashdisc set discountrub = round(previous_pos.summ*cashdisc.percent/100, numeric_round) where
		(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_limit());--�������� ����� ������ � ������ ������ ����������
-- 	select previous_pos.summ-sum(discountrub) into previous_pos.summ from cashdisc where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id);--�������� ����� �������
-- 	previous_pos.price := round(previous_pos.price*(1-percent_discount/100), numeric_round);--���� � ���������� �������
	--*****************************
	ret:=previous_pos;
	return ret;--����� �� �������!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	end if;
--���������� �������� �����������
limit_discount := get_percent_plulim(bcd);
if(limit_discount = 0) then--���� ����������� �� ����� �� ��������
	limit_discount := get_percent_claslim(cla);--��������� �������������
	end if;
--��������� ����������� ����� �������
disccli_bc = get_discclibc();
cla.groop1=previous_pos.groop1;
cla.groop2=previous_pos.groop2;
cla.groop3=previous_pos.groop3;
cla.groop4=previous_pos.groop4;
cla.groop5=previous_pos.groop5;
if (disccli_bc is not null) then--���� ����� ���� ������� ��������, ��������� ������ �� ��������
	current_discount := get_percent_disccli();--��������� ���������� ������ �� �����
	if(current_discount = 0) then--���� ������������ ������ �� ����� �� ��������� ��� ���������, ��������� ������������ ������ �� ������
		current_discount := get_percent_cliclass(cla);
		end if;
	if(current_discount <> 0) then--���� ������������ ������ ����������
		if(abs(current_discount)>abs(limit_discount)) then current_discount:= limit_discount; end if;
		--���������, ���� �� ��� ������ ��������� � ��� ����� ��������
		select discountrub from cashdisc where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_cli());
		if FOUND then
			update cashdisc set percent = current_discount, discountrub = round(previous_pos.summ*current_discount/100, numeric_round) where
				(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_cli());
			else
			insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
				values(shop_index, cash_number, z_number, check_number, previous_pos.id, get_vdisc_cli(), current_discount, round(previous_pos.summ*current_discount/100, numeric_round));
			end if;--if FOUND then
		--***********************
		if(current_discount=limit_discount) then
			ret:=previous_pos;
			return ret;
			end if;--���� �� ������� �� ������ ������� �� �������� �����������, ���� ������� �� �������
		end if;--if(current_discount <> 0) then--���� ������������ ������ ����������
	end if;--��������� ������� ������������ ������
percent_discount := current_discount;
--��������� ������ �� �����
current_discount := get_percent_pludisc(bcd);--��������� ������ �� �����
if(current_discount = 0) then--���� ������ �� ����� �� ����������
	current_discount := get_percent_clasdisc(cla);
	end if;
if(current_discount <> 0) then--���� ������ �� ����� ����������
	discount := percent_discount+current_discount;--��������� ������ �� ����� � ������ �������
	if(abs(discount)>abs(limit_discount)) then current_discount:= limit_discount-percent_discount; end if;
	--��������, �� ������������ �����
	select discountrub from cashdisc where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_good());
	if FOUND then
		update cashdisc set percent = current_discount, discountrub = round(previous_pos.summ*current_discount/100, numeric_round) where
				(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_good());
		else
		insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
				values(shop_index, cash_number, z_number, check_number, previous_pos.id, get_vdisc_good(), current_discount, round(previous_pos.summ*current_discount/100, numeric_round));		
		end if;--if FOUND then
	end if;--if(current_discount <> 0) then--���� ������ �� ����� ����������
--��������� ������ �� ����������
current_discount := get_percent_disccard(bcd);
if(current_discount<>0) then--���� ������ �� ���������� ����������
	--��������, �� ������������ �����
	select discountrub from cashdisc where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_good());
	if FOUND then
		update cashdisc set percent = current_discount, discountrub = (round(previous_pos.summ*current_discount/100, numeric_round)) where
				(shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = previous_pos.id) and (discountindex = get_vdisc_quantity());
		else
		insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
				values(shop_index, cash_number, z_number, check_number, previous_pos.id, get_vdisc_quantity(), current_discount, round(previous_pos.summ*current_discount/100, numeric_round));		
		end if;--if FOUND then
	if((current_discount+percent_discount)=limit_discount) then
		ret:=previous_pos;
		return ret;
		end if;
	end if;--if(current_discount<>0) then--���� ������ �� ���������� ����������
ret:=previous_pos;
return ret;			
END;
$$
LANGUAGE 'plpgsql';


create or replace function get_clients(bc clients.ident%type) returns client as $$--����� ����� ������, � ��������� ����������
declare
return_client client;
ce dclislst.codeend%type;
deleted clients.deleted%type;--������� ��������������� �������� �� �����
o_check record;--������� ��������� ����
bcd bc;
tmp art; 
begin
return_client.id := 0;
return_client.description := 'no name';
return_client.comments := 'no coments';
if(get_uclients() = 1) then--���� ������������ ������� �������� � �������������� ������������ ������!!!
	if(get_udclislst() = 1) then --eckb ������������ ������� ��������
		select codeend from dclislst where (codeend = bc);
		if FOUND then
			raise '�������� ������� ���������� ��� �������';
			end if;
		--����������� ������ ������ � ����������
		end if;
	select id, description, clients.comments, clients.deleted into return_client.id, return_client.description, return_client.comments, deleted from clients where (ident = bc);
	if not FOUND then
		raise '����� ������� �� ����������';
		--return  return_client;
		end if;
	if (deleted = 1) then
		raise '������������ ������� ������� ���������������';
		end if;
	update cashsettings set strvalue = bc, datevalue = now() where var_name = 'disccli_bc';--����� ���� �������� ������� � ���������� ����� �������, �������� �� ���������
	--select strvalue into disccli_bc from cashsettings where var_name = 'disccli_bc';
	for o_check in select cardarticul, cardsize, quantity from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) loop --�������� ��� �������� �������
		bcd := o_check;--������� ��� ��������������
		bcd.quantity := 0;--������������� ������� ����������
		tmp := add_position(bcd);
		end loop;
	end if;
return return_client;
end;
$$ language 'plpgsql';



--����� ����
-- drop view check_pos;
CREATE OR REPLACE VIEW check_pos AS 
 SELECT cd.id, pc.articul, pc.description, cs.cardsize, cs.department,
    cast(cs.pricerub as varchar)||' '||get_currency() as price, cast(cs.quantity as varchar)||' '||pc.mesuriment as quantity, cast(cs.totalrub as varchar)||' '||get_currency() as totalrub,
    cast(sum(cd.discountrub) as varchar)||' '||get_currency() as discountrub,
    cast((totalrub - sum(cd.discountrub)) as varchar)||' '||get_currency() as totalsum    
   FROM plucash pc, cashsail cs, cashdisc cd
  WHERE (pc.articul::text = cs.cardarticul::text) AND cs.shopindex = get_shopindex() AND cs.cashnumber = get_cashnumber() AND cs.znumber = get_znumber() and cs.checknumber = get_checknumber() and
  cs.shopindex = cd.shopindex AND cs.cashnumber = cd.cashnumber and cs.znumber = cd.znumber AND cs.checknumber = cd.checknumber and cs.id = cd.id
  group by cd.id, pc.articul, pc.description, cs.cardsize, cs.pricerub, cs.quantity, cs.totalrub, cs.department order by cd.id;
-- -- 
-- drop view check_pg;
CREATE OR REPLACE VIEW check_pg AS --� ������ ������������� ����������� ������� �������
 SELECT cd.id, pc.articul, pc.description, cs.cardsize, cs.department,
    cs.pricerub as price, cs.quantity as quantity, pc.mespresision, cs.totalrub as totalrub, sum(cd.discountrub) as discountrub, (totalrub - sum(cd.discountrub)) as totalsum    
   FROM plucash pc, cashsail cs, cashdisc cd
  WHERE (pc.articul::text = cs.cardarticul::text) AND cs.shopindex = get_shopindex() AND cs.cashnumber = get_cashnumber() AND cs.znumber = get_znumber() AND cs.checknumber = get_checknumber() and
  cs.shopindex = cd.shopindex AND cs.cashnumber = cd.cashnumber and cs.znumber = cd.znumber AND cs.checknumber = cd.checknumber and cs.id = cd.id 
  group by cd.id, pc.articul, pc.description, cs.cardsize, cs.pricerub, cs.quantity, cs.totalrub, cs.department order by cd.id;
  
drop view v_cashgood;
create or replace view v_cashgood as
select cs.shopindex, cs.cashnumber, cs.znumber, 3 as repl, cs.department,  cs.cardarticul, cs.cardsize, sum(quantity) as sum_quantity, sum(cs.totalrub-(select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and
	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber) and (cs.checknumber = cd.checknumber) and (cs.id = cd.id))) as sum_total from cashsail cs, currests cr where (cs.shopindex = cr.shopindex) and
	(cs.cashnumber = cr.cashnumber) and (cs.znumber = cr.znumber) and (cs.linked = 0) and (cr.wasoutput = 0) group by cs.shopindex, cs.cashnumber, cs.znumber, cs.department, cs.cardarticul, cs.cardsize 
	order by cs.shopindex, cs.cashnumber, cs.znumber, cs.department, cs.cardarticul, cs.cardsize;

-- drop view v_cashpay;--����������� �������
create or replace view v_cashpay as 
select cp.shopindex, cp.cashnumber, cp.znumber, cp.checknumber, cp.credcardindex as payment, cp.cardnumb, cp.payedrub as payedmoney, cp.discountrub as discountmoney, cp.payedrub,
	cp.payedrub as payedcur, cp.discountrub, cp.discountrub as discountcur, cp.discclirub, cp.discclirub as disc�li�ur from cashpay cp, currests cr
	where (cp.shopindex = cr.shopindex) and (cp.cashnumber = cr.cashnumber) and (cp.znumber = cr.znumber) and (cr.wasoutput = 0);

-- drop view v_curmoney;--������ � ��������� �������
create or replace view v_curmoney as
select cm.shopindex, cm.cashnumber, cm.znumber, 0 as curidx, cm.beforez, cm.beforein, cm.beforeout, cm.periodin, cm.periodout, cm.afterz from curmoney cm, currests cr
	where (cm.shopindex = cr.shopindex) and (cm.cashnumber = cr.cashnumber) and (cm.znumber = cr.znumber) and (cr.wasoutput = 0);

-- drop view v_currests;--�������� �����
create or replace view v_currests as
select cr.shopindex, cr.cashnumber, cr.znumber, to_char(cr.modifed, 'DD.MM.YYYY') as dt, cr.sale, cr.returnz, cr.storno, cr.stornret, cr.resultz, cr.wasoutput from currests cr
	where (cr.wasoutput = 0);

-- drop view v_cashsale;--����������� �����, ������ ��� ����������� ��
create or replace view v_cashsale as
select cs.shopindex, cs.cashnumber, cs.znumber, cs.checknumber, cs.id, to_char(cs.modifed, 'DD.MM.YYYY') as dt, cast(to_char(cs.modifed, 'HH24MI') as smallint) as tm, cs.cardarticul, cs.cardsize,
	cs.quantity, cs.pricerub, cs.pricerub as pricecur, cs.totalrub, cs.totalrub as totalcur, cs.department, cs.ident, 0, cs.repl, cs.operation, cs.credcardindex, cs.disccliindex, cs.linked
	from cashsail cs, currests cr where (cs.shopindex = cr.shopindex) and (cs.cashnumber = cr.cashnumber) and (cs.znumber = cr.znumber) and (cr.wasoutput = 0);

-- drop view v_cashdisc;--����������� ������
create or replace view v_cashdisc as
select cd.shopindex, cd.cashnumber, cd.znumber, cd.checknumber, cd.id, cd.discountindex, cd.percent, cd.discountrub, cd.discountrub as discountcur
	from cashdisc cd, currests cr where (cd.shopindex = cr.shopindex) and (cd.cashnumber = cr.cashnumber) and (cd.znumber = cr.znumber) and (cr.wasoutput = 0);


-- drop view v_plucash;--���������� ������� � ����������� ������
CREATE OR REPLACE VIEW v_plucash AS 
 SELECT p.articul, p.description, c.price, p.mesuriment, p.mespresision, p.scale, p.groop1, p.groop2, 
    p.groop3, p.groop4, p.groop5, p.department, p.deleted
   FROM plucash p, costs c
  WHERE p.articul::text = c.articul::text AND c.modified = (( SELECT max(c1.modified) AS max
           FROM costs c1
          WHERE c.articul::text = c1.articul::text));

-- drop view v_classif;
create or replace view v_classif as
(select vp.groop1, vp.groop2, vp.groop3, vp.groop4, vp.groop5, vp.articul, vp.description, vp.price from v_plucash vp
union all
select c.groop1, c.groop2, c.groop3, c.groop4, c.groop5, '' as c_art, c.description, 0 as c_pr  from classif c)
order by groop1, groop2, groop3, groop4, groop5;



	

create or replace function get_total() returns cashsail.totalrub%type as $$--�������� ������������ ��������
declare
ret cashsail.totalrub%type;
begin
if(get_checkopen() = get_reason_close()) then return 0; end if;
select sum(totalsum) into ret from check_pg;
return ret;
end;
$$ language 'plpgsql';

create or replace function get_percent_discsum() returns discsum.percent%type as $$--��������� �������� ������ �� �����/�������
declare
ret discsum.percent%type;
begin
ret :=0;
if(get_vdisc_sum() = 1) then --���� ������� ������ �� ����� ������� ������������
	select percent into ret from disc_sum ds1 where (ds1.pricelist = get_cpricelist()) and (get_total() = 
	(select max(ds2.summa) from disc_sum ds2 where (ds1.starttime = ds2.starttime) and (ds1.pricelist = ds2.pricelist))) and (ds1.starttime = 
	(select min(ds3.starttime) from disc_sum ds3 where (ds1.starttime = ds3.starttime) and (ds1.summa = ds3.summa)));
	end if;
return ret;
end;
$$ language 'plpgsql';

-- drop function subtotal();
create or replace function subtotal() returns cashsail.id%type as $$ --����� ������ ��������� ������������� ������������� ���� �� ������������ �������� select reload_check()
declare
ret cashsail.totalrub%type;
current_discount discsum.percent%type;
total_discount discsum.percent%type;
o_check record;--������ ������� ����
id_pos cashsail.id%type;--����� ������� � ����
bcd bc;
ar art;
cla clas;
discclibc cashdcrd.cardnumber%type;
tmp1 art;
tmp2 bc;
begin
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� ��� �������� ����'; end if;
if(get_checkopen() = get_reason_close()) then raise '��� �������� �����'; end if;
tmp1:=add_position(tmp2);
discclibc:=get_discclibc();
--������� ������� �������
delete from cashdisc where shopindex = get_shopindex() and cashnumber = get_cashnumber() and znumber = get_znumber() and checknumber = get_checknumber() and id = 0;
delete from cashsail where shopindex = get_shopindex() and cashnumber = get_cashnumber() and znumber = get_znumber() and checknumber = get_checknumber() and id = 0;
if(length(discclibc) > 0) then --���� ���� �������� ����� �������
	insert into cashdcrd values(get_shopindex(), get_cashnumber(), get_znumber(), get_checknumber(), 0, discclibc, 
		(select sum(discountrub) from cashdisc where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (discountindex = get_vdisc_cli())), 1);--���� ���� �����������, ����� ����� ����� 0, ��� ����� ������ ����������� �� ������ �������... � ����������� ��������, ���������� ��������� �� �������� � �����
	end if;
current_discount := get_percent_discsum();--��������� �������� �� �����-�������
if (current_discount <> 0) then--���� ������� �� �������
	id_pos:=0;
	for o_check in select articul, cardsize, pricerub, quantity, totalrub from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) loop
		id_pos:=id_pos+1;
		--��������� �� ���������� ����� ����������� �� ������
		select percent from cashdisc where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = id_pos) and (discountindex = get_vdisc_limit());
		if FOUND then continue; end if;--���� ������� ����������, ��������� � ���������
		--select intvalue into reload_check from cashsettings where var_name = 'reload_check';
		insert into cashdisc values(get_shopindex(), get_cashnumber(), get_znumber(), get_checknumber(), id_pos, get_vdisc_sum(), current_discount, round(o_check.totalrub*current_discount/100, get_numeric_round()));
		--��������� ��������
		select sum(percent) into total_discount from cashdisk where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = id_pos);
		--��������� �����������
		bcd.cardarticul:=o_check.articul;
		bcd.cardsize:=o_check.cardsize;
		bcd.quantity:=o_check.quantity;
		current_discount := get_percent_plulim(bcd);
		if(current_discount = 0) then--���� ����������� �� ����� �� ��������
			ar:=FindArticul(bcd);
			cla.groop1:=ar.groop1;
			cla.groop2:=ar.groop2;
			cla.groop3:=ar.groop3;
			cla.groop4:=ar.groop4;
			cla.groop5:=ar.groop5;
			current_discount := get_percent_claslim(cla);
			end if;
		if(total_discount < current_discount) then continue; end if;
		--���� ��������, ���� �������� �� �����������
		update cashdisc set discountrub = 0 where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = id_pos)/* and (discountindex <> get_vdisc_limit())*/;
		--ret.price := round(ret.price*(1-percent_discount/100), numeric_round);
		insert into cashdisc (shopindex, cashnumber, znumber, checknumber, id, discountindex, percent, discountrub)
				values(shop_index, cash_number, znumber, check_number, id_pos, get_vdisc_limit(), current_discount, round(ret.summ*current_discount/100, numeric_round));
		--ret.summ := ret.summ - round(ret.summ*current_discount/100, numeric_round);
		end loop;
			update cashsettings set intvalue = 1, datevalue = now() where var_name = 'reload_check';--���������� ������� ������������� ������� ����
	end if;--if (current_discount) then--���� ������� �� �������
update cashsail set operation = get_reason_subtotal() where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (operation = get_reason_inserting());--�������� preclose_check, ������ ��� ����������� �������
update cashsettings set intvalue = get_reason_subtotal(), datevalue = now() where var_name = 'check_open';
-- update cashsettings set intvalue = get_reason_inserting() where var_name = 'check_open';
return tmp1.id;
end;
$$ language 'plpgsql';


create or replace function close_check(au cash_auth) returns cashsail.totalrub%type as $$ --�������� ���� � �������� �����
declare
ret cashsail.totalrub%type;
begin
if(get_checkopen() = get_reason_close()) then raise '��� �������� �����'; end if;
if(get_checkopen() = get_reason_inserting()) then raise '�� ���� �� �������� ����'; end if;
ret:= au.summ - get_total();--������� �����
if(ret >= 0) then --���� ���������� �� ���������� ����� ������ ��� ����� ����� ����
	update cashsail set operation = get_reason_close(), credcardindex = au.id where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and
		(checknumber = get_checknumber()) and (operation = get_reason_subtotal());		
	insert into cashpay values (get_shopindex(), get_cashnumber(), get_znumber(), get_checknumber(), au.id, au.cardnum, get_total(), 0, 0);--���� ������ �� ������ �� ��������������
	update cashsettings set intvalue = get_reason_close() where var_name = 'check_open'; --���������� ������� ��������� ����
	return ret;
	else
	raise '������������ ������� ��� ������ �������';
	end if;
--���� ��������, ���� ����� ������� ���� ������� �� �������, �������������� ������ ���� ���� �� ������
end;
$$ language 'plpgsql';

create or replace function cash_in(cash curmoney.periodin%type) returns curmoney.periodin%type as $$ --�������� ��������
declare
old curmoney.periodin%type;--������ �������� �����
begin
if(get_zopen()=1) then
	begin--���� ����� �������
		select periodin into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
		update curmoney set periodin = old+cash where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());--��� �������� ����� �������� ������ � ������� curmoney
	end;
	else--����� �������
		begin
		select beforein into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()+1);
		if (not found) then--���� ������ � ����� ������ �� �������, ����� �� �����������
			begin
				select afterz into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
				insert into curmoney values (get_shopindex(), get_cashnumber(), get_znumber()+1, old, cash, 0, 0, 0, 0, 0);
			end;
			else--������ � ����� ������ �������
				begin
				update curmoney set beforein = old+cash where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
			end;
		end if;
end; end if;
return cash_money();--������� ������ ���������� ����� �������� ���������
end;
$$ language 'plpgsql';


create or replace function cash_out(cash curmoney.periodout%type) returns curmoney.periodout%type as $$ --������� ��������
declare
old curmoney.periodout%type;--������ �������� �����
begin
if(get_zopen()=1) then
	begin--���� ����� �������
		select periodout into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
		update curmoney set periodout = old+cash where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());--��� �������� ����� �������� ������ � ������� curmoney
	end;
	else--����� �������
		begin
		select periodout into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()+1);
		if (not found) then--���� ������ � ����� ������ �� �������, ����� �� �����������
			begin
				select afterz into old from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
				insert into curmoney values (get_shopindex(), get_cashnumber(), get_znumber()+1, old, 0, cash, 0, 0, 0, 0);
			end;
			else--������ � ����� ������ �������
				begin
				update curmoney set beforeout = old+cash where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
			end;
		end if;
end; end if;
return cash_money();
end;
$$ language 'plpgsql';

create or replace function cash_money() returns curmoney.periodout%type as $$--���������� ���������� � �����
declare
ret curmoney.periodout%type;--������������ ��������
sale cashsail.totalrub%type;--������� �� ��������
begin
if(get_zopen()=0) then--���� ����� �� �������
	begin
	select beforez+beforein+periodin-beforeout-periodout into ret from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()+1);
	if (not found)--���� ����� �� ���� �������� ����� � ����� �� �������
		then
		begin
		select afterz into ret from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
		return ret;--������, ��������� ���, ������� ���������� �� ���������� �����
		end;
		end if;
	--���� ��������, ���� ����� �� ������� � ���� �������� �����
	return ret;
	end;
	end if;
--����� �������
select beforez+beforein+periodin-beforeout-periodout into ret from curmoney where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());--�������� ������������, ������������ ������ ������ �������
select sum(cs.totalrub - (select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and 
	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber))) into sale from cashsail cs where (cs.shopindex = get_shopindex()) and (cs.cashnumber = get_cashnumber())
	and (cs.znumber = get_znumber()) and (cs.linked = 0) and (cs.credcardindex = 0/*��������*/);
return sale+ret;
end;
$$ language 'plpgsql'

create or replace function close_z() returns void as $$
declare
check_open cashsettings.intvalue%type;--������� ��������� ����
begin
--TODO
select intvalue into check_open from cashsettings where var_name = 'z_open';--�������� �������� �������� �����
if(check_open = 0) then
	raise exception '����� �� �������';
	end if;
check_open:=0;
select count(1), sum(totalrub) into check_open from cashsail where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber())
	and (operation in (get_reason_inserting(), get_reason_subtotal()));
if(check_open > 0) then
	raise exception '� �������� ����� ������������ �� ����������� ����';
	end if;
update curmoney set afterz = cash_money() where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());--�������� ���������� � ��
update currests set modifed = now(),
	sale = nvl_numeric((select sum(cs.totalrub - (select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and 
	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber))) from cashsail cs where (cs.shopindex = get_shopindex()) and (cs.cashnumber = get_cashnumber())
	and (cs.znumber = get_znumber()) and (cs.repl = get_vsale()))),
	returnz = nvl_numeric((select sum(cs.totalrub - (select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and 
	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber))) from cashsail cs where (cs.shopindex = get_shopindex()) and (cs.cashnumber = get_cashnumber())
	and (cs.znumber = get_znumber()) and (cs.repl = get_vreturn()))),
	storno = nvl_numeric((select sum(cs.totalrub - (select sum(cd.discountrub) from cashdisc cd where (cs.shopindex = cd.shopindex) and 
	(cs.cashnumber = cd.cashnumber) and (cs.znumber = cd.znumber))) from cashsail cs where (cs.shopindex = get_shopindex()) and (cs.cashnumber = get_cashnumber())
	and (cs.znumber = get_znumber()) and (cs.operation = get_vstorno()))),
	resultz = sale - returnz
	where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber());
update cashsettings set intvalue = 0 where var_name = 'z_open';--���������� ������� �������� �����
update cashsettings set intvalue = 0 where var_name = 'check_number';--�������� ������� �����
update cashsettings set intvalue = 0 where var_name = 'current_id';--��������(�� ������ ������) ������� �������
--update cashsettings set intvalue = 0 where var_name = 'check_open';--���������� ������� ��������� ����	 
end;
$$ language 'plpgsql';


--������������ �������
create or replace function add_bc(bc bar.barcode%type) returns cashsail.id%type as $$ --���������� ������ �� ��, ���������� ��������� ��� �� ��, ������� ������ ������� � ����
declare
bc_var bc;
art_var art;
begin
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� �������� ����'; end if;
bc_var:=findbar(bc);
art_var:=add_position(bc_var);
return art_var.id;--���������� ����� ���������� �������
end;
$$ language 'plpgsql';

create or replace function add_art(art plucash.articul%type) returns cashsail.id%type as $$ --���������� ������ �� ��, ���������� ��������� ��� �� ��, ������� ������ ������� � ����
declare
bc_var bc;
art_var art;
begin
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� �������� ����'; end if;
bc_var.cardarticul:=art;
bc_var.cardsize:='NOSIZE';
bc_var.quantity:=1;
art_var:=add_position(bc_var);
return art_var.id;--���������� ����� ���������� �������
end;
$$ language 'plpgsql';

create or replace function set_quantity(qnt varchar) returns cashsail.id%type as $$--��������� ����������
declare
bc_var bc;
ret_art art;
id_pos cashsail.id%type;
q cashsail.quantity%type;
mes plucash.mespresision%type;--�������� ������� ���������
nm plucash.description%type;--������������
begin
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� �������� ����'; end if;
q:=cast(qnt as numeric);
select description, mespresision into nm, mes from check_pg where id = 0;
if mod(q, mes)>0 then
-- 	ret.quantity := ret.mespresision;
	raise exception '��� ������ % ������������ �������� ������� ��������� %', nm, mes;
	end if;
update cashsail set quantity=q, totalrub=round(cashsail.pricerub*q, get_numeric_round()) where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = 0);
return 0;
end;
$$ language 'plpgsql';

create or replace function storno(pos varchar) returns cashsail.id%type as $$
declare
id_pos cashsail.id%type;
shop_index cashsail.shopindex%type;
cash_number cashsail.cashnumber%type;
z_number cashsail.znumber%type;
check_number cashsail.checknumber%type;
tmp1 art;
tmp2 bc;
begin
if(get_checkopen() = get_reason_subtotal()) then raise '�� ���� �������� ����'; end if;
tmp1:=add_position(tmp2);--��������� ���������� �������
update cashdisc set discountrub = 0, percent = 0 where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = 0);--��-�� ����������� �����������
update cashsail set quantity = 0, totalrub = 0 where (shopindex = get_shopindex()) and (cashnumber = get_cashnumber()) and (znumber = get_znumber()) and (checknumber = get_checknumber()) and (id = 0);--������� �������(�������) �������, � ��������� ������ ������� �� ������
if(pos is null) then
	id_pos:=0;
	else
	begin 
	id_pos:=cast(pos as integer);
	if(id_pos>=get_idpos()) then
		begin
		raise exception '�� ������ ����� ������� ��� �������������';
		end;
		end if;
	update cashsettings set intvalue = id_pos, datevalue = now() where var_name = 'current_id';--��������� ������ ������� �������
	end;
	end if;
shop_index := get_shopindex();
cash_number := get_cashnumber();
z_number := get_znumber();
check_number := get_checknumber();	
update cashsail set quantity=0, totalrub=0, operation=get_vstorno()  where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = id_pos);
update cashdisc set discountrub=0 where (shopindex = shop_index) and (cashnumber = cash_number) and (znumber = z_number) and (checknumber = check_number) and (id = id_pos);
return tmp1.id; --���������� ����� ���������� �������
end;
$$ language 'plpgsql'; 

create or replace function close_cash(cash varchar) returns check_pos.totalrub%type as $$--�������� ���� ���������
declare
au cash_auth;
ret check_pos.totalrub%type;
begin
au.summ:=cast(cash as numeric);
au.id:=0;--�������� insert into credcard values(0, '��������', 0, 1);
au.cardnum:='';--��� �����
au.authcode:='';--��� � ���� ����������
au.percent:=1;--��� ������
au.discount_sum:=0;--��� ����� ������
ret:=cast(close_check(au) as varchar)||' '||get_currency();
return ret;
end;
$$ language 'plpgsql';




create or replace function initialize_db() returns void as $$--�������������� ���������� ���������� ������� ������ � ������������
declare
begin

update cashsettings set intvalue = 0 where var_name = 'current_id';
update cashsettings set intvalue = 0 where var_name = 'z_open';
update cashsettings set intvalue = 1 where var_name = 'znumber';
update cashsettings set intvalue = 0 where var_name = 'check_open';
update cashsettings set intvalue = 0 where var_name = 'check_number';
delete from cashdisc;
delete from cashsail;
delete from cashpay;
delete from curmoney;
delete from currests;
-- insert into currests values(get_shopindex(), get_cashnumber(), 0, now(), 0, 0, 0, 0, 0, 0);
-- insert into curmoney values(get_shopindex(), get_cashnumber(), 0, 0, 0, 0, 0, 0, 0);
end;
$$ language 'plpgsql';


--������������� ����������
-- ������� ���������� �������� ������� ���������� ����� ���������� ��� ��� ��������� �������

 