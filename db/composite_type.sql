﻿-- drop type bc cascade;
-- create type bc as (
--  	cardarticul bar.cardarticul%type,--varchar(30),
-- 	cardsize bar.cardsize%type,--varchar(20),
-- 	quantity bar.quantity%type--numeric(7,3)
-- 	);
create type bc as (
 	cardarticul varchar(30),
	cardsize varchar(20),
	quantity numeric(7,3)
	);

drop type art cascade;
create type art as(
  id integer,--позиция в чеке
  articul varchar(30),
  description character varying(80),
  price numeric(12, 2),
  quantity numeric(7, 3),
  current_quantity numeric(7, 3),
  summ numeric(12, 2),
  mesuriment character varying(30),
  mespresision numeric(6,3),
  add1 character varying(40),
  add2 character varying(40),
  add3 character varying(40),
  addnum1 numeric,
  addnum2 numeric,
  addnum3 numeric,
  scale character varying(20),
  cardsize character varying(30),
  groop1 integer,
  groop2 integer,
  groop3 integer,
  groop4 integer,
  groop5 integer,
  department integer,
  deleted integer);

-- drop type clas cascade;
create type clas as(
groop1 integer,
groop2 integer,
groop3 integer,
groop4 integer,
groop5 integer);


-- drop type client cascade;
create type client as (
id integer,
description varchar(80),
comments varchar(255));

-- drop type cash_auth cascade;
create type cash_auth as (
summ numeric(12, 2),--сумма данного платежа
id integer,--индекс данного платежа
cardnum varchar(20),--номер карты
authcode varchar(10),--код авторизации
percent numeric(4, 2),--процент скидки на платеж
discount_sum numeric(12, 2)--сумма скидки на платеж
);


