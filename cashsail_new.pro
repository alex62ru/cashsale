#-------------------------------------------------
#
# Project created by QtCreator 2012-12-15T12:51:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Cashsale
TEMPLATE = app
CONFIG += warn_off debug -std=c++11
DEFINES += OS_LINUX

DESTDIR = ./rc
OBJECTS_DIR = ./rc

INCLUDEPATH += /usr/local/pgsql/include \
               ./cashload/cashload

LIBS += /usr/local/pgsql/lib/libpq.a

SOURCES += main.cpp\
        mainwindow.cpp\
        cash.cpp\
        cgoodsmodel.cpp\
        comdevice.cpp\
        fr101api.cpp\
        ./cashload/cashload/common.cpp\
        ./cashload/cashload/database.cpp\
        ./cashload/cashload/exceptions.cpp

HEADERS  += mainwindow.h\
        cash.h\
        cgoodsmodel.h\
        comdevice.h\
        fr101api.h\
        ./cashload/cashload/common.h\
        ./cashload/cashload/database.h\
        ./cashload/cashload/exceptions.h

FORMS    += mainwindow.ui \
    cash.ui

RESOURCES = image.qrc

TRANSLATIONS = cashsail_ru.ts
